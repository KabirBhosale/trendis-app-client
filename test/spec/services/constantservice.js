'use strict';

describe('Service: constantservice', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var constantservice;
  beforeEach(inject(function (_constantservice_) {
    constantservice = _constantservice_;
  }));

  it('should do something', function () {
    expect(!!constantservice).toBe(true);
  });

});
