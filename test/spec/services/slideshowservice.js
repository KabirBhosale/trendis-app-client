'use strict';

describe('Service: slideShowService', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var slideShowService;
  beforeEach(inject(function (_slideShowService_) {
    slideShowService = _slideShowService_;
  }));

  it('should do something', function () {
    expect(!!slideShowService).toBe(true);
  });

});
