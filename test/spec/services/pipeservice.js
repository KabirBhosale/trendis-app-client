'use strict';

describe('Service: pipeservice', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var pipeservice;
  beforeEach(inject(function (_pipeservice_) {
    pipeservice = _pipeservice_;
  }));

  it('should do something', function () {
    expect(!!pipeservice).toBe(true);
  });

});
