'use strict';

describe('Service: waitUtilService', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var waitUtilService;
  beforeEach(inject(function (_waitUtilService_) {
    waitUtilService = _waitUtilService_;
  }));

  it('should do something', function () {
    expect(!!waitUtilService).toBe(true);
  });

});
