'use strict';

describe('Service: contentFilterService', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var contentFilterService;
  beforeEach(inject(function (_contentFilterService_) {
    contentFilterService = _contentFilterService_;
  }));

  it('should do something', function () {
    expect(!!contentFilterService).toBe(true);
  });

});
