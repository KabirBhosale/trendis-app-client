'use strict';

describe('Service: cardviewfactory', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var cardviewfactory;
  beforeEach(inject(function (_cardviewfactory_) {
    cardviewfactory = _cardviewfactory_;
  }));

  it('should do something', function () {
    expect(!!cardviewfactory).toBe(true);
  });

});
