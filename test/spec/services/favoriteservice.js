'use strict';

describe('Service: favoriteservice', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var favoriteservice;
  beforeEach(inject(function (_favoriteservice_) {
    favoriteservice = _favoriteservice_;
  }));

  it('should do something', function () {
    expect(!!favoriteservice).toBe(true);
  });

});
