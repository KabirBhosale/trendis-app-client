'use strict';

describe('Service: feedserviceprovider', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var feedserviceprovider;
  beforeEach(inject(function (_feedserviceprovider_) {
    feedserviceprovider = _feedserviceprovider_;
  }));

  it('should do something', function () {
    expect(!!feedserviceprovider).toBe(true);
  });

});
