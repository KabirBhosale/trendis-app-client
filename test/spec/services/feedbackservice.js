'use strict';

describe('Service: feedbackService', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var feedbackService;
  beforeEach(inject(function (_feedbackService_) {
    feedbackService = _feedbackService_;
  }));

  it('should do something', function () {
    expect(!!feedbackService).toBe(true);
  });

});
