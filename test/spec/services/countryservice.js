'use strict';

describe('Service: countryservice', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var countryservice;
  beforeEach(inject(function (_countryservice_) {
    countryservice = _countryservice_;
  }));

  it('should do something', function () {
    expect(!!countryservice).toBe(true);
  });

});
