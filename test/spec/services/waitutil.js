'use strict';

describe('Service: waitUtil', function () {

  // load the service's module
  beforeEach(module('trendisAppClientApp'));

  // instantiate service
  var waitUtil;
  beforeEach(inject(function (_waitUtil_) {
    waitUtil = _waitUtil_;
  }));

  it('should do something', function () {
    expect(!!waitUtil).toBe(true);
  });

});
