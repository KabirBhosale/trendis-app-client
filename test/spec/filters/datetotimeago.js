'use strict';

describe('Filter: dateTotimeAgo', function () {

  // load the filter's module
  beforeEach(module('trendisAppClientApp'));

  // initialize a new instance of the filter before each test
  var dateTotimeAgo;
  beforeEach(inject(function ($filter) {
    dateTotimeAgo = $filter('dateTotimeAgo');
  }));

  it('should return the input prefixed with "dateTotimeAgo filter:"', function () {
    var text = 'angularjs';
    expect(dateTotimeAgo(text)).toBe('dateTotimeAgo filter: ' + text);
  });

});
