'use strict';

describe('Filter: reactionsToString', function () {

  // load the filter's module
  beforeEach(module('trendisAppClientApp'));

  // initialize a new instance of the filter before each test
  var reactionsToString;
  beforeEach(inject(function ($filter) {
    reactionsToString = $filter('reactionsToString');
  }));

  it('should return the input prefixed with "reactionsToString filter:"', function () {
    var text = 'angularjs';
    expect(reactionsToString(text)).toBe('reactionsToString filter: ' + text);
  });

});
