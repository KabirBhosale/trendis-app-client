'use strict';

describe('Controller: PersonalisecategoryCtrl', function () {

  // load the controller's module
  beforeEach(module('trendisAppClientApp'));

  var PersonalisecategoryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PersonalisecategoryCtrl = $controller('PersonalisecategoryCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PersonalisecategoryCtrl.awesomeThings.length).toBe(3);
  });
});
