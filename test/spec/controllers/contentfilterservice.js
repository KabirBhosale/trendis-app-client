'use strict';

describe('Controller: ContentfilterserviceCtrl', function () {

  // load the controller's module
  beforeEach(module('trendisAppClientApp'));

  var ContentfilterserviceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ContentfilterserviceCtrl = $controller('ContentfilterserviceCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ContentfilterserviceCtrl.awesomeThings.length).toBe(3);
  });
});
