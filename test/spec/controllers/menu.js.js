'use strict';

describe('Controller: MenuJsCtrl', function () {

  // load the controller's module
  beforeEach(module('trendisAppClientApp'));

  var MenuJsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MenuJsCtrl = $controller('MenuJsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MenuJsCtrl.awesomeThings.length).toBe(3);
  });
});
