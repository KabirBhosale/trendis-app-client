'use strict';

describe('Controller: ContentfilterCtrl', function () {

  // load the controller's module
  beforeEach(module('trendisAppClientApp'));

  var ContentfilterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ContentfilterCtrl = $controller('ContentfilterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ContentfilterCtrl.awesomeThings.length).toBe(3);
  });
});
