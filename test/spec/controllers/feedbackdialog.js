'use strict';

describe('Controller: FeedbackdialogCtrl', function () {

  // load the controller's module
  beforeEach(module('trendisAppClientApp'));

  var FeedbackdialogCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FeedbackdialogCtrl = $controller('FeedbackdialogCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FeedbackdialogCtrl.awesomeThings.length).toBe(3);
  });
});
