'use strict';

describe('Directive: instaFeedView', function () {

  // load the directive's module
  beforeEach(module('trendisAppClientApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<insta-feed-view></insta-feed-view>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the instaFeedView directive');
  }));
});
