'use strict';

describe('Directive: feedCardView', function () {

  // load the directive's module
  beforeEach(module('trendisAppClientApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<feed-card-view></feed-card-view>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the feedCardView directive');
  }));
});
