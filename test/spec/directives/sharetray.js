'use strict';

describe('Directive: shareTray', function () {

  // load the directive's module
  beforeEach(module('trendisAppClientApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<share-tray></share-tray>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the shareTray directive');
  }));
});
