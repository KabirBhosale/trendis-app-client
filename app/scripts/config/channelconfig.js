/**
 * Created by saran on 10/8/15.
 */
var channelConfigs = {
  trendis: {
    siteBaseUrl: 'http://www.trendis.com/',
    baseUrl: '/api/',
    // baseUrl: 'http://192.168.80.160:3000/api/',
    create: function (channelInfo, channelConfig) {
      return new TrendisChannel(channelInfo, channelConfig);
    }
  },
  facebook: {
    baseUrl: 'https://graph.facebook.com',
    create: function (channelInfo, channelConfig) {
      return new FacebookChannel(channelInfo, channelConfig);
    },
    appID: '1529707314013129'//Trendis production
    // appID: '531682143674670'//Trendis-web-server(Trendis app)
    //appID:'607118186096323'//opussoft2 development
  },
  favorite: {
    baseUrl: '/api/',
    create: function (channelInfo, channelConfig) {
      return new FavoriteChannel(channelInfo, channelConfig);
    }
  }
}
