
/*get share url for sharing feed-KB*/
var getSocialShareURL = function (type, feed) {
    if (feed == undefined) {
        return undefined;
    }
    var constantService = angular.element('*[ng-app]').injector().get("constantService");
    var srclink = feed.sourceLink;
    if (feed.feedType == "Youtube" || feed.feedType == "youtube") {
        srclink = feed.sourceLink.replace('embed', 'watch');
    }
    if (type == constantService.share.Facebook) {
        //return 'https://www.facebook.com/sharer/sharer.php?u=' + feed.sourceLink;
        //return 'https://www.facebook.com/dialog/feed?u=' + feed.sourceLink;
        return 'http://www.facebook.com/dialog/share?app_id=' + channelConfigs.facebook.appID +
            '&href=' + srclink +
            '&name=' + 'Shared Via www.trendis.com' +
            '&caption=' + 'Shared Via www.trendis.com' +
            '&redirect_uri=' + channelConfigs.trendis.siteBaseUrl + 'closeWindow.html' +
            '&display=popup';
    }

    if (feed.feedType == constantService.share.Twitter) {
        return 'https://twitter.com/intent/tweet?url=' + srclink + '&text=' + feed.title.substring(0, 50) + '&via=' + ' www.trendis.com';
    }
    else if (type == constantService.share.Twitter) {
        return 'https://twitter.com/intent/tweet?url=' + (_getShortUrl(feed, constantService)) + '&text=' + feed.title.substring(0, 50);
    }

    else if (type == constantService.share.WhatsApp) {
        return 'https://www.addtoany.com/add_to/whatsapp?linkurl=' + srclink + '&linkname= shared via :www.trendis.com';
    }
    else if (type == constantService.share.Reddit) {
        return 'https://reddit.com/submit?url=' + encodeURIComponent(_getShortUrl(feed, constantService)) + '&title=' + feed.title.substring(0, 150);
    }

    else if (type == constantService.share.Mail) {
        return "mailto:?subject=shared via Trendis&body=" + srclink;
    }

    return undefined;
}

function _getShortUrl(feed, constantService) {
    var srclink = feed.sourceLink;
    var contentType = 'p';
    var finalUrl = undefined;
    if (feed.feedType == constantService.feedTypes.Facebook) {
        srclink = srclink.replace('https://www.facebook.com', 'fb');
    } else if (feed.feedType == constantService.feedTypes.Youtube) {
        srclink = srclink.replace('https://www.youtube.com', 'yb');
        srclink = srclink.replace('https://www.youtu.be', 'yb');
    } else if (feed.feedType == constantService.feedTypes.Twitter) {
        srclink = srclink.replace('https://www.twitter.com', 'tw');
        srclink = srclink.replace('http://www.twitter.com', 'tw');
        srclink = srclink.replace('https://twitter.com', 'tw');
    } else if (feed.feedType == constantService.feedTypes.Instagram) {
        srclink = srclink.replace('https://www.instagram.com', 'ig');
    } else {
        srclink = srclink.replace('https://www.reddit.com', 'rt');
    }
    //contentType  
    if (feed.mediaType == "video") {
        contentType = 'v';
    }
    else if (feed.mediaType == "link") {
        contentType = 'l';
        srclink = srclink.replace('http://', '');
        srclink = srclink.replace('https://', '');
    }
    else {
        contentType = 'p';
    }

    if (feed.is_newWindow && feed.is_newWindow == true) {
        contentType = 'o' + contentType;
    }

    finalUrl = "https://www.trendis.com/pv/" + feed.id + "/" + contentType + "?s=" + srclink;
    return finalUrl;
}

/*toggle favourites feed-KB*/
var toggleFavouriteFeed = function (feed) {
    if (feed == undefined) {
        return undefined;
    }
    var favoriteService = angular.element('*[ng-app]').injector().get("favoriteService");
    if (feed.favorites) {
        favoriteService.remove(feed);
    } else {
        favoriteService.add(feed);
    }
}

var _returnColumns = function () {
    var screenWidth = window.innerWidth;
    if (screenWidth < 600) {
        return 1;
    } else if (screenWidth >= 600 && screenWidth < 960) {
        return 2;
    } else if (screenWidth >= 960 && screenWidth < 1280) {
        return 3;
    } else {
        return 4;
    }
}

var getShortNetworkName = function (network) {
    var constantService = angular.element('*[ng-app]').injector().get("constantService");
    if (network == constantService.feedTypes.Facebook) {
        return 'FB';
    } else if (network == constantService.feedTypes.Youtube) {
        return 'YB';
    } else if (network == constantService.feedTypes.Twitter) {
        return 'TW';
    } else if (network == constantService.feedTypes.Instagram) {
        return 'IG';
    } else {
        return 'RT';
    }
}

var getShortGender=function(gender){
    if(gender=='male'){
        return 'm';
    }
    return 'f';
}