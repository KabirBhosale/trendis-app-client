/**
 * Created by saran on 10/5/15.
 */
var menuConfigs = [{
    id: "home",
    name: "Home",
    menuIcon:"home",
    countrySpecific: false,
    requiresContentFilter: true,
    tags: [],
    // subMenuConfigs: [{
    //     id: "mytrender",
    //     name: "My Trender",
    //     tags: [],
    //     countrySpecific: false
    // }, {
    //     id: "settings",
    //     name: "Customize MyTrender",
    //     tags: [],
    //     countrySpecific: false,
    //     requiresInfoBar: true
    // }, {
    //     id: "sharepad",
    //     name: "Bookmarked",
    //     tags: [],
    //     countrySpecific: false,
    //     requiresContentFilter: false,
    //     requiresInfoBar: true,
    //     helpRequired: true
    // }]
}, {
    id: "humor",
    name: "Humor",
    class: 'red',
    menuIcon:"humor",
    feedCategory:true,
    requiresContentFilter: true,
    tags: [],
    subMenuConfigs: [{
        id: "all",
        name: "All",
        tags: []
    }, {
        id: "comics",
        name: "Comics",
        tags: []
    }, {
        id: "dailyhumor",
        name: "Daily Humor",
        tags: []
    }, {
        id: "gags",
        name: "Pranks",
        tags: []
    }]
}, {
    id: "sports",
    name: "Sports",
    menuIcon:"basketball",
    feedCategory:true,
    requiresContentFilter: true,
    tags: [],
    subMenuConfigs: [{
        id: "all",
        name: "All",
        tags: []
    }, {
        id: "american_football",
        name: "American Football",
        tags: []
    }, {
        id: "basketball",
        name: "Basketball",
        tags: []
    }, {
        id: "cricket",
        name: "Cricket",
        tags: []
    }, {
        id: "fights",
        name: "Fights",
        tags: []
    }, {
        id: "soccer",
        name: "Soccer",
        tags: []
    }, {
        id: "baseball",
        name: "Baseball",
        tags: []
    }, {
        id: "golf",
        name: "Golf",
        tags: []
    }, {
        id: "tennis",
        name: "Tennis",
        tags: []
    }]
}, {
    id: "entertainment",
    name: "Entertainment",
    feedCategory:true,
    menuIcon:"entertainment",
    requiresContentFilter: true,
    tags: [],
    subMenuConfigs: [{
        id: "all",
        name: "All",
        tags: []
    }, {
        id: "movies",
        name: "Movies",
        tags: []
    }, {
        id: "music",
        name: "Music",
        tags: []
    }, {
        id: "celebrity",
        name: "Celebrity",
        tags: []
    },{
        id: "television",
        name: "Television",
        tags: [] 
    }]
}, {
    id: "infotainment",
    name: "Infotainment",
    feedCategory:true,
    menuIcon:"infotainment",
    requiresContentFilter: true,
    tags: [],
    subMenuConfigs: [{
        id: "all",
        name: "All",
        tags: []
    }, {
        id: "interesting",
        name: "Interesting",
        tags: []
    }, {
        id: "auto",
        name: "Auto",
        tags: []
    }, {
        id: "tech",
        name: "Tech",
        tags: []
    }, {
        id: "dailynews",
        name: "Daily News",
        tags: []
    },{
        id: "gaming",
        name: "Gaming",
        tags: []
    }, {
        id: "business",
        name: "Business",
        tags: []
    }, {
        id: "science",
        name: "Science",
        tags: []
    }]
}, {
    id: "lifestyle",
    name: "LifeStyle",
    feedCategory:true,
    menuIcon:"lifestyle",
    requiresContentFilter: true,
    tags: [],
    subMenuConfigs: [{
        id: "all",
        name: "All",
        isActive:true,
        tags: []
    }, {
        id: "beauty",
        name: "Beauty",
        tags: []
    }, {
        id: "food",
        name: "Food",
        tags: []
    }, {
        id: "health",
        name: "Health",
        tags: []
    }, {
        id: "travel",
        name: "Travel",
        tags: []
    }, {
        id: "quotes",
        name: "Quotes",
        tags: []
    }, {
        id: "workout",
        name: "Workout",
        tags: []
    }]
}, {
    id: "hobbies",
    name: "Hobbies",
    menuIcon:"hobbies",
    requiresContentFilter: true,
    tags: [],
    subMenuConfigs: [{
        id: "all",
        name: "All",
        tags: []
    }, {
        id: "books",
        name: "Books",
        tags: []
    }, {
        id: "diy",
        name: "DIY",
        tags: []
    }, {
        id: "photography",
        name: "Photography",
        tags: []
    }]
}]
