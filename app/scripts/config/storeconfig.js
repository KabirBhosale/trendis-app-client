/**
 * Created by saran on 10/8/15.
 */
var storeConfigs = {
    albums: {
        storeType: 'indexedDB',
        schema: {
            definition: {
                properties: {
                    keyPath: 'albumId'
                }
            },
            indexes: [
                {
                    name: 'albums_albumId_idx',
                    column: 'albumName',
                    properties: {
                        unique: false
                    }
                }]
        },
        cache: true
    },
    favorites: {
        storeType: 'indexedDB',
        schema: {
            definition: {
                properties: {
                    keyPath: 'feedKey'
                }
            },
            indexes: [
                {
                    name: 'favorites_feedId_idx',
                    column: 'feedType',
                    properties: {
                        unique: false
                    }
                }]
        },
        cache: true
    },
    likes: {
        storeType: 'indexedDB',
        dbVersion:2,
        schema: {
            definition: {
                properties: {
                    keyPath: 'feedKey'
                }
            },
            indexes: [
                {
                    name: 'likes_feedId_idx',
                    column: 'feedType',
                    properties: {
                        unique: false
                    }
                }]
        },
        cache: true
    }
}
