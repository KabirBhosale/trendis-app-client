'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:imageOnLoad
 * @description
 * # imageOnLoad
 */
angular.module('trendisAppClientApp')
  .directive('imageOnLoad', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.bind('load', function () {
          if (this.naturalWidth === 1 || this.naturalWidth === 0) {
            console.log('image is loaded empty');
            this.src = "/images/image_not_found.jpg";
          }
        });
        element.bind('error', function () {
          console.log('image could not be loaded');
          this.src = "/images/image_not_found.jpg";
        });
      }
    };
  });
