'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:feedCardView
 * @description
 * # feedCardView
 */
angular.module('trendisAppClientApp')
  .directive('feedCardView', ['$rootScope', '$timeout', '$mdDialog', '$mdMedia', '$window', 'slideShowService', 'favoriteService', 'constantService', 'likeService', '_', 'ytPlayer', 'ytFactory', 'storageService', '$mdToast', function ($rootScope, $timeout, $mdDialog, $mdMedia, $window, slideShowService, favoriteService, constantService, likeService, _, ytPlayer, ytFactory, storageService, $mdToast) {
    return {
      templateUrl: 'views/feedcardview.html',
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        feeds: "=feeds",
        feed: "=feed"
      },
      link: function postLink(scope, element, attrs) {
        var height = undefined;
        var innerW = window.innerWidth;
        scope.playerState = 0;
        if ($mdMedia('xs') || $mdMedia('sm')){
          scope.variableHeight=true;
        }

        $timeout(function () {
          var myScopeVideo = angular.element('#video_' + attrs.id);
          if (myScopeVideo && myScopeVideo[0]) {
            var v = myScopeVideo[0];
            // v.addEventListener("loadedmetadata", function () {
            //   if (!$mdMedia('xs')){
            //     //myScopeVideo[0].muted = "muted";
            //     v.volume =0;
            //   }
            // });
             
            v.addEventListener("playing", function () {
              if ($rootScope.currentVideos.length > 0) {
                _.each($rootScope.currentVideos, function (videoContent, videoIndex) {
                  if (videoContent.feedType != 'Youtube') {
                    var video = angular.element(videoContent.videoid);
                    if (video[0] && videoContent.videoid!='#video_' + attrs.id) {
                      video[0].pause();
                    }
                  } else {
                    $rootScope.$broadcast('stopYbVideos', attrs.id);
                  }
                });
              }
  
              $rootScope.currentVideos = [];

              $rootScope.currentVideos.push({
                videoid: '#video_' + attrs.id,
                feedType: scope.feed.feedType
              });
              scope.playerState=1;
              scope.$apply();
            }, true);

            v.addEventListener("pause", function () {
              scope.playerState=2;
            });
            v.addEventListener("stop", function () {
              scope.playerState=3;
            });
          }
        });


        if ((scope.feed.feedType == 'Facebook' || scope.feed.feedType == 'Reddit') && scope.feed.source && (scope.feed.source.indexOf('youtu.be') > 0 || scope.feed.source.indexOf('youtube.com') > 0)) {
          var ybVideoId = scope.feed.source.match(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/)[2];
          scope.ybVideoId = ybVideoId;
        }

        if (!scope.ybVideoId && scope.feed.feedType != 'Youtube' && $rootScope.activeRoute && $rootScope.activeRoute == constantService.searchCategory) {
          scope.isSearchRoute = true;
        }
        if(scope.$parent.menuname=='sharepad' && scope.feed.feedType != 'Youtube' && !scope.ybVideoId){
          scope.isSearchRoute = true;
        }

        if (!$mdMedia('xs') && (scope.feed.feedType == 'Youtube' || scope.ybVideoId)) {
          scope.mutedFlag = 1;
        }

        scope.ytPlayer;
        if (scope.feed.feedType == 'Youtube') {
          ytFactory.onReady(function () {
            //succes
            scope.ytPlayer = ytPlayer;
          })
        }

        scope.$on('ngYoutubePlayer:onPlayerReady', function (event, data, id) {
          scope.ytPlayer = ytPlayer;
        });

        scope.$on('ngYoutubePlayer:onPlayerStateChange', function (event, info, id) {
          //console.log(data);console.log(id);
          if (info.data == 1 && scope.ytPlayer) {
            console.log('playing : #video_' + attrs.id)
            if ($rootScope.currentVideos.length > 0) {
              _.each($rootScope.currentVideos, function (videoContent, videoIndex) {
                if (videoContent.feedType != 'Youtube') {
                  var video = angular.element(videoContent.videoid);
                  if (video[0]) {
                    video[0].pause();
                  }
                } else {
                  $rootScope.$broadcast('stopYbVideos', attrs.id);
                }
              });
              $rootScope.currentVideos = [];
            }


            scope.playerState = 1;
            $rootScope.currentVideos.push({
              videoid: '#video_' + attrs.id,
              feedType: scope.ybVideoId ? 'Youtube' : scope.feed.feedType
            });
          } else if (info.data == 2) {
            if ($rootScope.currentVideos[0] && $rootScope.currentVideos[0].videoid === '#video_' + attrs.id) {
              $rootScope.currentVideos = [];
            }
          }
        });

        scope.showMsgToast = function (feed_index) {
          var toast = $mdToast.simple()
            .textContent('The more you like or dislike, the better the content gets!!')
            .position('bottom center')
            .parent(angular.element('#feed' + feed_index))
            .highlightAction(false);

          $mdToast.show(toast).then(function (response) {
            if (response == 'ok') {
              console.log('You clicked \'OK\'.');
            }
          });
        };
        scope.onSuccess = function(e) {
          // console.info('Action:', e.action);
          // console.info('Text:', e.text);
          // console.info('Trigger:', e.trigger);
          alert("Copied!");    
          e.clearSelection();
        };
        scope.onError = function(e) {
          console.error('Action:', e.action);
          console.error('Trigger:', e.trigger);
          alert("Error while copy!");
        }

        scope.callMouseenter=function(){
          angular.element('.play_icon_'+scope.feed.id).css('fill','red');
        }

        scope.callMouseleave=function(){
          angular.element('.play_icon_'+scope.feed.id).css('fill','rgb(33, 33, 33)');
        }

        $rootScope.$on('stopYbVideos', function (ev, currentId) {
          try {
            if (attrs.id != currentId && scope.playerState == 1 && scope.ytPlayer && scope.ytPlayer['myYoutubePlayer' + (scope.feed.index % 4) + '_' + scope.feed.index]) {
              console.log('stoping #video_' + attrs.id + '===' + currentId);
              scope.ytPlayer['myYoutubePlayer' + (scope.feed.index % 4) + '_' + scope.feed.index].pauseVideo();
            }
          } catch (ex) {
            console.log('Unavailable player Id for : ' + scope.feed.id);
            //console.log(scope.ytPlayer['myYoutubePlayer'+(scope.feed.index%4)+'_'+scope.feed.index]);
          }
        });

        scope.pauseOrPlay = function () {
          var myVideo = angular.element('#video_' + attrs.id);
          if (myVideo[0].paused) {
            myVideo[0].play();
            scope.playerState = 1;
          }
          else {
            myVideo[0].pause();
            scope.playerState = 2;
          }
        }

        scope.dialogWidth = innerW - 30;
        scope.shareConstants = constantService.share;
        scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
        scope.showSlideShow = function (ev) {
          if ($rootScope.currentVideos.length > 0) {
            _.each($rootScope.currentVideos, function (videoContent, videoIndex) {
              if (videoContent.feedType != 'Youtube') {
                var video = angular.element(videoContent.videoid);
                if (video[0]) {
                  video[0].pause();
                }
              } else {
                $rootScope.$broadcast('stopYbVideos');
              }
            });
          }
          if ($rootScope.swiped == 1) {
            $rootScope.swiped = 0;
          }
          else if ($mdMedia('xs') || $mdMedia('sm')) {
            $rootScope.isDialogOpen = true;
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && scope.customFullscreen;
            $mdDialog.show({
              //controller: DialogController,
              templateUrl: 'views/sm-feed-view.html',
              scope: scope,
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose: true,
              preserveScope: true,
              onComplete: function () {
                scope.loadFeedView();
              }
            })
              .then(function (answer) {
                $rootScope.isDialogOpen = false;
              }, function () {
                $rootScope.isDialogOpen = false;
              });
          }
          else {
            slideShowService.doSlideShow(scope, { feeds: scope.feeds, index: scope.feed.index })
          }
        }

        scope.hide = function () {
          $rootScope.isDialogOpen = false;
          $mdDialog.hide();
        };

        scope.cancel = function () {
          $rootScope.isDialogOpen = false;
          $mdDialog.cancel();
        };

        scope.answer = function (answer) {
          $rootScope.isDialogOpen = false;
          $mdDialog.hide(answer);
        };

        scope.loadFeedView = function () {
          slideShowService.viewFeed(scope, angular.element('.sm-feed-view-body'));
        }

        scope.stateChanged = function (e) {
          console.log(e);
        };

        scope.toggleFavorite = function (e, feed) {
          if (scope.feed.favorites) {
            favoriteService.remove(scope.feed);
            $rootScope.$emit('delete_feedfav', scope.feed);
          } else {
            favoriteService.add(scope.feed);
            $rootScope.$emit('add_feedfav', scope.feed);
          }

          scope.feed.favorites = !scope.feed.favorites;
          e.stopPropagation();
        }

        scope.toggleLike = function (feed, likeVal) {
          // if (likeVal) {
          //   scope.feed.likes = likeVal;
          // } else {
          //   scope.feed.likes = likeVal;
          // }
          // likeService.add(scope.feed);
          storageService.get(constantService.storageDefaultCategory, constantService.storageDefaultFieldName, constantService.storageFeedLikesKey).then(function (value) {
            if (value == null) {
              //feed.index%4+'_'+feed.index
              scope.showMsgToast(feed.index%4+'_'+feed.index);
              storageService.set(constantService.storageDefaultCategory, constantService.storageFeedLikesKey, true).then(function () {});
            }
            if (feed.likes == likeVal) {
              delete scope.feed.likes;
              likeService.remove(scope.feed);
            }
            else {
              scope.feed.likes = likeVal;
              likeService.add(scope.feed);
            }
          });
        }

        //new function KB
        scope.getHeightAspectRatio = function (feedObj) {
          var width = angular.element('.feedcardview').width();
          if(feedObj.feedType=='Twitter' && feedObj.mediaType=='video'){
            height = 300;
            return 300;
          }

          if (feedObj) {
            if (!feedObj.imgW) {
              feedObj.imgW = 500;
            }
            if (!feedObj.imgH) {
              feedObj.imgH = 500;
            }
            var ratio = feedObj.imgW / feedObj.imgH;
            height = width / ratio;
            return height;
          } else {
            return;
          }
        }

        scope.getSourceChannelUrl = function (feedObj) {
          if (feedObj && feedObj.feedType == constantService.feedTypes.Facebook) {
            return 'https://www.facebook.com/' + (feedObj.sourceId ? feedObj.sourceId : feedObj.sourceName);
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.Twitter) {
            return 'https://twitter.com/' + feedObj.screen_name;
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.Youtube) {
            return 'https://www.youtube.com/channel/' + feedObj.sourceId;
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.Instagram) {
            return 'https://www.instagram.com/' + feedObj.sourceId;
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.Reddit) {
            return 'https://www.reddit.com/user/' + feedObj.sourceId;
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.GooglePlus) {
            return 'https://plus.google.com/' + feedObj.sourceId;
          }
        }

        scope.getSourceFeedUrl = function (feedObj) {
          if (feedObj && feedObj.feedType == constantService.feedTypes.Facebook) {
            return 'https://www.facebook.com/' + feedObj.id;
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.Twitter) {
            return 'https://twitter.com/' + feedObj.id;
          } else if (feedObj && feedObj.feedType == constantService.feedTypes.Youtube) {
            return 'https://www.youtube.com/watch?v=' + feedObj.id;
          }
        }

        scope.openOverlay = function (feed_index) {
          angular.element('.feed' + feed_index + '.after').css({ 'top': angular.element('#feed' + feed_index).position().top, 'height': angular.element('#feed' + feed_index).height() + 2, 'width': angular.element('#feed' + feed_index).width() + 1, 'left': angular.element('#feed' + feed_index).position().left, 'display': 'block', 'text-align': 'center', 'background': 'rgba(0, 0, 0, .6)' });
        }

        scope.openViewOverlay = function (feed_index, feedObj) {
          if (!$mdMedia('sm') && !$mdMedia('xs')) {
            if (feedObj.imageURL && feedObj.imageURL != 'NA') {
              angular.element('.feed' + feed_index + '.feedmouseover').css({ 'top': angular.element('#feed' + feed_index).position().top + angular.element('.md-card-img' + feed_index).parent().parent().prev()[0].clientHeight + angular.element('.md-card-img' + feed_index).parent().parent().prev().prev().prev()[0].clientHeight, 'height': height + 2, 'width': angular.element('#feed' + feed_index).width() + 1, 'left': angular.element('#feed' + feed_index).position().left, 'display': 'block', 'text-align': 'center', 'background': 'rgba(0, 0, 0, .6)' });
            } else {
              angular.element('.feed' + feed_index + '.feedmouseover').css({
                'top': angular.element('#feed' + feed_index).position().top + angular.element('.md-card-img' + feed_index).parent().parent().prev().prev()[0].clientHeight,
                'height': angular.element('.md-card-img' + feed_index).parent().parent()[0].clientHeight,//height, 
                'width': angular.element('#feed' + feed_index).width() + 1,
                'left': angular.element('#feed' + feed_index).position().left, 'display': 'block',
                'text-align': 'center', 'background': 'rgba(0, 0, 0, .6)'
              });
            }
          }
        }

        scope.closeViewOverlay = function (feed_index, feedObj) {
          angular.element('.feed' + feed_index + '.feedmouseover').css({ 'display': 'none' });
        }

        scope.closeOverlay = function (feed_index) {
          angular.element('.feed' + feed_index + '.after').css({ 'display': 'none' });
          angular.element('.feed' + feed_index + '.feedmouseover').css({ 'display': 'none' });
        }

        scope.openlink = function () {
          slideShowService.showLinkContentInNewWindow();
        }

        scope.showShareContent = function (e) {
          e.stopPropagation();
        }
      }
    };
  }]);
