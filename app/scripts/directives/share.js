'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:share
 * @description
 * # share
 */
angular.module('trendisAppClientApp')
.directive('share',['$rootScope','$window', 'constantService', 'shareService', 'favoriteService',function ($rootScope,$window, constantService, shareService, favoriteService) {
  return {
    templateUrl: 'views/share.html',
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
      feed: "=feed"
    },
    link: function postLink(scope, element, attrs) {
      scope.getShareURL = shareService.getShareURL;
      scope.shareConstants = constantService.share;
      scope.istouchdevice = $rootScope.isTouchDevice;

      scope.toggleFavorite = function (feed) {
        if (scope.feed.favorites) {
          favoriteService.remove(scope.feed);
          //scope.$emit('delete_feed', element.find(scope.feed.feedKey));
          $rootScope.$emit('delete_feedfav', scope.feed);
        } else {
          favoriteService.add(scope.feed);
          $rootScope.$emit('add_feedfav', scope.feed);
        }

        //scope.emit("", scope.feed.);
        scope.feed.favorites = !scope.feed.favorites;
      }

      scope.share = function ($event, type) {
        $event.stopPropagation();
        if($rootScope.isdeviceapp && type==="Facebook" && parseFloat($rootScope.androidAppVersion)>105){//010416
        var feedsourcelink=angular.copy(scope.feed.sourceLink);
          if(scope.feed.feedType==="Youtube" || scope.feed.feedType==="youtube"){
            feedsourcelink=feedsourcelink.replace('embed','watch');
          }
          fbContentShareApp(feedsourcelink);
        }
        else{
            var url = shareService.getShareURL(type, scope.feed);

            if (type=="Reddit") {
              var newwindow = $window.open(url, 'name', 'height=585,width=1042');
            }
            else{
              var newwindow = $window.open(url, 'name', 'height=300,width=500');
            }
            if (newwindow.focus) {
              newwindow.focus();
            }
        }
        return false;
      }
    }
  };
}]);

