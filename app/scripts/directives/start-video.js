'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:startVideo
 * @description
 * # startVideo
 */
angular.module('trendisAppClientApp')
  .directive('startVideo',['$rootScope', '$mdMedia', function ($rootScope,$mdMedia) {
    return {
      scope: {
        feed: "=feed"
      },
      link: function(scope, element, attrs) {
        if(scope.feed.mediaType=='video' && scope.feed.feedType=='Facebook' && attrs.id=='video_feed1_1'){          
          element.attr("autoplay", "true");
        }    
        if (!$mdMedia('xs')) {
          element.attr("muted", "true");
        }    
      }
    };
  }]);
