'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:imgCheckDir
 * @description
 * # imgCheckDir
 */
angular.module('trendisAppClientApp')
  .directive('imgCheckDir', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var imgAttr = undefined;
        var categoryElement = undefined;
        var subcategoryElement = undefined;
        if (attrs.name && attrs.name.split("__").length > 1) {
          imgAttr = attrs.name,
            categoryElement = imgAttr.split("__")[0],
            subcategoryElement = imgAttr.split("__")[1];
        }

        element.imgCheckbox({
          onload: function () {
            if (scope.selectedItems && scope.selectedItems[categoryElement] && scope.selectedItems[categoryElement][subcategoryElement]) {
              //console.log(attrs.$$element[0].offsetParent)
              $(attrs.$$element[0].offsetParent).addClass("imgChked");
              $(attrs.$$element[0].parentNode).addClass("imgChked");
            }
            if (scope.personalisedArr && scope.personalisedArr.length > 0 && scope.personalisedArr.indexOf(attrs.name) > -1) {
              $(attrs.$$element[0].offsetParent).addClass("imgChked");
              $(attrs.$$element[0].parentNode).addClass("imgChked");
            }
          },
          onclick: function () {
            var isChecked = element[0].offsetParent.className.contains("imgChked");
            var imgEl = element[0].name;
            var categoryElement = undefined;
            var subcategoryElement = undefined;
            if (imgEl.split("__").length > 1) {
              categoryElement = imgEl.split("__")[0];
              subcategoryElement = imgEl.split("__")[1];
            }
            if (isChecked) {
              if (categoryElement && subcategoryElement) {
                if (!scope.selectedItems[categoryElement]) {
                  scope.selectedItems[categoryElement] = {};
                }
                scope.selectedItems[categoryElement][subcategoryElement] = true;
              } else {
                scope.personalise.value = element[0].name;
                scope.addPersonalize(element[0]);
                scope.$apply();
              }
            } else {
              if (categoryElement && subcategoryElement) {
                scope.selectedItems[categoryElement][subcategoryElement] = false;
              } else {
                scope.dropPersonalize(undefined, undefined, element[0].name);
                scope.$apply();
              }
            }
          }
        });
      }
    };
  });
