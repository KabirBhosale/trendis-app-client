'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:fancybox
 * @description
 * # fancybox
 */
angular.module('trendisAppClientApp')
.directive('fancybox',['$compile', 'ezfb',function ($compile, ezfb) {
  return {
      restrict: 'A',
      replace: false,
      link: function ($scope, element, attrs) {
          function parseUri(str) {
              var o = parseUri.options,
                  m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
                  uri = {},
                  i = 14;

              while (i--) uri[o.key[i]] = m[i] || "";

              uri[o.q.name] = {};
              uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
                  if ($1) uri[o.q.name][$1] = $2;
              });

              return uri;
          };

          parseUri.options = {
              strictMode: false,
              key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
              q: {
                  name: "queryKey",
                  parser: /(?:^|&)([^&=]*)=?([^&]*)/g
              },
              parser: {
                  strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                  loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
              }
          };

          function getURLParameters(sURL, paramName) {
              if (sURL.indexOf("?") > 0) {
                  var arrParams = sURL.split("?");
                  var arrURLParams = arrParams[1].split("&");
                  var arrParamNames = new Array(arrURLParams.length);
                  var arrParamValues = new Array(arrURLParams.length);

                  var i = 0;
                  for (i = 0; i < arrURLParams.length; i++) {
                      var sParam = arrURLParams[i].split("=");
                      arrParamNames[i] = sParam[0];
                      if (sParam[1] != "")
                          arrParamValues[i] = unescape(sParam[1]);
                      else
                          arrParamValues[i] = "No Value";
                  }

                  for (i = 0; i < arrURLParams.length; i++) {
                      if (arrParamNames[i] == paramName) {
                          //alert("Parameter:" + arrParamValues[i]);
                          return arrParamValues[i];
                      }
                  }
                  return "No Parameters Found";
              }
          }

          function showFacebookContent(htmlElement, feedItem) {
              console.log('Showing Facebook video...')
              htmlElement.empty();
              htmlElement.append('<div class="fb-post" data-width="500" data-href="' + feedItem.sourceLink + '"></div>')
              console.log(htmlElement.get(0))
              ezfb.XFBML.parse(htmlElement.get(0))
          }

          function showYoutubeContent(htmlElement, feedItem) {
              console.log('Showing Youtube video...')
              var sourceURL = feedItem.sourceLink;
              var imageURL = feedItem.imageURL;
              var videoId = getURLParameters(sourceURL, 'v');

              if (!videoId) {
                  if (sourceURL.split('/').length > 4) {
                      videoId = imageURL.split('%2F')[4] ? imageURL.split('%2F')[4] : videoId;
                  } else {
                      videoId = sourceURL.split('/')[3] ? sourceURL.split('/')[3] : videoId;
                  }
              }

              //var video_url = "https://www.youtube.com/watch?v=" + videoId + "&feature=youtube_gdata_player"
              htmlElement.empty();
              htmlElement.append('<iframe id="youtube-video-popup-frame" src="https://www.youtube.com/embed/' + videoId + '" width="100%" height="390" frameborder="0" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"><iframe>');
          }

          function showLinkContent(htmlElement, feedItem) {
              var width = window.innerWidth - 75;
              var height = window.innerHeight - 150;

              htmlElement.empty();
              htmlElement.append('<iframe style="overflow-y:scroll;" id="iFramelink" src=' + feedItem.sourceLink + ' width=' + width + ' height=' + height + '><iframe>');
          }

          var showContent = function (htmlElement, feedItem) {
              console.log(feedItem)
              var sourceURL = feedItem.sourceLink;
              var urlDetails = parseUri(sourceURL)

              var mediaType = feedItem.mediaType;
              var sourceType = feedItem.feedType;
              if (mediaType == 'photo') {
                  showFacebookContent(htmlElement, feedItem);
                  return {width: 300, height: 400}
              } else if (mediaType == 'video') {
                  if (sourceType == 'Facebook' && (sourceURL.indexOf("youtube") === -1 && sourceURL.indexOf("youtu.be") === -1 && sourceURL.indexOf("madovermarketing") === -1)) {
                      if (sitesToOpenInNewWindow.indexOf(urlDetails.host) == -1) {
                          showFacebookContent(htmlElement, feedItem);
                          return {width: 100, height: 300}
                      }
                      else {
                          window.open(sourceURL, 'name', 'height=600,width=500');
                      }
                  } else if (sourceType == 'Youtube' || (sourceURL.indexOf("youtube") > -1 || sourceURL.indexOf("youtu.be") > -1) || sourceURL.indexOf("madovermarketing") > -1) {
                      if (sitesToOpenInNewWindow.indexOf(urlDetails.host) == -1) {
                          showYoutubeContent(htmlElement, feedItem);
                          return {width: 100, height: 200}
                      }
                      else {
                          window.open(sourceURL, 'name', 'height=600,width=500');
                      }
                  } else {
                      console.log('unknown feedType :' + sourceType);
                  }
              } else if (mediaType == 'link') {
                  if (sitesToOpenInNewWindow.indexOf(urlDetails.host) == -1) {
                      showLinkContent(htmlElement, feedItem)
                      return {width: 800, height: 800}
                  } else {
                      window.open(sourceURL, 'name', 'height=600,width=500');
                  }

              }

          }

          if ($scope.$first && !element.parent().attr('loaded')) {
              console.log('Adding loaded attribute to parent...')
              element.parent().attr('loaded', true);
              console.log('Checking the value...after creation...')
              console.log(element.parent().attr('loaded'))
              console.log('Initialize fancybox...')
              $('.fancybox-thumbs').fancybox({
                  prevEffect: 'none',
                  nextEffect: 'none',
                  theme: 'default',
                  closeBtn: true,
                  arrows: true,
                  nextClick: true,
                  autoSize: false,

                  helpers: {
                      thumbs: {
                          width: 100,
                          height: 60,
                          margin: 20
                      }
                  },
                  afterLoad: function (fancybox) {
                      console.log(fancybox)
                      var htmlElement = angular.element('<div></div>');
                      var dimensions = showContent(fancybox.inner, $scope.feeds[fancybox.index])
/*
                      if (dimensions) {
                          fancybox.width = dimensions.width;
                          fancybox.height = dimensions.height;
                      }
*/
                      console.log(htmlElement.html());
                      this.content = htmlElement.html();
                      console.log(fancybox.index)
                      console.log(fancybox)
                  }
              });
          }

          $scope.open_fancybox2 = function () {
              console.log('Inside open_fancybox2 - ' + $scope.fancybox_loaded);
              if ($scope.fancybox_loaded == false) {
                  $scope.fancybox_loaded = true;
              } else {
                  console.log($.fancybox.group);
                  //$.fancybox.resize();
              }
          }

          $scope.open_fancybox = function () {
              var el = angular.element(element.html()),
                  compiled = $compile(el);
              console.log($.fancybox)
              $.fancybox.open({content: el});
              compiled($scope);

          };
      }
  };
}]);
