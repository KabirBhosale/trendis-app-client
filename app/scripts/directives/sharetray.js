'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:shareTray
 * @description
 * # shareTray
 */
angular.module('trendisAppClientApp')
  .directive('shareTray', ['$rootScope', '$window', 'constantService', 'shareService', 'favoriteService', function ($rootScope, $window, constantService, shareService, favoriteService) {
    return {
      templateUrl: 'views/share-tray.html',
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        feed: "=feed"
      },
      link: function postLink(scope, element, attrs) {
        scope.getShareURL = shareService.getShareURL;
        scope.shareConstants = constantService.share;

        scope.showShareContent = function (e) {
          e.stopPropagation();
        }

        scope.share = function ($event, type) {
          $event.stopPropagation();
          if ($rootScope.isdeviceapp && type === "Facebook" && parseFloat($rootScope.androidAppVersion) > 105) {//010416
            var feedsourcelink = angular.copy(scope.feed.sourceLink);
            if (scope.feed.feedType === "Youtube" || scope.feed.feedType === "youtube") {
              feedsourcelink = feedsourcelink.replace('embed', 'watch');
            }
            fbContentShareApp(feedsourcelink);
          }
          else {
            var url = shareService.getShareURL(type, scope.feed);

            if (type == "Reddit") {
              var newwindow = $window.open(url, 'name', 'height=585,width=1042');
            }
            else {
              var newwindow = $window.open(url, 'name', 'height=300,width=500');
            }
            if (newwindow.focus) {
              newwindow.focus();
            }
          }
          return false;
        }
      }
    };
  }]);
