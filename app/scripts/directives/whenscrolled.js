'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:whenScrolled
 * @description
 * # whenScrolled
 */
angular.module('trendisAppClientApp')
  .directive('whenScrolled',['$window',function ($window) {
    return function (scope, elm, attr) {
      var raw = angular.element($window)[0];

      angular.element($window).bind('scroll', function () {
        var scrollMaxY = 0
        if (raw.scrollMaxY) {
          scrollMaxY = raw.scrollMaxY
        } else {
          scrollMaxY = document.documentElement.scrollHeight - document.documentElement.clientHeight
        }

        var scrollY = 0
        if (raw.scrollY) {
          scrollY = raw.scrollY
        } else {
          scrollY = document.documentElement.scrollTop
        }

        if (scrollMaxY < scrollY + 600) {//400
          scope.next()
        }
      });

      scope.$on("$destroy", function() {
        console.log('Scope destroy called...')
        angular.element($window).unbind('scroll');
      });
    };
  }]);
