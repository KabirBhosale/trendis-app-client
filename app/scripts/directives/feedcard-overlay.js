'use strict';

/**
 * @ngdoc directive
 * @name trendisAppClientApp.directive:feedcardOverlay
 * @description
 * # feedcardOverlay
 */
angular.module('trendisAppClientApp')
  .directive('feedcardOverlay',['$rootScope', 'slideShowService', 'constantService', function ($rootScope, slideShowService, constantService) {
    return {
      templateUrl: 'views/feed-card-overlay.html',
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        feeds: "=feeds",
        feed: "=feed"
      },
      link: function postLink(scope, element, attrs) {
        scope.showSlideShow = function () {
          if($rootScope.currentVideos.length>0){
            _.each($rootScope.currentVideos,function(videoContent,videoIndex){
              if(videoContent.feedType!='Youtube'){
                var video = angular.element(videoContent.videoid);
                if(video[0]){
                  video[0].pause();
                } 
              }else{
                $rootScope.$broadcast('stopYbVideos');
              }
            });
          }
          slideShowService.doSlideShow(scope, { feeds: scope.feeds, index: scope.feed.index })
        }
        
        scope.onSuccess = function(e) {
          // console.info('Action:', e.action);
          // console.info('Text:', e.text);
          // console.info('Trigger:', e.trigger);
          alert("Copied!");    
          e.clearSelection();
        };
        scope.onError = function(e) {
          console.error('Action:', e.action);
          console.error('Trigger:', e.trigger);
          alert("Error while copy!");   
        }
      }
    };
  }]);
