/**
 * Created by KabirB on 09/18/17.
 */

function TrendisChannel(channelInfo, config) {
    this.channelInfo = channelInfo;
    this.maxItemPerPage = config.maxItemPerPage || channelInfo.maxItemPerPage;
    this.baseUrl = config.baseUrl || this.channelInfo.baseUrl
  
    this.pageNo = -1;
    this.noMoreData = false;
  
    this.country = config.country;
    this.mediaType = config.mediaType;
    this.category = config.category;
    this.subcategory = config.subcategory;// || 'all';
    this.pageType = config.pageType || 'top';
    this.networks=config.networks;
    if(this.pageType=='highlights' || this.pageType=='live'){
      this.gender=config.gender;
      this.age=config.age;
      this.url = this.baseUrl + 'feeds/top/' + this.pageType + '?country=' + this.country + '&media=' + this.mediaType.join(',') + '&gender=' + this.gender + '&age=' + this.age;
    }
    else if(this.pageType=='topHome'){
      this.preferences=config.userPreferences;
        this.url = this.baseUrl + 'feeds/' + this.pageType + '?country=' + this.country;
    }
    else if (config.searchText) {
      this.searchText=config.searchText;
      this.timelimit=config.timelimit;
      this.sortBy=config.sortBy;
      this.url = this.baseUrl + 'search?country=' + this.country + '&media=' + this.mediaType.join(',') +'&category=' + this.category + '&timelimit=' + this.timelimit+'&sortby='+this.sortBy+'&networks='+this.networks;
    }
    else{
        this.url = this.baseUrl + 'feeds/' + this.pageType + '?country=' + this.country + '&media=' + this.mediaType.join(',') + '&category=' + this.category + '&subcategory=' + this.subcategory+'&networks='+this.networks;
    }
  }
  
  TrendisChannel.prototype._getNews = function () {
    if (this.searchText) {
      return $.ajaxAsObservable({
        url: this.url + '&page=' + this.pageNo,
        data: {searchtext:this.searchText}
      });
    }else if(this.preferences){
      return $.ajaxAsObservable({
        url: this.url + '&page=' + this.pageNo,
        data: {preferences:this.preferences}
      });
    }else if (this.pageType=="live_videos") {
      return $.ajaxAsObservable({
        url: this.url + '&nextPageToken=' + this.nextPageToken,
        data: {preferences:this.preferences}
      });
    }else{
      return $.ajaxAsObservable({
        url: this.url + '&page=' + this.pageNo
      });
    }
  }
  
  TrendisChannel.prototype._extractNews = function (responseData) {
    if (responseData == undefined || responseData == "" || responseData.data == undefined) {
      this.noMoreData = true;
      return Rx.Observable.just([]);
    }
    return Rx.Observable.fromArray(responseData.data);
  }
  
  TrendisChannel.prototype._transform = function (feedItem) {
    if(feedItem.feedType){
      feedItem.feedKey = feedItem.feedType + "_" + feedItem.id;
    }
    else{//for homePage category feeds
      _.each(feedItem.feeds,function(feed){
        feed.feedKey = feed.feedType + "_" + feed.id;
      });
    }
    return feedItem;
  }
  
  
  TrendisChannel.prototype.getNextPage = function () {
    if (this.noMoreData == true) {
      return Rx.Observable.just([]);
    }
  
    this.pageNo += 1;
  
    if (this.searchText && this.pageNo>10) {//restrict to 11 pages for search
      return Rx.Observable.just([]);
    }
  
    return this._getNews()
      .flatMap(this._extractNews, null, this)
      .map(this._transform)
  }
  