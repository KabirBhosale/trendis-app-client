/**
 * Created by Ramkumar on 11/16/15.
 */
function FavoriteChannel(channelInfo, config) {
    this.channelInfo = channelInfo;
    this.config = config;

    this.albumID = config.albumID;
    this.mediaType = config.mediaType;

    this.injectDependencies();

    this.lastFetchedRecordDttm = undefined;
}

FavoriteChannel.prototype.injectDependencies = function () {
    this.userService = this.config.injector.get('userService')
    this.favoriteService = this.config.injector.get('favoriteService')
    this.$http = this.config.injector.get('$http')
}

FavoriteChannel.prototype.getNextPage = function () {
    return Rx.Observable.fromPromise(this.favoriteService.getAll())
        .flatMap(this.toArrayItems, null, this)
        .map(this.transform, this, this)
        .filter(this.filterByAlbum, this)
        .filter(this.filterByMedia, this);
}

FavoriteChannel.prototype.toArrayItems = function (favoriteItem) {
    console.log(Object.keys(favoriteItem).length);
    var array = [];
    for (variable in favoriteItem) {
        if (favoriteItem.hasOwnProperty(variable)) {
            array.push(favoriteItem[variable]);
        }
    }

    if (array.length > 0) {
        /*
        array.sort(function(a, b) {
            return a.createdTime - b.createdTime;  
        });
        */
    }

    return Rx.Observable.fromArray(array);
}

FavoriteChannel.prototype.transform = function (favoriteItem) {
    if (typeof (favoriteItem.createdDateTime) === "string") {
        favoriteItem.createdDateTime = new Date(favoriteItem.createdDateTime);
    }
    return favoriteItem;
}

FavoriteChannel.prototype.filterByMedia = function (favoriteItem) {
    if (!favoriteItem) {
        return false;
    }

    if (this.mediaType === undefined || this.mediaType === null) {
        return true;
    }

    var mediaArray = [];
    mediaArray = this.mediaType.split(",");
    for (i = 0; i < mediaArray.length; i++) {
        if (favoriteItem.mediaType && favoriteItem.mediaType === mediaArray[i])
            return true;
    }

    return false;

}

FavoriteChannel.prototype.filterByAlbum = function (favoriteItem) {
    if (!favoriteItem) {
        return false;
    }

    if (this.albumID === undefined || this.albumID === "1") {
        return true;
    }

    if (favoriteItem.albumId && favoriteItem.albumId === this.albumID) {
        return true;
    }

    return false;
}
