/**
 * Created by saran on 10/30/15.
 */
function FacebookChannel(channelInfo, config) {
    this.channelInfo = channelInfo;
    this.config = config;

    this.albumID = config.albumID;
    this.mediaType = config.mediaType;
    this.selectedfeedKey = config.selectedfeedKey;

    this.injectDependencies();

    this.accesstoken = this.userService.getAccessToken();
    //console.log(this.accesstoken);
    this.nextPageUrl = undefined;
}

FacebookChannel.prototype.injectDependencies = function () {
    this.userService = this.config.injector.get('userService')
    this.facebookService = this.config.injector.get('facebookService')
    this.favoriteService = this.config.injector.get('favoriteService')
    this.$http = this.config.injector.get('$http')
}

FacebookChannel.prototype.getNextPage = function () {
    var url = this.nextPageUrl || (this.channelInfo.baseUrl + '/me/video.saves?access_token=' + this.accesstoken);

    var req = {
        method: 'GET',
        url: url,
        headers: {
            'Authorization': 'Bearer ' + this.accesstoken
        },
        json: true
    }

    return Rx.Observable.fromPromise(this.$http(req))
        .flatMap(this._extractNews, this, this)
        .flatMap(this._getPostDetails, this, this)
        .map(this.createFavorite, this, this)
        .filter(this.filterByMedia, this)
        .filter(this.filterByAlbum, this)
    //.filter(this.filterDuplicate, this)
}

FacebookChannel.prototype.filterByMedia = function (feedItem) {
    if (!feedItem) {
        return false;
    }

    if (this.mediaType === undefined || this.mediaType === null) {
        return true;
    }

    var mediaArray = [];
    mediaArray = this.mediaType.split(",");
    for (i = 0; i < mediaArray.length; i++) {
        if (feedItem.mediaType && feedItem.mediaType === mediaArray[i])
            return true;
    }

    return false;
}

FacebookChannel.prototype.filterByAlbum = function (feedItem) {
    if (!feedItem) {
        return false;
    }

    if (this.albumID === undefined || this.albumID === "1") {
        return true;
    }

    return false;
}

FacebookChannel.prototype.createFavorite = function (feedItem) {
    this.favoriteService.contains(feedItem).then(function (item) {
        if (item === undefined) {
            this.favoriteService.add(feedItem);
        }
    }.bind(this));

    return feedItem;
}

FacebookChannel.prototype._getPostDetails = function (feed) {
    var fetchLinkObs = Rx.Observable.fromCallback(function (data, callback) {
        var postId = feed.data.other.id;
        if (feed.data.other.type == "video.other" && feed.data.other.type != undefined)
            var medType = "video";
        var feedItem = {
            feedType: "Facebook",
            sourceLink: "https://www.facebook.com/" + feed.data.other.url,
            isFacebookSaved: true,
            createdDateTime: new Date(feed.end_time),
            feedKey: "Facebook_" + feed.id + "_" + postId,
            mediaType: medType
        }

        this.facebookService.getPostDetailFromPostId(this.accesstoken, postId).then(function (response) {
            feedItem.imageURL = response.picture;
            feedItem.title = response.description;
            //feedItem.sourceLink = response.source;
            callback(feedItem)
        });
    }.bind(this));

    return fetchLinkObs(feed);
}


FacebookChannel.prototype._extractNews = function (responseData) {
    console.log('FacebookChannel response data : ');
    console.log(responseData)
    if (responseData == undefined || responseData == "" || responseData.data == undefined) {
        return Rx.Observable.just([]);
    }

    if (responseData.paging) {
        this.nextPageUrl = responseData.paging.next;
    }

    return Rx.Observable.fromArray(responseData.data.data);
}
