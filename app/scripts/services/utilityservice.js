'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.UtilityService
 * @description
 * # UtilityService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .factory('_',['$window',function ($window) {
    return $window._;
  }]);
