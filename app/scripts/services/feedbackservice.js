'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.feedbackService
 * @description
 * # feedbackService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('feedbackService',['$http', '$q', 'constantService',function ($http, $q, constantService) {
    this.submit = function (subject, message, email) {
      var deferred = $q.defer();
      var channelConfig = channelConfigs[constantService.trendisChannelName];
      var baseUrl = channelConfig.baseUrl;

      var postData = { subject: subject, message: message, email: email };

      $http.post(baseUrl + 'feedbacks', postData).then(function (response) {
        deferred.resolve(response.data)
      }, function (error) {
        if (error.status == '200') {
          deferred.reject({ status: false, error: error })
        } else {
          deferred.reject({ status: false, error: 'Internal Server error...Please retry after some time...' })
        }
      })
      return deferred.promise;
    }

  }]);