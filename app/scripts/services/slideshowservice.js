'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.slideShowService
 * @description
 * # slideShowService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('slideShowService', ['$rootScope', '$mdMedia', 'ezfb', '$compile', '_', function ($rootScope, $mdMedia, ezfb, $compile, _) {
    var $scope = undefined

    function parseUri(str) {
      var o = parseUri.options,
        m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i = 14;

      while (i--) uri[o.key[i]] = m[i] || "";

      uri[o.q.name] = {};
      uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
      });

      return uri;
    };

    parseUri.options = {
      strictMode: false,
      key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
      q: {
        name: "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
      },
      parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
      }
    };

    function getURLParameters(sURL, paramName) {
      if (sURL.indexOf("?") > 0) {
        var arrParams = sURL.split("?");
        var arrURLParams = arrParams[1].split("&");
        var arrParamNames = new Array(arrURLParams.length);
        var arrParamValues = new Array(arrURLParams.length);

        var i = 0;
        for (i = 0; i < arrURLParams.length; i++) {
          var sParam = arrURLParams[i].split("=");
          arrParamNames[i] = sParam[0];
          if (sParam[1] != "")
            arrParamValues[i] = unescape(sParam[1]);
          else
            arrParamValues[i] = "No Value";
        }

        for (i = 0; i < arrURLParams.length; i++) {
          if (arrParamNames[i] == paramName) {
            //alert("Parameter:" + arrParamValues[i]);
            return arrParamValues[i];
          }
        }
        return "No Parameters Found";
      }
    }

    function getPopupContent(feed, innerContent) {
      if ($mdMedia('xs') || $mdMedia('sm')) {
        var templateHTML = '<div id="iframe-body" style="text-align:center;min-height:inherit;max-height:' + (window.innerHeight - 50) + 'px;" class="ml-iframe-body">'
        templateHTML += innerContent
        templateHTML += '</div>'
      } else {
        var templateHTML = '<div ng-init="showTooltip=false" class="iframe-head"><div class="iframe-head-title" style="display:inline-block;">';
        templateHTML += '<h4 ng-click="showTooltip=!showTooltip" style="overlay:0;display:inline-block;" class="tools">' + feed.title + '</h4>';
        // templateHTML += '<div ng-show="showTooltip"  style="z-index: 900000;margin-left:10px;display:inline-block;font-size:13px;">{{feed.feedType}}_{{feed.id}}</div>'
        templateHTML += '<div ng-show="showTooltip" style="padding: 0 0 5px 5px;display:inline-block;"><button style="background-color: #008CBA;color: white;border-radius: 8px;padding: 5px 8px 5px 8px;border: 1px solid;" class="btn btn-info" ngclipboard ngclipboard-success="onSuccess(e);" ngclipboard-error="onError(e);" data-clipboard-text="{{feed.sourceLink}}">Copy Link</button></div>';
        templateHTML += '</div>'

        templateHTML += '<div class="iframe-head-socials text-right pull-right">'
        templateHTML += '<share feed="_feed"/>'
        templateHTML += '</div>'
        templateHTML += '</div>'
        templateHTML += '<div id="iframe-body" style="min-height:inherit;text-align:center;" class="iframe-body">'

        templateHTML += innerContent

        templateHTML += '</div>'
      }


      var newElement = angular.element(templateHTML);

      $scope._feed = feed;
      var compiled = $compile(newElement)($scope)
      return compiled;
    }

    function showFacebookContent(htmlElement, feedItem) {
      htmlElement.empty();

      var innerW = window.innerWidth;
      //var width = (innerW >= 600 ? 500 : innerW - 30);
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }
      //var width = (innerW >= 800 ? 830 : innerW - 30);

      var fbContent = "";
      var feed_load_type = "fb-post";
      if (feedItem.mediaType === "video" && feedItem.sourceLink.indexOf('/posts/') == -1) {//New KB
        fbContent = getPopupContent(feedItem, '<div id="fb-video" class="fb-video" data-autoplay="true" data-allowfullscreen="true" data-height="420" data-width="' + width + '"></div>');
        var foundElements = fbContent.find('#fb-video')
        angular.element(foundElements[0]).attr('data-href', feedItem.sourceLink);
        $rootScope.$broadcast('_remove_loading', 'Facebook', 'fb-video');
      }
      else {
        //width=650;
        fbContent = getPopupContent(feedItem, '<div id="fb-post" class="fb-post" style="height:inherit;width:' + width + 'px" data-width="' + width + '"></div>');
        var foundElements = fbContent.find('#fb-post')
        angular.element(foundElements[0]).attr('data-href', feedItem.sourceLink);
        $rootScope.$broadcast('_remove_loading', 'Facebook', 'fb-post');
      }
      htmlElement.append(fbContent);
      return { width: (width + 100), height: 800 };
    }

    function showTwitterContent(htmlElement, feedItem) {
      htmlElement.empty();
      var innerW = window.innerWidth;
      //var width = (innerW >= 600 ? 500 : innerW - 30);
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }
      var twContent = "";

      twContent = getPopupContent(feedItem, '<div style="text-align:-webkit-center;"><blockquote class="twitter-tweet" width="550" theme="dark" style="margin:0 auto;width:' + width + 'px;" data-lang="en"><p lang="en" dir="ltr"><a twitter-widget-id=' + feedItem.id.toString() + ' href=' + feedItem.sourceLink + '></a></p></blockquote></div>');

      htmlElement.append(twContent)
      twttr.widgets.load();
      twttr.events.bind('rendered', function (event) {
        $rootScope.$broadcast('_remove_loading', 'Twitter', 'iframe-body');
        twttr.events.unbind('rendered');
      });
      return { width: (width + 100), height: 800 };
      //return {width: '80%', height: '48%'};
    }

    function showInstagramContent(htmlElement, feedItem) {
      htmlElement.empty();
      var innerW = window.innerWidth;
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }
      var igContent = "";
      if (($mdMedia('xs') || $mdMedia('sm')) && feedItem.mediaType != 'video') {
        igContent = getPopupContent(feedItem, '<div style="text-align:-webkit-center;"><insta-feed-view insta-url="' + feedItem.sourceLink + '"></insta-feed-view></div>');
      } else if (feedItem.mediaType == 'video') {
        igContent = getPopupContent(feedItem, '<div class="feed-card-view"><iframe src="' + feedItem.sourceLink + 'embed" width="420" height="400" frameborder="0" style="border:1px solid grey;border-radius:5px;max-height:450px" autoplay="true" scrolling="no" allowtransparency="true"></iframe></div>');
      } else {
        igContent = getPopupContent(feedItem, '<div class="feed-card-view"><iframe src="' + feedItem.sourceLink + 'embed/" width="' + width + '" height="700px" frameborder="0" style="border:1px solid grey;border-radius:5px;min-height:100%;" autoplay="true" scrolling="no" allowtransparency="true"></iframe></div>');
      }
      htmlElement.append(igContent);
      $rootScope.$broadcast('_remove_loading', 'Instagram', 'iframe-body');
      return { width: (width + 100), height: 800 };
      //return {width: '80%', height: '48%'};
    }

    function showRedditContent(htmlElement, feedItem) {
      var sourceURL = feedItem.sourceLink;
      htmlElement.empty();
      var innerW = window.innerWidth;
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }
      var RdContent = "";
      RdContent = getPopupContent(feedItem, '<div class="feed-card-view"><blockquote class="reddit-card" width="550"  data-card-created="1490648549" style="margin:0 auto;width:' + width + 'px;" data-lang="en"><a href="' + sourceURL + '?ref=share&ref_source=embed"></a></blockquote></div>');
      htmlElement.append(RdContent);
      $rootScope.$broadcast('_remove_loading', 'Reddit', 'iframe-body');
      return { width: (width + 100), height: 800 };
    }

    function showPinterestContent(htmlElement, feedItem) {
      var sourceURL = feedItem.sourceLink;
      htmlElement.empty();
      var innerW = window.innerWidth;
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }
      var RdContent = "";
      RdContent = getPopupContent(feedItem, '<div class="feed-card-view"><blockquote class="reddit-card" width="550"  data-card-created="1490648549" style="margin:0 auto;width:' + width + 'px;" data-lang="en"><a href="' + sourceURL + '?ref=share&ref_source=embed"></a></blockquote></div>');
      htmlElement.append(RdContent);
      $rootScope.$broadcast('_remove_loading', 'Pinterest', 'iframe-body');
      return { width: (width + 100), height: 800 };
    }

    function showYoutubeContent(htmlElement, feedItem) {
      var sourceURL = feedItem.sourceLink;
      var innerW = window.innerWidth;
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }
      var imageURL = feedItem.imageURL;
      var videoId = getURLParameters(sourceURL, 'v');

      if (videoId === "No Parameters Found" || !videoId) {//updated 100316
        if (sourceURL.split('/').length > 4) {
          videoId = imageURL.split('%2F')[4] ? imageURL.split('%2F')[4] : videoId;
        } else {
          videoId = sourceURL.split('/')[3] ? sourceURL.split('/')[3] : videoId;
        }
      }

      //var video_url = "https://www.youtube.com/watch?v=" + videoId + "&feature=youtube_gdata_player"
      htmlElement.empty();
      if ($mdMedia('xs') || $mdMedia('sm')) {
        htmlElement.append(getPopupContent(feedItem, '<iframe id="youtube-video-popup-frame" class="youtube-player" src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1" width="' + width + '" height="320" frameborder="0" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>'));
      } else {
        htmlElement.append(getPopupContent(feedItem, '<iframe id="youtube-video-popup-frame" class="youtube-player" src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1" width="100%" height="450" frameborder="0" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>'));
      }

      //return {width: 900, height: 500};
      return { width: (width + 100), height: 800 };
      //return {width: '80%', height: '48%'};
    }

    function showLinkContent(htmlElement, feedItem) {
      var width = window.innerWidth - 75;
      //var height = window.innerHeight - 150;
      var innerW = window.innerWidth;
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }

      htmlElement.empty();
      htmlElement.append(getPopupContent(feedItem, '<iframe style="overflow-y:scroll;" id="iFramelink" src=' + feedItem.sourceLink + ' width=' + width + ' height=' + height + '><iframe>'));

      return { width: (width + 100), height: 800 };
    }

    function showContentInNewWindow(htmlElement, feedItem) {
      htmlElement.empty();
      var srclink = feedItem.sourceLink;
      var innerW = window.innerWidth;
      var width = (innerW >= 768 ? 668 : innerW - 30);
      if (innerW > 768) {
        width = 668;
      } else if (innerWidth >= 600) {
        width = 500;
      } else {
        width = innerW - 30;
      }

      if (feedItem.mediaType == "link" || feedItem.mediaType == "video") {//New KB
        htmlElement.empty();
        htmlElement.append(getPopupContent(feedItem, '<img class="img-responsive" ng-click=openlink() style="cursor:pointer;max-height:440px;width:100%" ng-src=' + feedItem.imageURL + '></img>'));
      }
      else {
        htmlElement.append(getPopupContent(feedItem, 'Opened in new window'));
      }
      if (feedItem.feedType == "Youtube" && feedItem.mediaType == "video") {
        srclink = srclink.replace('embed', 'watch');
      }
      window.open(srclink, new Date(), 'height=650,width=' + width + ',scrollbars=1');
      //return {width: 600, height: 300}
      //return {width: '80%', height: '48%'};

      return { width: (width + 100), height: 800 };
    }


    var showContent = function (htmlElement, feedItem) {
      var sourceURL = feedItem.sourceLink;
      var urlDetails = parseUri(sourceURL)

      var mediaType = feedItem.mediaType;
      var sourceType = feedItem.feedType;
      if (mediaType == 'photo') {
        if (sourceType == 'Facebook') {
          return showFacebookContent(htmlElement, feedItem);
        } else if (sourceType == 'Twitter') {
          return showTwitterContent(htmlElement, feedItem);
        } else if (sourceType == 'Instagram') {
          return showInstagramContent(htmlElement, feedItem);
        } else if (sourceType == 'Pinterest') {
          return showPinterestContent(htmlElement, feedItem);
        } else {
          return showRedditContent(htmlElement, feedItem);
        }
      } else if (mediaType == 'video') {
        if (sourceType == 'Facebook') {
          if (feedItem.is_newWindow && feedItem.is_newWindow == true) {
            return showContentInNewWindow(htmlElement, feedItem);
          } else if (sourceURL.indexOf("youtube") > -1 || sourceURL.indexOf("youtu.be") > -1) {
            return showYoutubeContent(htmlElement, feedItem);
          }
          else {
            return showFacebookContent(htmlElement, feedItem);
          }
        }
        else if (sourceType == 'Youtube' || (sourceURL.indexOf("youtube") > -1 || sourceURL.indexOf("youtu.be") > -1) || sourceURL.indexOf("madovermarketing") > -1) {
          if (feedItem.is_newWindow && feedItem.is_newWindow == true) {
            return showContentInNewWindow(htmlElement, feedItem);
          } else {
            return showYoutubeContent(htmlElement, feedItem);
          }
        } else if (sourceType == 'Twitter') {
          return showTwitterContent(htmlElement, feedItem);
        } else if (sourceType == 'Instagram') {
          return showInstagramContent(htmlElement, feedItem);
        } else if (sourceType == 'Reddit') {
          return showRedditContent(htmlElement, feedItem);
        } else if (sourceType == 'Pinterest') {
          return showPinterestContent(htmlElement, feedItem);
        }
        else {
          console.error('unknown feedType :' + sourceType);
        }
      } else if (mediaType == 'link') {
        return showContentInNewWindow(htmlElement, feedItem);//added all links open in new window-KB
      }

    }
    //new KB
    this.showLinkContentInNewWindow = function () {
      var srclink = $scope.currentfeed.sourceLink;
      if ($scope.currentfeed.feedType == "Youtube" && $scope.currentfeed.mediaType == "video") {
        srclink = srclink.replace('embed', 'watch');
      }
      window.open(srclink, new Date(), 'height=650,width=1000,scrollbars=1');
      return { width: 600, height: 300 }
    }
    //end

    this.viewFeed = function (scope, containerElement) {
      $scope = scope;
      showContent(containerElement, scope.feed);
    }
    this.doSlideShow = function (scope, detail) {
      if ($rootScope.isdeviceapp == true) {//open feed in new window for device app
        showContent(undefined, scope.feed);
      }
      else {
        $scope = scope;
        _.forEach(detail.feeds, function (feed) {
          if (feed) {
            feed.href = feed.imageURL;
            feed.type = 'html';
            feed.group = 'thumb';
          }
        })


        $.fancybox.open(detail.feeds, {
          prevEffect: 'none',
          nextEffect: 'none',
          theme: 'default',
          closeBtn: true,
          arrows: true,
          nextClick: true,
          autoSize: false,
          keys: {
            next: {
              34: "up",
              39: "left"
            },
            prev: {
              33: "down",
              37: "right"
            }
          },
          helpers: {
            thumbs: {
              width: 100,
              height: 60,
              margin: 20,
              source: function (feed) {
                return feed.imageURL != 'NA' ? feed.imageURL : feed.prof_img_url;
              }
            }
          },
          padding: [0, 15, 0, 15],
          index: detail.index - 1,
          beforeLoad: function () {
            this.title = ''
          },
          afterShow: function () {
            // $('#fancybox-thumbs').swipe({
            //   swipe: function (event, direction) {
            //     if (direction === 'left' || direction === 'up') {
            //       //$.fancybox.prev(direction);
            //       $('.fancybox-thumb-next').trigger('click');
            //     } else {
            //       //$.fancybox.next(direction);
            //       $('.fancybox-thumb-prev').trigger('click');
            //     }
            //   },
            //   excludedElements: ".nav a"
            // });
          },
          afterLoad: function (fancybox) {
            var feed = detail.feeds[fancybox.index];
            //console.log(fancybox.index+" : "+feed);
            $scope.currentfeed = feed;
            var dimensions = showContent(fancybox.inner, feed)
            if (dimensions) {
              fancybox.width = dimensions.width;
              fancybox.height = dimensions.height;
            }
          }
        });
      }
    }
  }]);
