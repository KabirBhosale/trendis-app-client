'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.albumService
 * @description
 * # albumService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('albumService', ['$rootScope', '$http', 'userService', 'storageService', 'constantService', function ($rootScope, $http, userService, storageService, constantService) {
    var STORAGE_CATEGORY = constantService.albums.category;
    var associatedUserID = gDeviceID;

    this.add = function (item) {
      storageService.set(STORAGE_CATEGORY, item.albumId, item);
      putItem(item);
    }

    this.remove = function (item) {
      storageService.remove(STORAGE_CATEGORY, item.albumId);
      deleteItem(item.albumId);
    }

    this.removeById = function (albumId) {
      storageService.remove(STORAGE_CATEGORY, albumId);
      deleteItem(albumId);
    }

    this.getAll = function () {
      return storageService.getAll(STORAGE_CATEGORY);
    }

    this.contains = function (item) {
      return storageService.get(STORAGE_CATEGORY, 'albumId', item.albumId);
    }

    this.migrateUserAlbums = function (oldAssociatedUserID, newAssociatedUserID) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var req = baseUrl + 'albums/migrate?associatedUserID=' + newAssociatedUserID +
        '&oldAssociatedUserID=' + oldAssociatedUserID;
      $http.get(req, { withCredentials: true }).then(function (response) {
        console.log(response.data);
      }, function (error) {
        console.log(error);
      });
    };

    this.pullFromServer = function (associatedUserID) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var req = baseUrl + 'albums/user?associatedUserID=' + associatedUserID;
      _getAlbums(req);
    };

    function _getAlbums(requestURI) {
      $http.get(requestURI, { withCredentials: true }).then(
        function (response) {
          if (response.data === undefined || response.data.error !== undefined) {
            return;
          }

          _.forEach(response.data, function (albumData) {
            storageService.set(STORAGE_CATEGORY, albumData.albumId, albumData.album);
          });
          storageService.set(constantService.storageDefaultCategory, "lastSynchTime_album", Date.now());
        },
        function (error) {
          console.log(error);
        }
      );
    }

    function getAlbums(lastSynchTime) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var req = baseUrl + 'albums?associatedUserID=' + associatedUserID +
        '&lastSynchTime=' + lastSynchTime;
      _getAlbums(req);
    }

    function putItem(album) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      if (!userService.isLoggedIn()) {
        associatedUserID=gDeviceID;
      }
      var reqData = {
        associatedUserID: associatedUserID,
        album: album
      };

      $http.post(baseUrl + 'albums/', reqData).then(
        function (response) {
          if (response.data === undefined || response.data.error !== undefined) {
            return;
          }
        },
        function (error) {
          console.log(error);
        }
      );
      storageService.set(constantService.storageDefaultCategory, "lastSynchTime_album", Date.now());
    }

    function deleteItem(albumId) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var reqData = {
        associatedUserID: associatedUserID,
        albumId: albumId
      };

      $http.post(baseUrl + 'albums/delete', reqData).then(
        function (response) {
          if (response.data === undefined || response.data.error !== undefined) {
            return;
          }
        },
        function (error) {
          console.log(error);
        }
      );
    }

    this.init = function () {
      var lastSynchTime = -1;
      // This event was needed since during synchup with server, the cache was not 
      // getting updated due to asychronous nature of cache initialization &
      // synch from server. To avoid it, we are initiating synch only after
      // cache initialization. Still there is an issue of icon not getting
      // highlighted on first load of page when the page contents gets drawn first
      // before completion of synch.
      $rootScope.$on("category_cache_initialized", function (event, value) {
        if (value === STORAGE_CATEGORY) {
          getAlbums(lastSynchTime);
        }
      });


      storageService.get(constantService.storageDefaultCategory, "deviceID", "deviceID").then(function (item) {
        if (item && item != "") {
          associatedUserID = item;
        } else {
          // To use the same deviceID, we are storing the fingerprint in localstorage
          // If we take everytime by calculating, there is a slight chance that the
          // fingerprint will change (when browser is upgraded, new system fonts installed, etc.,))
          storageService.set(constantService.storageDefaultCategory, "deviceID", gDeviceID);
        }
      });
      storageService.get(constantService.storageDefaultCategory, "lastLoggedInID", "lastLoggedInID").then(function (item) {
        if (item) {
          // if lastLoggedInID available then use it as the associatedUserID for synching
          associatedUserID = item;
        }
        storageService.get(constantService.storageDefaultCategory, "lastSynchTime_album", "lastSynchTime_album").then(
          function (item) {
            lastSynchTime = (item ? item : -1);
          }
        );
      });

      $rootScope.$on('logged_into_server', function (event, status) {
        if (userService.isLoggedIn()) {
          storageService.set(constantService.storageDefaultCategory, "lastLoggedInID", userService.id);
          var oldAssociatedUserID = associatedUserID;
          associatedUserID = userService.id;

          // pull from server the information related to new userID
          // if (oldAssociatedUserID !== associatedUserID) {
          //   this.pullFromServer(associatedUserID); 
          // }
          this.pullFromServer(associatedUserID);
          // inform server to migrate old userID information to new one
          //this.migrateUserAlbums(oldAssociatedUserID, associatedUserID);
        } else {
          // on logout
        }
      }.bind(this));
    };

    this.init();
  }]);
