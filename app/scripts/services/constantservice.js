'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.constantService
 * @description
 * # constantService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('constantService', function () {
    this.menuConfigs = menuConfigs;

    this.storageDefaultCategory = "_";
    this.storageDefaultFieldName = "name";
    this.defaultFeedType = 'all';
    this.defaultNetworkType = "all";
    this.storageMediaTypeKey = "mediatype";
    this.searchCategory = "search";
    this.searchSubcategory = "all";
    this.storageSearchMediaTypeKey = "searchmediatype";
    this.storageFeedLikesKey="likes";
    this.storageSearchCountries = "searchcountry";
    this.storageNetworkKey = "network";
    this.storageSearchNetworkKey = "searchnetwork";
    this.trendisWebVersion = {
      name: "web-version",
      value: 1.1
    }

    this.storageKeys = {
      COUNTRY: 'COUNTRY',
      LOCALITY: 'LOCALITY'
    }

    this.country = {
      US: 'US',
      IN: 'IN'
    }

    this.mediaType = {
      photo: 'photo',
      video: 'video',
      link: 'link',
      page: 'page'
    }

    this.feedTypes = {
      Facebook: 'Facebook',
      Youtube: 'Youtube',
      Twitter: 'Twitter',
      Instagram: 'Instagram',
      Reddit: 'Reddit'
    }

    this.share = {
      Facebook: 'Facebook',
      Twitter: 'Twitter',
      WhatsApp: 'WhatsApp',
      Reddit: 'Reddit',
      Mail: 'Mail'
    }

    this.defaultCountry = this.country.US;
    this.defaultMedias=['photo','video'];
    this.defaultFbUserProp={
      gender:'male',age:21
    }

    this.maxItemPerPage = 15;

    this.favorites = {
      category: 'favorites'
    }

    this.likes = {
      category: 'likes'
    }

    this.albums = {
      category: 'albums'
    }

    this.trendisChannelName = 'trendis';

    this.defaultCategory = 'humor';
    this.defaultSubCategory = 'all';

    this.settings = {
      minSubCategory: 2,
      minPersonalize: 6,
      storageKey: 'user_preferences'
    }

    this.homeScreenFeedCategories={
      IN : [
        { category: "humor", subCategories: ["all"] },
        { category: "sports", subCategories: ["cricket"] },
        { category: "entertainment", subCategories: ["movies"] },
        { category: "infotainment", subCategories: ["interesting"] },
        { category: "lifestyle", subCategories: ["health","food","travel","beaty"] }
      ],
      US : [
        { category: "humor", subCategories: ["all"] },
        { category: "sports", subCategories: ["all"] },
        { category: "entertainment", subCategories: ["all"] },
        { category: "lifestyle", subCategories: ["health","food"] },
        { category: "infotainment", subCategories: ["dailynews","tech","interesting"] }        
      ]
    }

    this.homeDefaultCategories={
      IN :[
        { id: 'movies', categoryId: 'entertainment', subcategoryId: 'movies', name: 'Movies'},
        { id: 'humor', categoryId: 'humor', subcategoryId: 'all', name: 'Humor'},
        { id: 'cricket', categoryId: 'sports', subcategoryId: 'cricket', name: 'Cricket'},
        { id: 'interesting', categoryId: 'infotainment', subcategoryId: 'interesting', name: 'Interesting'},
        { id: 'food', categoryId: 'lifestyle', subcategoryId: 'food', name: 'Food'},
        { id: 'travel', categoryId: 'lifestyle', subcategoryId: 'travel', name: 'Travel'},
        { id: 'health', categoryId: 'lifestyle', subcategoryId: 'health', name: 'Health'},
        { id: 'beauty', categoryId: 'lifestyle', subcategoryId: 'beauty', name: 'Beauty'}        
      ],
      US :[
        { id: 'humor', categoryId: 'humor', subcategoryId: 'all', name: 'Humor'},
        { id: 'dailynews', categoryId: 'infotainment', subcategoryId: 'dailynews', name: 'Daily News'},
        { id: 'sports', categoryId: 'sports', subcategoryId: 'all', name: 'Sports'},
        { id: 'tech', categoryId: 'infotainment', subcategoryId: 'tech', name: 'Technology'},
        { id: 'health', categoryId: 'lifestyle', subcategoryId: 'health', name: 'Health'},
        { id: 'interesting', categoryId: 'infotainment', subcategoryId: 'interesting', name: 'Interesting'},
        { id: 'entertainment', categoryId: 'entertainment', subcategoryId: 'all', name: 'Entertainment'},
        { id: 'food', categoryId: 'lifestyle', subcategoryId: 'food', name: 'Food'}
      ]
    }

    this.defaultProfilePicture = "images/avtar.png"
  });
