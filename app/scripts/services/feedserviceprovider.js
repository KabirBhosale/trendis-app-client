'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.feedServiceProvider
 * @description
 * # feedServiceProvider
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('feedServiceProvider', ['constantService', '_', '$q', '$injector', 'pipeService', 'countryService', function (constantService, _, $q, $injector, pipeService, countryService) {
    function _getChannelInstance(type, channelConfig) {
      var channelInfo = channelConfigs[type];
      if (channelInfo == undefined) {
        return channelInfo;
      }

      return channelInfo.create(channelInfo, channelConfig);
    }

    function FeedService(config) {
      this.config = config;
      var maxItemPerPage = config.maxItemPerPage || constantService.maxItemPerPage;

      this.channelInstances = _.map(config.channels, function (channelConfig) {
        channelConfig.maxItemPerPage = channelConfig.maxItemPerPage || maxItemPerPage;
        channelConfig.injector = $injector;
        return {
          instance: _getChannelInstance(channelConfig.type, channelConfig),
          config: channelConfig,
          pipe: channelConfig.pipe  //TODO: Not used as of now
        }
      })

      if (config.pipes !== undefined && config.pipes.length > 0) {
        this.pipes = _.map(config.pipes, function (pipeConfig) {
          var pipeCreator = pipeService[pipeConfig.name];
          if (pipeCreator) {
            return pipeCreator(pipeConfig.config)
          }
        })
      }
    }

    FeedService.prototype.getNextPage = function () {
      var channelObservables = _.map(this.channelInstances, function (channelInstanceInfo) {
        return channelInstanceInfo.instance.getNextPage()
      })

      var deferred = $q.defer();
      var allData = [];
      var locality_country = countryService.getLocalityCountry();
      var currentPageType = this.channelInstances[0].instance.pageType;
      var currentCategory = this.channelInstances[0].instance.category;
      var currentSubcategory = this.channelInstances[0].instance.subcategory;
      var currentPageNo = this.channelInstances[0].instance.pageNo;
      var currentCountry = this.channelInstances[0].instance.country;
      var mediatype = this.channelInstances[0].instance.mediaType;
      var lsKey = currentCountry + "_" + currentPageType + "_" + currentCategory + "_" + currentSubcategory + "_" + mediatype;
      Rx.Observable.mergeDelayError(channelObservables)
        .subscribe(function (data) {
          if (this.pipes && currentPageType !== "autoComplete") {
            _.map(this.pipes, function (pipe) {
              if (data) {
                data = pipe(data, currentCategory + "_" + currentSubcategory);
                // if (currentPageType === "topHome") {
                //   _.map(data.feeds, function (feed, index) {
                //     if (feed) {
                //       feed = pipe(feed, data.category + "_" + data.subcategory);
                //     } else {//removed country restricted feed
                //       data.feeds.splice(index, 1);
                //     }
                //   });
                //   data = data;
                // } else {
                //   data = pipe(data, currentCategory + "_" + currentSubcategory);
                // }
              }
            })
          }
          if (data) {
            allData.push(data);
          } else {
            //undefined : country restricted feed excluded
          }

        }.bind(this), function (error) {
          console.log(error);
          deferred.resolve({ data: allData, error: error, pageNo: currentPageNo });
        }, function () {
          
          deferred.resolve({ data: allData, pageNo: currentPageNo });
        });

      return deferred.promise;
    }

    return {
      create: function (config) {
        return new FeedService(config);
      }
    }
  }]);
