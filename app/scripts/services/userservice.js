'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.userService
 * @description
 * # userService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('userService', ['$rootScope', '$location', 'ezfb', '$q', '$http', 'constantService', 'storageService','_','$route', function ($rootScope, $location, ezfb, $q, $http, constantService, storageService,_,$route) {
    this.loginStatus = undefined;
    this.userName = 'Guest';
    this.id = undefined;
    this.picture = undefined;
    this.userEmail = undefined;
    this.gender=undefined;
    this.age=undefined;
    this.trendisAccessToken = "";
    this.score = 0;
    var type = typeof (AndroidFunction);
    if (type !== "undefined") {//010416
      $rootScope.isdeviceapp = true;
    }

    this.login = function () {
      ezfb.login(function (res) {
        if (res.authResponse) {
          this.updateLoginStatus();
        }
      }.bind(this), { scope: 'email' });//,user_likes,publish_actions
    };

    this.logout = function () {
      ezfb.logout(function (res) {
        this.trendisAccessToken = "";
        this.gender=undefined;
        this.age=undefined;
        clearLocalData();
        this.updateLoginStatus();
        $location.url("/");//redirect to login screen
        $route.reload();
      }.bind(this))
    };

    this.updateLoginStatus = function () {
      upgradeAlbumsAndFavoriets().then(function () {
        ezfb.getLoginStatus(function (res) {
          this.loginStatus = res;
          console.log('Login status...');
          console.log(res);
          if (this.isLoggedIn()) {//,gender,age_range
            ezfb.api('/me?fields=id,picture,name,email').then(function (me) {
              console.log(me);
              this.setFBUserProperties(me);
            }.bind(this));
          } else {
            this.setFBUserProperties(undefined);
          }
        }.bind(this));
      }.bind(this), function (err) {
        ezfb.getLoginStatus(function (res) {
          this.loginStatus = res;
          console.log('Login status...');
          console.log(res);
          if (this.isLoggedIn()) {//,gender,age_range
            ezfb.api('/me?fields=id,picture,name,email').then(function (me) {
              console.log(me);
              this.setFBUserProperties(me);
            }.bind(this));
          } else {
            this.setFBUserProperties(undefined);
          }
        }.bind(this));
      }.bind(this))
    }

    this.setFBUserProperties = function (meResObj) {
      if (meResObj) {
        this.userName = meResObj.name;
        this.id = meResObj.id;
        if (meResObj.picture && meResObj.picture.data && meResObj.picture.data.url) {
          this.picture = meResObj.picture.data.url;
        }
        if (meResObj.email) {
          this.userEmail = meResObj.email;
        }
        if(meResObj.gender && meResObj.gender!=''){
          this.gender=meResObj.gender;
        }
        if(meResObj.age_range && meResObj.age_range.min && meResObj.age_range.min!=''){
          this.age=meResObj.age_range.min;
        }

        // Create the user
        loginToServer(this);
      } else {
        this.userName = 'Guest';
        this.id = undefined;
        this.picture = undefined;
        this.score = 0;
        logoutFromServer(this);
      }
      $rootScope.$emit('login_status_change', "");
      //_setAccessToken(this.getAccessToken(), this.id);//index.html script call
    }

    this.isLoggedIn = function () {
      return (this.loginStatus !== undefined && this.loginStatus.status === 'connected');
    }

    this.getUserFbProperties=function(){
      var properties={};
      if(this.id){
        properties.id=this.id;
      }
      if(this.userName){
        properties.userName=this.userName;
      }
      if(this.picture){
        properties.picture=this.picture;
      }
      if(this.userEmail){
        properties.userEmail=this.userEmail;
      }
      if(this.gender){
        properties.gender=this.gender;
      }
      if(this.age){
        properties.age=this.age;
      }

      return properties;
    }

    this.getAccessToken = function () {
      if (!this.isLoggedIn()) {
        return undefined;
      }

      return this.loginStatus.authResponse.accessToken;
    }

    if (!$rootScope.isdeviceapp) {//010416
      this.updateLoginStatus();
    }
    //setRootScope($rootScope);

    function loginToServer(userService) {
      var channelConfig = channelConfigs[constantService.trendisChannelName];
      var baseUrl = channelConfig.baseUrl;
      var postData = {
        deviceId: gDeviceID,
        userId: userService.id,
        name: userService.userName,
        email: userService.userEmail,
        accessToken: userService.getAccessToken()
      };

      storageService.get(constantService.storageDefaultCategory, constantService.settings.storageKey, constantService.settings.storageKey).then(function (res) {
        if (res) {
          postData.preferences = res.preferences;
          postData.personalized = res.personalized;
        }
        $http.post(baseUrl + 'user/login', postData, { withCredentials: true }).then(
          function (response) {
            if (response.data == undefined || response.data.error != undefined) {
              return;
            }
            userService.trendisAccessToken = response.data.accessToken;
            userService.score = (response.data.score ? response.data.score : 0);
            if (response.data.preferences.length > 0) {//save preferences locally
              storageService.set(constantService.storageDefaultCategory, constantService.settings.storageKey, { preferences: response.data.preferences, personalized: response.data.personalized })
            }
            $rootScope.$emit('logged_into_server', "");
          },
          function (error) {
            console.log(error);
          }
        );
      });
    }

    function logoutFromServer(userService) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      $http.get(baseUrl + 'user/logout', { withCredentials: true }).then(
        function (response) {
          if (response.data == undefined || response.data.error != undefined) {
            return;
          }

          userService.trendisAccessToken = "";
        },
        function (error) {
          console.log(error);
        }
      );
    }

    // Called only if our webapp is loaded thru the inappbrowser of
    // nativeApp. Required since in nativeApp the facebook login window
    // opens in a new webview and hence our page which triggered
    // login doesn't have a way to know that login has happened
    $rootScope.$on('check_status', function (event, status) {
      FB.init({
        appId: channelConfigs.facebook.appID,
        status: true,
        cookie: true,
        xfbml: true,
        version: "v2.6"
      });

      this.updateLoginStatus();
    }.bind(this));

    this.getPreferences = function () {
      return storageService.get(constantService.storageDefaultCategory, constantService.storageDefaultFieldName, constantService.settings.storageKey)
    }

    this.savePreferences = function (userPreferences) {
      var deferred = $q.defer();

      storageService.set(constantService.storageDefaultCategory, constantService.settings.storageKey, userPreferences)
        .then(function (data) {
          if (this.isLoggedIn()) {
            //TODO HTTP call to save it in server
            var channelConfig = channelConfigs[constantService.trendisChannelName];
            var baseUrl = channelConfig.baseUrl;
            var postData = {
              userId: this.id,
              deviceId: gDeviceID,
              preferences: userPreferences.preferences,
              personalized: userPreferences.personalized,
              source: 'website'
            };

            $http.post(baseUrl + 'user/save_details', postData, { withCredentials: true }).then(function (response) {
              console.log('Preferences saved on server :' + response);
              deferred.resolve(data);
            }, function (error) {
              console.log('Save preferences to server failed!');
              console.log(error);
              deferred.resolve(data);
            });
          } else {
            deferred.resolve(data);
          }
        }.bind(this), function (error) {
          deferred.reject(error)
        });

      return deferred.promise;
    }

    this.suggestCategory = function (category, subCategory) {
      var deferred = $q.defer();
      var channelConfig = channelConfigs[constantService.trendisChannelName];
      var baseUrl = channelConfig.baseUrl;

      $http.post(baseUrl + 'suggests', { category: category, subCategory: subCategory }).then(function (response) {
        deferred.resolve(response.data)
      }, function (error) {
        if (error.status == '200') {
          deferred.reject({ status: false, error: error })
        } else {
          deferred.reject({ status: false, error: 'Internal Server error...Please retry after some time...' })
        }
      })

      return deferred.promise;
    }

    //clear data on logout
    function clearLocalData() {
      storageService.removeCategory(constantService.albums.category).then(function (res) {
        console.log('local albums cleared!')
      });
      storageService.removeCategory(constantService.favorites.category).then(function (res) {
        console.log('local favorites cleared!')
      });
      var localStorageKeys = ["user_preferences", "lastLoggedInID", "lastSynchTime", 'lastSynchTime_album'];
      _.forEach(localStorageKeys, function (key, index) {
        storageService.remove(constantService.storageDefaultCategory, key).then(function (res) {
          console.log('local ' + key + ' cleared!');
          // if(index==localStorageKeys.length-1){
          //     $location.url("/");
          // }
        });
      });
    }
    //upgrade albums and favorites
    function upgradeAlbumsAndFavoriets() {
      var deferred = $q.defer();
      storageService.get(constantService.storageDefaultCategory, constantService.trendisWebVersion.name, constantService.trendisWebVersion.name).then(function (version) {
        if ((!version && version == undefined) || version == 1) {
          //update localstorage keys : remove 'localstorage' keys
          storageService.removeCategory("localstorage");
          storageService.removeCategory("localStorage");
          upgradeAlbums().then(function (albumObj) {
            upgradeFavorites(albumObj).then(function (res) {
              storageService.set(constantService.storageDefaultCategory, constantService.trendisWebVersion.name, constantService.trendisWebVersion.value)
              console.log('all upgradation process complete!');
              deferred.resolve('done');
            });
          });
        } else {
          deferred.resolve('upgraded!');
        }
      });

      return deferred.promise;
    }

    function upgradeAlbums() {
      var deferred = $q.defer();
      var STORAGE_CATEGORY_ALBUMS = constantService.albums.category;
      var albumObj = {};
      storageService.getAll(STORAGE_CATEGORY_ALBUMS).then(function (albums) {
        var cnt = 0;
        if (!albums || Object.keys(albums).length == 0) {
          console.log('albums not found!');
          deferred.resolve(albumObj)
        }
        var lstItem = [Object.keys(albums)[Object.keys(albums).length - 1]];
        angular.forEach(albums, function (item, album_id) {
          cnt++;
          var albumName = item.albumName || item.AlbumName;
          var newAlbumId = albumName.trim().replace(/ /g, '').toLowerCase();
          albumObj[albumName.toLowerCase().trim()] = newAlbumId;
          albumObj[album_id.trim()] = albumName;
          storageService.remove(STORAGE_CATEGORY_ALBUMS, item.albumId).then(function (res) {
            var album = {
              id: newAlbumId,
              albumId: newAlbumId,
              albumStore_id: newAlbumId + '_1',
              albumName: item.albumName,
              title: item.albumName
            }
            storageService.set(STORAGE_CATEGORY_ALBUMS, album.albumId, album);

            if (lstItem == album_id) {
              console.log('All album upgradation done!');
              deferred.resolve(albumObj)
            }
          });
        });
      }, function (err) {
        console.log('All album upgradation err ->' + err);
        deferred.resolve('err' + err)
      });
      return deferred.promise;
    }

    function upgradeFavorites(albumObj) {
      var deferred = $q.defer();
      var STORAGE_CATEGORY_FAV = constantService.favorites.category;
      storageService.getAll(STORAGE_CATEGORY_FAV).then(function (favorites) {
        var cnt = 0;
        if (!favorites || Object.keys(favorites).length == 0) {
          console.log('favorites not found!');
          deferred.resolve('empty!');
        }
        var lstItem = [Object.keys(favorites)[Object.keys(favorites).length - 1]];
        angular.forEach(favorites, function (item, feedKey) {
          cnt++;
          if (item.albumId || item.albumName) {
            storageService.remove(STORAGE_CATEGORY_FAV, item.feedKey).then(function (res) {
              if (item.albumId && albumObj[item.albumId]) {
                item.albumName = albumObj[item.albumId];
                item.albumId = albumObj[item.albumName.toLowerCase().trim()];
              }
              else if (item.albumName && albumObj[item.albumName.toLowerCase().trim()]) {
                item.albumId = albumObj[item.albumName.toLowerCase().trim()];
              } else {
                console.log('AlbumId OR albumName not found!')
              }
              storageService.set(STORAGE_CATEGORY_FAV, item.feedKey, item).then(function () {
                if (lstItem == feedKey) {
                  console.log('All favorites upgradation done!' + cnt + '-------' + Object.keys(favorites).length);
                  deferred.resolve('done')
                }
              });
            });
          } else {
            console.log('all album feed!')
            if (lstItem == feedKey) {
              console.log('All favorites upgradation done!');
              deferred.resolve('done')
            }
          }
        });
      }, function (err) {
        console.log('All album upgradation err ->' + err);
        deferred.resolve('err' + err)
      });
      return deferred.promise;
    }

    //response from app for login
    $rootScope.$on('fbAuhResponseUpdate', function (event, resObj) {
      if (resObj.accessToken) {
        this.loginStatus = { "status": "connected", "authResponse": { "accessToken": resObj.accessToken } };
        console.log(this.loginStatus);
        this.setFBUserProperties(resObj);
      } else {
        this.loginStatus = undefined;
        this.trendisAccessToken = "";
        this.setFBUserProperties(undefined);
      }
    }.bind(this));
  }]);
