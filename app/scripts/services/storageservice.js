'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.storageService
 * @description
 * # storageService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('storageService',['$q', '$rootScope', 'localStorageService', '$indexedDB','_', function ($q, $rootScope, localStorageService, $indexedDB,_) {
    var localStoreInstance = {
      _instance: localStorageService,
      cache: false,

      get: function (category, fieldName, key) {
        return $q.when(this._instance.get(category + "_" + key));
      },

      set: function (category, key, value) {
        return $q.when(this._instance.set(category + "_" + key, value));
      },

      remove: function (category, key) {
        return $q.when(this._instance.remove(category + "_" + key));
      },

      removeCategory: function (category) {
        var categoryRegEx = new RegExp("^" + category + "_");
        return $q.when(this._instance.clearAll(categoryRegEx));
      }
    };

    var indexedDBInstance = {
      _instance: $indexedDB,
      _indexConfig: {},
      _keyConfig: {},
      cache: false,

      init: function () {
        _.forEach(storeConfigs, function (storeConfig, categoryName) {
          if (storeConfig.storeType == 'indexedDB') {
            var schema = storeConfig.schema;
            if (schema && schema.definition && schema.definition.properties && schema.definition.properties.keyPath) {
              this._keyConfig[categoryName] = schema.definition.properties.keyPath;
            }
            if (schema && schema.indexes) {
              _.forEach(schema.indexes, function (indexConfig) {
                this._indexConfig[categoryName + '_' + indexConfig.column] = indexConfig.name;
              }.bind(this))
            }
          }
        }.bind(this))
      },

      _getFieldIndex: function (category, fieldName) {
        var key = category + '_' + fieldName;
        return this._indexConfig[key];
      },

      get: function (category, fieldName, key) {
        var indexName = this._getFieldIndex(category, fieldName);
        if (indexName == undefined) {
          return this._instance.openStore(category, function (indexedDBStore) {
            return indexedDBStore.find(key)
          })
        } else {
          return this._instance.openStore(category, function (indexedDBStore) {
            return indexedDBStore.findBy(indexName, key)
          })
        }
      },

      _getKeyField: function (category) {
        return this._keyConfig[category];
      },

      getAll: function (category) {
        var keyField = this._getKeyField(category)
        if (keyField == undefined) {
          return $q.reject('Key field not found for ' + category)
        }

        return this._instance.openStore(category, function (indexedDBStore) {
          var deferred = $q.defer();

          indexedDBStore.getAll().then(function (items) {
            console.log('getAll count : '+items.length)
            var result = {};
            _.forEach(items, function (item) {
              result[item[keyField]] = item;
            });
            deferred.resolve(result)
          }, function (error) {
            deferred.resolve(error);
          })

          return deferred.promise;
        });

      },

      set: function (category, key, value) {
        return this._instance.openStore(category, function (indexedDBStore) {
          return indexedDBStore.insert(value)
        })
      },
      
      update: function (category, key, value) {
          return this._instance.openStore(category, function (indexedDBStore) {
            return indexedDBStore.upsert(value)
          })
        },

      remove: function (category, key) {
        return this._instance.openStore(category, function (indexedDBStore) {
          return indexedDBStore.delete(key)
        })

        //return $q.when(this._instance.remove(category + "_" + key));
      },

      removeCategory: function (category) {
        _cache = {};
        return this._instance.openStore(category, function (indexedDBStore) {
          return indexedDBStore.clear();
          // return indexedDBStore.clear().then(function(){
          //   // do something
          // },function(err){alert(err)});
        })
      }
    }

    var DEFAULT_STORE_INSTANCE = localStoreInstance;

    var _storeInstances = {}
    var _cache = {}

    var getStore = function (category) {
      var store = _storeInstances[category];
      if (store != undefined) {
        return store;
      }

      var storeConfig = storeConfigs[category];
      if (storeConfig != undefined && storeConfig.storeType != undefined) {
        var storeType = storeConfig.storeType;
        if (storeType == 'indexedDB') {
          store = indexedDBInstance;
        } else if (storeType == 'localStorage') {
          store = localStoreInstance;
        }
      }

      store = (store == undefined) ? DEFAULT_STORE_INSTANCE : store;
      _storeInstances[category] = store;

      return store;
    }

    var _init = function () {
      indexedDBInstance.init();

      _.forEach(storeConfigs, function (storeConfig, category) {
        if (storeConfig.cache == true) {
          getStore(category).getAll(category).then(function (allData) {
            _cache[category] = allData;
            $rootScope.$emit("category_cache_initialized", category);
          }, function (error) {
            console.log(error);
          });
        }
      });
    }

    this.get = function (category, fieldName, key) {
      var categoryCache = _cache[category];
      if (categoryCache != undefined) {
        return $q.when(categoryCache[key]);
      }

      return getStore(category).get(category, fieldName, key);
    }

    this.set = function (category, key, value) {
      var categoryCache = _cache[category];
      if (categoryCache != undefined) {
        //TODO: store set may fail...So cache and real store may go out of sync.
        categoryCache[key] = value;
      }

      return getStore(category).set(category, key, value);
    }
    
    this.update = function (category, key, value) {
        var categoryCache = _cache[category];
        if (categoryCache != undefined) {
          //TODO: store set may fail...So cache and real store may go out of sync.
          categoryCache[key] = value;
        }

        return getStore(category).update(category, key, value);
      }

    this.getAll = function (category) {
      return getStore(category).getAll(category);
    }


    this.remove = function (category, key) {
      var categoryCache = _cache[category];
      if (categoryCache != undefined) {
        //TODO: store remove may fail...So cache and real store may go out of sync.
        delete categoryCache[key] ;
      }
      
      return getStore(category).remove(category, key);
    }

    this.removeCategory = function (category) {
      //return getStore(category).removeCategory(category, key);
      return getStore(category).removeCategory(category);
    }
    
    this.getGuid = function() {
    	  function s4() {
    	    return Math.floor((1 + Math.random()) * 0x10000)
    	      .toString(16)
    	      .substring(1);
    	  }
    	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    	    s4() + '-' + s4() + s4() + s4();
    }

    _init();
  }]);
