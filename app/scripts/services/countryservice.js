'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.countryService
 * @description
 * # countryService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('countryService', ['$rootScope', '$http', 'storageService', 'constantService', '$q','_', function ($rootScope, $http, storageService, constantService, $q,_) {
    this.country = undefined;
    this.locality_country = undefined;

    this.isValidCountry = function (country, returnOnInvalid) {
      if (country == undefined) {
        return returnOnInvalid;
      }

      var validCountry = _.find(constantService.country, function (value) {
        return (value == country);
      });
      return (validCountry == undefined) ? returnOnInvalid : validCountry;
    }

    this.locateCountry = function () {
      var deferred = $q.defer();

      $http.get('https://ipinfo.io/geo').
		  then(function onSuccess(res) {
        if (res==undefined || res.data == undefined || res.data.country == undefined) {
          data.country = constantService.defaultCountry;
          //return;
        }
        storageService.set(constantService.storageDefaultCategory, constantService.storageKeys.LOCALITY, res.data.country.toUpperCase()).finally(function () {
          this.locality_country = res.data.country.toUpperCase();
        });

        deferred.resolve(res.data.country.toUpperCase());
		  }).catch(function onError(error) {
        deferred.reject(error);
		  });

      // $http.get('https://ipinfo.io/geo')
      //   .success(function (data) {
      //     if (data == undefined || data.country == undefined) {
      //       //deferred.reject('No data');
      //       data.country=constantService.defaultCountry;
      //       //return;
      //     }
      //     storageService.set(constantService.storageDefaultCategory, constantService.storageKeys.LOCALITY, data.country.toUpperCase()).finally(function () {
      //       this.locality_country = data.country.toUpperCase();
      //     });

      //     deferred.resolve(data.country.toUpperCase())
      //   })
      //   .error(function (error) {
      //     deferred.reject(error);
      //   });
      
      return deferred.promise;
    }

    this._locateAndSetStorage = function (defaultCountryIfFailed) {
      var deferred = $q.defer();

      this.locateCountry().then(function (locatedCountry) {

        //..Verify whether the country is valid or not
        var validCountry = this.isValidCountry(locatedCountry, defaultCountryIfFailed);

        //..Store the country to storage
        storageService.set(constantService.storageDefaultCategory, constantService.storageKeys.COUNTRY, validCountry).finally(function () {
          this.country = validCountry;
          deferred.resolve(validCountry);
        }.bind(this))
      }.bind(this), function (error) {
        this.country = defaultCountryIfFailed;
        //..Locate country failed...Not setting the country to storage because next time it may locate properly
        deferred.resolve(defaultCountryIfFailed)
      }.bind(this))

      return deferred.promise;
    }

    this.getCountry = function () {
      return this.country;
    }

    this.getLocalityCountry = function () {
      return this.locality_country;
    }

    this.resolveAndGet = function (newCountry) {
      newCountry = this.isValidCountry(newCountry, undefined);
      //check LOCALITY
      storageService.get(constantService.storageDefaultCategory, constantService.storageKeys.LOCALITY, constantService.storageKeys.LOCALITY).then(function (result) {
        if (!result) {
          this.locateCountry();
        }
        this.locality_country = result;
      }.bind(this));
      if (newCountry != undefined) {
        this.country = newCountry;
        //set filter used flag
        storageService.get(constantService.storageDefaultCategory, "tooltipBoxInfo", "tooltipBoxInfo").then(function (item) {
          if (item) {
            var infoCpy = angular.copy(item);
            if (!infoCpy.countryfilterused) {
              infoCpy.countryfilterused = !infoCpy.countryfilterused;
              storageService.set(constantService.storageDefaultCategory, "tooltipBoxInfo", infoCpy);
            }
          }
        });

        var deferred = $q.defer();
        storageService.set(constantService.storageDefaultCategory, constantService.storageKeys.COUNTRY, newCountry).then(function () {
          deferred.resolve(newCountry)
        }, function (error) {
          deferred.reject(error);
        });
        return deferred.promise;
      }

      if (this.country) {
        return $q.when(this.country);
      }

      var deferred = $q.defer();

      //..Check whether country set on storage
      storageService.get(constantService.storageDefaultCategory, undefined, constantService.storageKeys.COUNTRY)
        .then(function (country) {

          var validCountry = this.isValidCountry(country, undefined);

          //..Verify whether the country is valid or not
          if (validCountry == undefined) {
            this._locateAndSetStorage(constantService.defaultCountry).finally(function () {
              deferred.resolve(this.country);
              $rootScope.$broadcast("country_resolved", this.country);
            }.bind(this))
          } else {
            this.country = validCountry;
            deferred.resolve(this.country)
          }
        }.bind(this), function (error) {
          //..Failed to access storage
          deferred.resolve(constantService.defaultCountry)
        })

      return deferred.promise;
    }
  }]);
