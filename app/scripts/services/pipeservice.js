'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.pipeService
 * @description
 * # pipeService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('pipeService', ['favoriteService', 'countryService', '_', 'likeService', function (favoriteService, countryService, _, likeService) {
    this.locality_country = countryService.getLocalityCountry();
    var restrictFeedExcluder = function (item) {
      if (item.exclude_countries && item.exclude_countries.indexOf(this.locality_country) > -1) {
        return undefined;
      }

      return item;
    }
    var indexAppender = function (ctx, indexFieldName, item, category_subcategory) {
      if (!ctx.category_subcategory) {
        ctx.category_subcategory = category_subcategory;
      }
      else if (ctx.category_subcategory && ctx.category_subcategory != category_subcategory) {
        ctx.index = 0;
        ctx.category_subcategory = category_subcategory;
      }
      ctx.index += 1;
      item[indexFieldName] = ctx.index;
      return item;
    }

    var favoriteAppender = function (favoriteService, item) {
      favoriteService.contains(item).then(function (data) {
        if (data) {
          item.favorites = true;
        }
      })

      return item;
    }

    var likeAppender = function (likeService, item) {
      likeService.contains(item).then(function (data) {
        if (data) {
          item.likes = data.likes;
        }
      })

      return item;
    }

    return {
      restrictFeedExcluder: function () {
        return _.partial(restrictFeedExcluder);
      },
      indexAppender: function () {
        return _.partial(indexAppender, { index: 0 }, 'index');
      },
      favoriteAppender: function () {
        return _.partial(favoriteAppender, favoriteService);
      },
      likeAppender: function () {
        return _.partial(likeAppender, likeService);
      }
    }
  }]);
