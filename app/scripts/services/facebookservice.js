'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.facebookService
 * @description
 * # facebookService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('facebookService', ['$http', '$q', 'ezfb', function ($http, $q, ezfb) {
    var baseURL = "https://graph.facebook.com/v2.8/";
    var postFields = "fields=picture,full_picture,link,shares,created_time,message,name,type,attachments,likes.limit(1).summary(true)"

    this.extractUrlFromEmbeddedPost = function (embededPostContent) {
      // var searchText = "data-href=\"";
      // var startIndex = embededPostContent.indexOf(searchText);
      // if (startIndex == -1) {
      //   return {status: false, error: "data-href attribute not found..."}
      // }
      //
      // var endIndex = embededPostContent.indexOf("\"", startIndex + searchText.length);
      // if (endIndex == -1) {
      //   return {status: false, error: "closing quotes missing..."}
      // }
      // return {status: true, postUrl: embededPostContent.substring(startIndex + searchText.length, endIndex)};

      //New code as per FB changed embedPost Content
      var searchText = "href=";
      var startIndex = embededPostContent.indexOf(searchText);
      if (startIndex == -1) {
        return { status: false, error: "href attribute not found..." }
      }

      var endIndex = embededPostContent.indexOf("&", startIndex + searchText.length);
      if (endIndex == -1) {
        return { status: false, error: "closing quotes missing in href attribute..." }
      }
      var postUrl = embededPostContent.substring(startIndex + searchText.length, endIndex);
      postUrl = postUrl.replace(/%3A/g, ':');
      postUrl = postUrl.replace(/%2F/g, '/');
      return { status: true, postUrl: postUrl };
    }

    this.extractPageNameFromUrl = function (pageURL) {
      var pageURLSegments = pageURL.split("?");
      var urlSegments = pageURLSegments[0].split("/")
      var length = urlSegments.length;

      var takeNext = false;
      var pageName = undefined;
      for (var index = 0; index < length; index++) {
        var segment = urlSegments[index].trim();

        if (takeNext == true && segment.length > 0) {
          pageName = segment;
          break;
        }

        segment = segment.toLowerCase();
        if (segment == 'www.facebook.com') {
          takeNext = true;
        }
      }

      return pageName;
    }

    this.getPageId = function (access_token, pageName) {
      var deferred = $q.defer();
      $http.get(baseURL + pageName + "?access_token=" + access_token).then(function (response) {
        //console.log(response)
        if (response.status == 200) {
          if (response.data && response.data.id) {
            deferred.resolve({ id: response.data.id })
            return;
          }
        }

        deferred.reject({
          httpStatus: response.status,
          error: "Error while fetching page id for " + pageName,
          data: response.data
        })
      }, function (error) {
        deferred.reject({ error: "Error while fetching page id for " + pageName, errorDetail: error })
      });

      return deferred.promise;
    }

    this.getPostDetail = function (access_token, pageId, postId) {
      var deferred = $q.defer();
      console.log("Page Id : " + pageId + ", Post Id : " + postId);
      postId = postId.split(":")[0]
      $http.get(baseURL + pageId + "_" + postId + "?access_token=" + access_token + "&" + postFields).then(function (response) {
        //console.log(response)
        if (response.status == 200) {
          deferred.resolve(response.data)
          return;
        }

        deferred.reject({
          httpStatus: response.status,
          error: "Error while fetching post detail for page Id : " + pageId + ", post Id : " + postId,
          data: response.data
        })
      }, function (error) {
        deferred.reject({
          error: "Error while fetching post detail for page Id : " + pageId + ", post Id : " + postId,
          errorDetail: error
        })
      });

      return deferred.promise;
    }

    this.getPostDetailFromPostId = function (access_token, postId, reqFields) {
      var deferred = $q.defer();
      var reqURL = baseURL + postId + "?access_token=" + access_token + "&fields=" +
        (!reqFields ? "picture,description,source" : reqFields);
      $http.get(reqURL).then(function (response) {
        //console.log(response)
        if (response.status == 200) {
          deferred.resolve(response.data)
          return;
        }

        deferred.reject({
          httpStatus: response.status,
          error: "Error while fetching post detail for post Id : " + postId,
          data: response.data
        })
      }, function (error) {
        deferred.reject({
          error: "Error while fetching post detail for post Id : " + postId,
          errorDetail: error
        })
      });

      return deferred.promise;
    }
    // get page ID and Post ID from copied Url-KB
    this.extractDetailsFromCopiedUrl = function (embededPostContent) {
      var vars = {};
      embededPostContent.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function (m, key, value) {
          vars[key] = value;
        });
      if (_.isEmpty(vars)) {
        return { status: false, error: "page ID and Post ID not found..." }
      }
      else if (vars.id == undefined) {
        return { status: false, error: "page ID not found..." }
      }
      else if (vars.story_fbid == undefined) {
        return { status: false, error: "Post ID not found..." }
      }
      if (vars.substory_index != undefined) {
        return { status: true, pageId: vars.id, postId: vars.story_fbid + ":" + vars.substory_index };
      }
      else {
        return { status: true, pageId: vars.id, postId: vars.story_fbid };
      }
    }
    //get page details using pageID-KB
    this.getPageDetailFromPageId = function (access_token, pageId, reqFields) {
      var deferred = $q.defer();
      var reqURL = baseURL + pageId + "?access_token=" + access_token + "&fields=" +
        (!reqFields ? "id,name,link" : reqFields);
      $http.get(reqURL).then(function (response) {
        //console.log(response)
        if (response.data && response.data.link.indexOf('app_scoped_user_id') > -1) {//check individual user has posted the post or not?
          deferred.resolve({ status: false, error: "Invalid  post Url. Make sure you are posting only public post !" })
          return;
        }
        else if (response.status == 200) {
          deferred.resolve(response.data)
          return;
        }

        deferred.reject({
          httpStatus: response.status,
          error: "Error while fetching page detail for page Id : " + pageId,
          data: response.data
        })
      }, function (error) {
        deferred.reject({
          error: "Error while fetching page detail for page Id : " + pageId,
          errorDetail: error
        })
      });

      return deferred.promise;
    }

    this.getPageNameAndPostId = function (postUrl) {
      var postUrlSegments = postUrl.split("?")

      var domainUrl = postUrlSegments[0]
      var paramUrl = undefined
      if (postUrlSegments.length > 1) {
        paramUrl = postUrlSegments[1]
      }

      var domainSegments = _.filter(domainUrl.split('/'), function (domainPart) {
        //console.log(domainPart)
        return (domainPart.trim().length > 0)
      });

      var index = 0;
      var segment = domainSegments[index].trim().toLowerCase();
      if (segment == 'http:' || segment == 'https:') {
        index++;
      }

      if (index + 1 > domainSegments.length) {
        return { status: false, error: "Invalid URL" }
      }

      segment = domainSegments[index].trim();
      if (segment == 'www.facebook.com') {
        index++;
      }

      if (index + 1 > domainSegments.length) {
        return { status: false, error: "Invalid URL" }
      }

      var pageName = undefined
      var postId = undefined
      segment = domainSegments[index].trim();
      if (segment.endsWith('.php') && index + 1 == domainSegments.length) {
        //..Video php loop
        if (paramUrl == undefined) {
          return { status: false, error: "Invalid URL" }
        }

        var paramSegments = paramUrl.split("&");
        var videoParam = _.find(paramSegments, function (paramSegment) {
          return (paramSegment.startsWith("v="))
        });

        var videoParamKeyValue = videoParam.split("=")
        if (videoParamKeyValue.length != 2) {
          return { status: false, error: "Invalid URL" }
        }

        postId = videoParamKeyValue[1].trim();
        console.log('Post Id : ' + postId)
      } else {
        pageName = segment;
        console.log('Page name : ' + pageName)

        if (domainSegments.length <= 2) {
          return { status: false, error: "Invalid URL" }
        }

        var postId = domainSegments[domainSegments.length - 1].trim();
        console.log('Post Id : ' + postId)
      }

      return { pageName: pageName, postId: postId }

      /*
       var baseURL = "https://www.facebook.com/";
       if (postUrl[0] != '/' && postUrl.length < baseURL.length) {
       return {status: false, error: "Invalid URL"}
       }

       var postIdPosition = 2

       if (postUrl[0] != '/') {
       postUrl = postUrl.substring(baseURL.length)
       } else {
       postUrl = postUrl.substring(1)
       postIdPosition = 3
       }

       var urlSplit = postUrl.split("/");
       if (urlSplit.length < postIdPosition + 1) {
       return {status: false, error: "Invalid URL"}
       }

       return {pageName: urlSplit[0], postId: urlSplit[postIdPosition]}
       */
    }

    this.getPostDataFromUrl = function (access_token, url) {
      var result = this.getPageNameAndPostId(url);
      if (result.status == false) {
        return $q.reject(result)
      }

      var deferred = $q.defer();
      this.getPageId(access_token, result.pageName).then(function (response) {
        var pageId = response.id;
        this.getPostDetail(access_token, pageId, result.postId).then(function (response) {
          deferred.resolve(response)
        }, function (error) {
          deferred.reject(error)
        })
      }.bind(this), function (error) {
        deferred.reject(error)
      })

      return deferred.promise;
    }

    this.page_live_videos = function () {
      var channels = [
        {
          "sourceName": "Timesnow",
          "feedType": "Facebook",
          "feedItemsToRead": 9,
          "timeDepthInHrs": 2,
          "minValidItemsBeforeChangeDepth": 1,
          "include": "IN",
          "subcategory": "Dailynews",
          "tags": "",
          "exclude": ""
        },
        {
          "sourceName": "Newsxonline",
          "feedType": "Facebook",
          "feedItemsToRead": 9,
          "timeDepthInHrs": 2,
          "minValidItemsBeforeChangeDepth": 1,
          "include": "IN",
          "subcategory": "Dailynews",
          "tags": "",
          "exclude": ""
        }];
      var batchRequestsArr = [];
      _.each(channels, function (channelInfo, channelIndex) {
        if (channelIndex > 349 && channelIndex <= 399) {
          batchRequestsArr.push({ method: 'GET', relative_url: channelInfo.sourceName + '/live_videos?fields=seconds_left,status,title,permalink_url,description,embed_html&limit=5&include_headers=false' })
        }
      });
      var deferred = $q.defer();
      ezfb.api('/', 'POST', {
        //  batch: [
        //       { method: 'GET', relative_url: 'outdoorlife/live_videos?fields=seconds_left,status,title,permalink_url,description,embed_html&limit=5&include_headers=false'},
        //       { method: "GET",relative_url: 'earthcaminc/live_videos?fields=seconds_left,status,title,permalink_url,description,embed_html&limit=5&include_headers=false' }
        //  ]
        batch: batchRequestsArr
      }, function (response) {
        //console.log(response);
        if (response.error) {
          deferred.reject(response.error);
        }
        else {
          var mergedInfo = [];
          var page_live_feeds = undefined;
          _.each(response, function (pageData, pageIndex) {
            page_live_feeds = JSON.parse(pageData.body);
            var filteredFeeds = _.filter(page_live_feeds.data, function (feed, index) {
              return feed.status == "LIVE";
            });
            mergedInfo = mergedInfo.concat(filteredFeeds);
          });
          deferred.resolve(mergedInfo);
        }
      });

      return deferred.promise;
    }
  }]);
