'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.contentFilterService
 * @description
 * # contentFilterService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('contentFilterService', ['$q', 'storageService', 'constantService', 'countryService','_', function ($q, storageService, constantService, countryService,_) {
    var defaultMediaTypes = [constantService.mediaType.photo, constantService.mediaType.video];
    var defaultCountry = [countryService.getCountry()];
    var defaultSearchCountry = [constantService.country.IN, constantService.country.US];
    var defaultNetworks = [getShortNetworkName(constantService.feedTypes.Facebook), getShortNetworkName(constantService.feedTypes.Youtube), getShortNetworkName(constantService.feedTypes.Twitter), getShortNetworkName(constantService.feedTypes.Instagram), getShortNetworkName(constantService.feedTypes.Reddit)];

    this.get = function (category, subcategory, storageKey) {
      var deferred = $q.defer();

      var mediaType = undefined;
      storageService.get(constantService.storageDefaultCategory, constantService.storageDefaultFieldName, storageKey)
        .then(function (storagevalues) {
          if (storagevalues == undefined || storagevalues == '') {
            if (storageKey == constantService.storageMediaTypeKey || storageKey == constantService.storageSearchMediaTypeKey) {
              return deferred.resolve(defaultMediaTypes)
            } else if (storageKey == constantService.storageSearchCountries) {
              return deferred.resolve(defaultSearchCountry);
            } else if (storageKey == constantService.storageNetworkKey || storageKey == constantService.storageSearchNetworkKey) {
              return deferred.resolve(defaultNetworks)
            } else {
              return deferred.resolve(defaultCountry);
            }
          }

          if (typeof storagevalues === 'string') {
            return deferred.resolve(storagevalues.split(","));
          } else {
            if (storageKey == constantService.storageMediaTypeKey || storageKey == constantService.storageSearchMediaTypeKey) {
              return deferred.resolve(defaultMediaTypes)
            } else if (storageKey == constantService.storageSearchCountries) {
              return deferred.resolve(defaultSearchCountry);
            } else if (storageKey == constantService.storageNetworkKey) {
              return deferred.resolve(defaultNetworks)
            } else {
              return deferred.resolve(defaultCountry);
            }
          }
        }, function (error) {
          //return deferred.resolve(defaultMediaTypes)
          if (storageKey == constantService.storageMediaTypeKey || storageKey == constantService.storageSearchMediaTypeKey) {
            return deferred.resolve(defaultMediaTypes)
          } else if (storageKey == constantService.storageSearchCountries) {
            return deferred.resolve(defaultSearchCountry);
          } else {
            return deferred.resolve(defaultCountry);
          }
        })

      return deferred.promise;
    }

    this.set = function (category, subcategory, storageValue, storageKey) {
      //set filter used flag
      storageService.get(constantService.storageDefaultCategory, "tooltipBoxInfo", "tooltipBoxInfo").then(function (item) {
        if (item) {
          var infoCpy = angular.copy(item);
          if (!infoCpy.contentfilterused) {
            infoCpy.contentfilterused = !infoCpy.contentfilterused;
            storageService.set(constantService.storageDefaultCategory, "tooltipBoxInfo", infoCpy);
          }
        }
      });
      return storageService.set(constantService.storageDefaultCategory, storageKey, storageValue.join(','))
    }
  }]);
