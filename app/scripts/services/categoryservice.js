'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.categoryService
 * @description
 * # categoryService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('categoryService', ['$http', '$q', 'constantService', 'countryService', function ($http, $q, constantService, countryService) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var baseURL = channelConfigs[constantService.trendisChannelName].baseUrl;

    this._getsuggestedTopics = function () {
      var localityCountry = countryService.getLocalityCountry();
      if (!localityCountry) {
        localityCountry = constantService.country.US;
      }
      var deferred = $q.defer();
      $http.get(baseURL + 'suggest_topics/' + localityCountry + '/').then(function (response) {
        if (response) {
          return deferred.resolve(response.data);
        }
        else {
          console.log("data not found!");
          return deferred.resolve([]);
        }
      }.bind(this), function (error) {
        console.log(error);
        return deferred.reject(error);
      });
      return deferred.promise;
    }
  }]);
