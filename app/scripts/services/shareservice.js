'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.shareService
 * @description
 * # shareService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('shareService',['constantService',function (constantService) {
    this.getShareURL = function (type, feed) {
      if (feed == undefined) {
        return undefined;
      }
      else {
        return getSocialShareURL(type, feed);
      }

      return undefined;
    }
  }]);
