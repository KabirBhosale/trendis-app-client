'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.cardViewFactory
 * @description
 * # cardViewFactory
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('cardViewFactory', ['$q', '$injector', 'constantService', 'storageService', 'feedServiceProvider', 'userService','_', function ($q, $injector, constantService, storageService, feedServiceProvider, userService,_) {
    function CardView(pageConfig, notification) {
      this.country = pageConfig.country;
      this.category = pageConfig.category;
      this.subcategory = pageConfig.subcategory;
      this.pageType = pageConfig.pageType;      
      this.busy = false;
      if (pageConfig.searchText) {
        this.searchText = pageConfig.searchText;
        this.timelimit = pageConfig.timelimit;
        this.sortBy = pageConfig.sortBy;
      }
      if (pageConfig.is_personalise) {
        this.is_personalise = true;
      }
      if(pageConfig.pageType=='highlights' || pageConfig.pageType=='live'){
        this.gender=pageConfig.gender;
        this.age=pageConfig.age;
      }
      this.notification = notification;
      this._init();
    }


    CardView.prototype._init = function () {
      var config = undefined;

      this.getInitialConfig().then(function (initialConfig) {
        config = initialConfig;
        config.injector = $injector;
        this.feedService = feedServiceProvider.create(config);
        this.getNextPage()
      }.bind(this))
    }

    CardView.prototype.getInitialConfig = function () {
      if (this.notification.getInitialConfig) {
        return $q.when(this.notification.getInitialConfig());
      }

      var deferred = $q.defer();

      this.mediaType = undefined;
      var storageMediaKey = constantService.storageMediaTypeKey;
      var storageNetworkKey = constantService.storageNetworkKey;
      if (this.searchText && !this.is_personalise) {
        storageMediaKey = constantService.storageSearchMediaTypeKey;
        storageNetworkKey = constantService.storageSearchNetworkKey;
      }
      storageService.get(constantService.storageDefaultCategory, constantService.storageDefaultFieldName, storageMediaKey)
        .then(function (storageMediaType) {
          if (storageMediaType == undefined || storageMediaType == '' || typeof storageMediaType !== 'string') {
            storageMediaType = constantService.mediaType.photo+','+constantService.mediaType.video;
          }

          this.mediaType = storageMediaType.split(",");

          //remove articals from humor-dailyhumor
          if (this.category == 'humor' && this.subcategory == 'dailyhumor' && this.mediaType.indexOf('link') > -1) {
            if (this.mediaType.length == 1) {
              this.mediaType[0] = constantService.mediaType.video;
            }
            else {
              this.mediaType.splice(this.mediaType.indexOf('link'), 1);
            }
          }
          storageService.get(constantService.storageDefaultCategory, constantService.storageDefaultFieldName, storageNetworkKey)
            .then(function (storageNetworks) {
              if (storageNetworks == undefined || storageNetworks == '' || typeof storageNetworks !== 'string') {
                storageNetworks = constantService.defaultNetworkType;
              }
              this.networks = storageNetworks.split(",");
              if (this.networks.length == Object.keys(constantService.feedTypes).length) {
                this.networks = constantService.defaultNetworkType;
              } else {
                //do not change!
              }
              if (this.pageType == "topHome") {
                this.userPreferences = [{"category":"humor","subCategories":["all"]},{"category":"sports","subCategories":["all"]},{"category":"entertainment","subCategories":["all"]},{"category":"infotainment","subCategories":["all"]},{"category":"lifestyle","subCategories":["all"]},{"category":"hobbies","subCategories":["all"]}];
              }
              return deferred.resolve(this.getUrlConfig());
              // if (this.pageType == "topHome") {
              //   this.getUserPreferences().then(function (userPreferences) {
              //     this.userPreferences = userPreferences;
              //     return deferred.resolve(this.getUrlConfig());
              //   }.bind(this));
              // } else {
              //   return deferred.resolve(this.getUrlConfig());
              // }
            }.bind(this));
        }.bind(this), function (error) {
          //var storageMediaType = constantService.mediaType.photo+','+constantService.mediaType.video;
          this.mediaType = [constantService.mediaType.photo, constantService.mediaType.video];
          var storageNetworks = constantService.defaultNetworkType;
          this.networks = storageNetworks.split(",");
          if (this.pageType == "topHome") {
            this.userPreferences = [{"category":"humor","subCategories":["all"]},{"category":"sports","subCategories":["all"]},{"category":"entertainment","subCategories":["all"]},{"category":"infotainment","subCategories":["all"]},{"category":"lifestyle","subCategories":["all"]},{"category":"hobbies","subCategories":["all"]}];
          }
          return deferred.resolve(this.getUrlConfig());
          // if (this.pageType == "topHome") {
          //   this.getUserPreferences().then(function (userPreferences) {
          //     this.userPreferences = userPreferences;
          //     return deferred.resolve(this.getUrlConfig());
          //   }.bind(this));
          // } else {
          //   return deferred.resolve(this.getUrlConfig());
          // }
        });

      return deferred.promise;
    }

    CardView.prototype.getUserPreferences = function () {
      console.log('*** country : '+this.country)
      var settings= constantService.homeScreenFeedCategories[this.country || 'US'];
      var deferred = $q.defer();
      userService.getPreferences().then(function (preferencesData) {
        var categoriesData = preferencesData;
        if (preferencesData && preferencesData.preferences) {
          categoriesData = preferencesData.preferences;
        }
        if (categoriesData == undefined || preferencesData.preferences==undefined || categoriesData.length == 0) {
          return deferred.resolve(this.bindPreferencesWithMediaType(settings));
        }
        else {
          return deferred.resolve(this.bindPreferencesWithMediaType(categoriesData));
        }
      }.bind(this), function (error) {
        return deferred.resolve(this.bindPreferencesWithMediaType(settings));
      });

      return deferred.promise;
    }

    CardView.prototype.bindPreferencesWithMediaType = function (preferences) {
      _.each(preferences, function (setting, index) {
        var category = setting.category;
        var subCategories = setting.subCategories;
        setting.medias = this.mediaType;
      }.bind(this));
      return preferences;
    }


    CardView.prototype.getUrlConfig = function () {
      var pipes = [
        {
          name: 'restrictFeedExcluder',
          config: {}
        },
        {
          name: 'indexAppender',
          config: {}
        },
        {
          name: 'favoriteAppender',
          config: {}
        },
        {
          name: 'likeAppender',
          config: {}
        }
      ];
      if (this.pageType == constantService.searchCategory) {
        return {
          channels: [
            {
              type: 'trendis',
              country: this.country,
              mediaType: this.mediaType,
              category: this.category,
              subcategory: this.subcategory,
              pageType: this.pageType,
              userPreferences: this.userPreferences,
              searchText: this.searchText,
              timelimit: this.timelimit,
              sortBy: this.sortBy,
              networks: this.networks
            }
          ],
          pipes: pipes
        }
      }
      else {
        return {
          channels: [
            {
              type: 'trendis',
              country: this.country,
              mediaType: this.mediaType,
              category: this.category,
              subcategory: this.subcategory,
              pageType: this.pageType,
              userPreferences: this.userPreferences,
              gender:this.gender,
              age:this.age,
              networks: this.networks
            }
          ],
          pipes: pipes
        }
      }
    }


    CardView.prototype.getNextPage = function () {
      if (this.busy == true) {
        return $q.reject({ busy: true });
      }
      this.busy = true;

      var deferred = $q.defer()
      this.feedService.getNextPage().then(function (feeds) {
        this.busy = false;
        this.notification.onData(feeds)
        deferred.resolve(feeds)
      }.bind(this), function (error) {
        console.error('Error while fetching the data...')
        console.error({ error: error })
        this.busy = false;
        this.notification.onError(feeds)
        deferred.reject(error)
      }.bind(this))

      return deferred.promise;
    }

    return {
      create: function (pageConfig, notification) {
        return new CardView(pageConfig, notification);
      }
    };
  }]);
