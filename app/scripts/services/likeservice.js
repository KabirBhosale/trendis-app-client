'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.likeService
 * @description
 * # likeService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('likeService', ['$rootScope', '$http', 'userService', 'storageService', 'constantService', '_',function ($rootScope, $http, userService, storageService, constantService,_) {
    var STORAGE_CATEGORY = constantService.likes.category;
    var associatedUserID = gDeviceID;
    this.add = function (item) {
      item['createdDateTime'] = new Date();
      storageService.set(STORAGE_CATEGORY, item.feedKey, item);
      putLike(item);
    };
    
    this.update = function (item) {
        storageService.update(STORAGE_CATEGORY, item.feedKey, item);
        putLike(item);
    };

    this.remove = function (item) {
      storageService.remove(STORAGE_CATEGORY, item.feedKey);
      deleteLike(item);
    };
    
    this.getAll = function () {
      return storageService.getAll(STORAGE_CATEGORY);
    };

    this.contains = function (item) {
      return storageService.get(STORAGE_CATEGORY, 'feedKey', item.feedKey);
    };

    this.migrateUserLikes = function (oldAssociatedUserID, newAssociatedUserID) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var req = baseUrl + 'likes/migrate?associatedUserID=' + newAssociatedUserID +
                '&oldAssociatedUserID=' + oldAssociatedUserID;
      $http.get(req, {withCredentials: true}).then(function (response) {
        console.log(response.data);
      }, function (error) {
        console.log(error);
      });
    };

    this.pullFromServer = function (associatedUserID) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var req = baseUrl + 'likes/user?associatedUserID=' + associatedUserID;
      _getLikes(req);
    };

    this.setAssociatedUserID = function (newAssociatedUserID) {
      associatedUserID = newAssociatedUserID;
    };
  
    function _getLikes(requestURI) {
      $http.get(requestURI, {withCredentials: true}).then(
        function (response) {
          if (response.data === undefined || response.data.error !== undefined) {
            return;
          }

          _.forEach(response.data, function(likeItem) {
            storageService.set(STORAGE_CATEGORY, likeItem.feedKey, likeItem.feedItem);                  
          });
          storageService.set(constantService.storageDefaultCategory, "lastSynchTime", Date.now());
        }, 
        function (error) {
          console.log(error);
        }
      );      
    }
  
    function getLikes(lastSynchTime) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var req = baseUrl + 'likes?associatedUserID=' + associatedUserID + 
                '&lastSynchTime=0';// + lastSynchTime;
      _getLikes(req);
    }    

    function putLike(likeItem) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      if (!userService.isLoggedIn()) {
        associatedUserID=gDeviceID;
      }
      var reqData = {
        associatedUserID: associatedUserID,
        feedItem: likeItem,
        feedKey:likeItem.feedKey,
        likeType:likeItem.likes
      };
      
      $http.post(baseUrl + 'likes/', reqData).then(
        function (response) {
          if (response.data === undefined || response.data.error !== undefined) {
            return;
          }          
        }, 
        function (error) {
          console.log(error);
        }
      );      
      storageService.set(constantService.storageDefaultCategory, "lastSynchTime", Date.now());                      
    }

    function deleteLike(favoriteItem) {
      var baseUrl = channelConfigs[constantService.trendisChannelName].baseUrl;
      var reqData = {
        associatedUserID: associatedUserID,
        feedKey: favoriteItem.feedKey
      };
      
      $http.post(baseUrl + 'likes/delete', reqData).then(
        function (response) {
          if (response.data === undefined || response.data.error !== undefined) {
            return;
          }          
        }, 
        function (error) {
          console.log(error);
        }
      );      
    }

    this.init = function() {
      var lastSynchTime = -1;
      // This event was needed since during synchup with server, the cache was not 
      // getting updated due to asychronous nature of cache initialization &
      // synch from server. To avoid it, we are initiating synch only after
      // cache initialization. Still there is an issue of favorite icon not getting
      // highlighted on first load of page when the page contents gets drawn first
      // before completion of synch.
      $rootScope.$on("category_cache_initialized", function(event, value){
        if (value === STORAGE_CATEGORY) {
          getLikes(lastSynchTime);   
        }
      });

      storageService.get(constantService.storageDefaultCategory, "deviceID", "deviceID").then(function(item) {
        if (item) {
          associatedUserID = item;
        } else {
          // To use the same deviceID, we are storing the fingerprint in localstorage
          // If we take everytime by calculating, there is a slight chance that the
          // fingerprint will change (when browser is upgraded, new system fonts installed, etc.,))
          storageService.set(constantService.storageDefaultCategory, "deviceID", gDeviceID);
        }
      });
      storageService.get(constantService.storageDefaultCategory, "lastLoggedInID", "lastLoggedInID").then(function(item) {
        if (item) {
          // if lastLoggedInID available then use it as the associatedUserID for synching
          associatedUserID = item;
        }
        storageService.get(constantService.storageDefaultCategory, "lastSynchTime", "lastSynchTime").then(
          function(item) {
            lastSynchTime = (item ? item : -1);
          }
        );
      });    
      
      $rootScope.$on('logged_into_server', function (event, status) {
        if (userService.isLoggedIn()) {
          storageService.set(constantService.storageDefaultCategory, "lastLoggedInID", userService.id);
          var oldAssociatedUserID = associatedUserID;
          associatedUserID = userService.id;

          // pull from server the information related to new userID
          // if (oldAssociatedUserID !== associatedUserID) {
          //   this.pullFromServer(associatedUserID); 
          // }
          this.pullFromServer(associatedUserID); 
          // inform server to migrate old userID information to new one
          //this.migrateUserLikes(oldAssociatedUserID, associatedUserID);
        } else {
          // on logout
        }
      }.bind(this));
    };
    
    this.init();
  }]);
