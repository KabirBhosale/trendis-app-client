'use strict';

/**
 * @ngdoc service
 * @name trendisAppClientApp.waitUtilService
 * @description
 * # waitUtilService
 * Service in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .service('waitUtilService', ['$mdDialog', '$timeout', function ($mdDialog,$timeout) {
    this.showWait = function () {
      console.log('calling wait...'+new Date())
      $mdDialog.show({
        //controller: 'waitCtrl',
        template: '<md-dialog id="plz_wait" style="background-color:transparent;box-shadow:none;overflow:hidden;">' +
        '<div layout="row" layout-sm="column" layout-align="center center" aria-label="wait">' +
        '<md-progress-circular md-mode="indeterminate" class="md-accent" md-diameter="60"></md-progress-circular>' +
        '</div>' +
        '</md-dialog>',
        parent: angular.element('#container'),//angular.element(document.body),
        bindToController: true,
        clickOutsideToClose: false,
        fullscreen: false
      })
      .then(function (answer) {

      }, function() {
        console.log('No changes in content filter!');
      });
    }

    this.hideWait = function () {
      console.log('hiding wait...'+new Date())
      $mdDialog.cancel();
    }
  }]);
