'use strict';

/**
 * @ngdoc filter
 * @name trendisAppClientApp.filter:dateToMinutes
 * @function
 * @description
 * # dateToMinutes
 * Filter in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .filter('dateToMinutes', function () {
    return function(input)
    {
        if(input == null){ return "";}
        return dateToMinutes(input);
    };

    function dateToMinutes(input_date) {
      var current_date = new Date();
      var ms_current = Date.parse(current_date);
      var actual_date = new Date(input_date);
      if(actual_date=="Invalid Date"){
    	  actual_date= new Date((input_date || "").replace(/-/g,"/").replace(/[TZ]/g," "));
      }
      //var actual_date = moment(input_date);
      var ms_actual = Date.parse(actual_date);
      var ms_in_diff = ms_current - ms_actual;
      var mins = parseInt(((ms_in_diff / 1000) / 60));
      return mins;
    }
  });
