'use strict';

/**
 * @ngdoc filter
 * @name trendisAppClientApp.filter:dateTotimeAgo
 * @function
 * @description
 * # dateTotimeAgo
 * Filter in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .filter('dateTotimeAgo', function () {
    return function (input) {
      if (input == null) { return ""; }
      return dateToTimeAgo(input);
    };

    function dateToTimeAgo(input_date) {
      var minutes = dateToMinutes(input_date);
      return minutesTotimeAgo(minutes);
    }

    function dateToMinutes(input_date) {

      var current_date = new Date();
      var ms_current = Date.parse(current_date);
      var actual_date = new Date(input_date);
      if (actual_date == "Invalid Date") {
        actual_date = new Date((input_date || "").replace(/-/g, "/").replace(/[TZ]/g, " "));
      }
      if (actual_date == "Invalid Date") {
        var v = input_date.split(' ');
        // return new Date(Date.parse(v[1]+" "+v[2]+", "+v[5]+" "+v[3]+" UTC"));
        actual_date = new Date(v[1] + " " + v[2] + ", " + v[5] + " " + v[3] + " UTC");
      }
      //var actual_date = moment(input_date);
      var ms_actual = Date.parse(actual_date);
      var ms_in_diff = ms_current - ms_actual;
      var mins = parseInt(((ms_in_diff / 1000) / 60));
      return mins;
    }

    function minutesTotimeAgo(minutes) {

      var time_ago = '';
      if (minutes < 60) {
        time_ago = minutes;
        if (time_ago == 1) {
          time_ago = time_ago + ' Minute Ago';
        }
        else {
          time_ago = time_ago + ' Minutes Ago';
        }
      }
      else if (minutes <= (60 * 24)) {
        time_ago = Math.floor(minutes / 60);
        if (time_ago == 1) {
          time_ago = time_ago + ' Hour Ago';
        }
        else {
          time_ago = time_ago + ' Hours Ago';
        }
      }
      else if (minutes <= ((60 * 24) * 30)) {
        time_ago = Math.floor((minutes / 60) / 24);
        if (time_ago < 7) {
          if (time_ago == 1) {
            time_ago = time_ago + ' Day Ago';
          }
          else {
            time_ago = time_ago + ' Days Ago';
          }
        }
        else {
          time_ago = Math.floor(time_ago / 7);
          if (time_ago == 1) {
            time_ago = time_ago + ' Week Ago';
          }
          else {
            time_ago = time_ago + ' Weeks Ago';
          }
        }
      }
      else {
        time_ago = Math.floor(((minutes / 60) / 24) / 30);
        if (time_ago == 1) {
          time_ago = time_ago + ' Month Ago';
        }
        else {
          time_ago = time_ago + ' Months Ago';
        }
      }
      return time_ago;
    }
  });
