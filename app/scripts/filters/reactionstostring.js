'use strict';

/**
 * @ngdoc filter
 * @name trendisAppClientApp.filter:reactionsToString
 * @function
 * @description
 * # reactionsToString
 * Filter in the trendisAppClientApp.
 */
angular.module('trendisAppClientApp')
  .filter('reactionsToString', function () {
    return function (input) {
      if (input == null) { return ""; }
      return reactionsToString(input);
    };

    function reactionsToString(reactionCount){
      if(reactionCount>1000){
        return parseFloat(reactionCount/1000).toFixed(1)+'k';
      }else{
        return reactionCount;
      }
    }
  });
