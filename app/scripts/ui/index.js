function _deviceType(){
    var screenWidth = window.innerWidth;//console.log(screenWidth)
    if (screenWidth <= 480) {
        return 'xxs';
    }else if (screenWidth > 480 && screenWidth < 768) {
        return 'xs';
    } else if (screenWidth >= 768 && screenWidth < 992) {
        return 'sm';
    } else if (screenWidth >= 992 && screenWidth < 1200) {
        return 'md';
    } else{
        return 'lg';
    }
}

//get ajax-requests
var pendingAjaxReq=[];
$(document).ajaxSend(function(event, jqXHR, ajaxOptions) {
		//alert("BLOCKED");
		//set options to global variable so can re initiate ajax call later
		pendingAjaxReq.push({
			xhr:jqXHR,url:ajaxOptions.url
		});
		//newajaxOptions = ajaxOptions;
		//ajaxOptions.error = null;
		//jqXHR.abort();
});