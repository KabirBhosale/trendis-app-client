'use strict';

/**
 * @ngdoc overview
 * @name trendisAppClientApp
 * @description
 * # trendisAppClientApp
 *
 * Main module of the application.
 */
angular
  .module('trendisAppClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngMaterial',
    'ngRoute',
    'ngSanitize',
    'LocalStorageModule',
    'indexedDB',
    'ezfb',
    'angulartics',
    'angulartics.google.analytics',
    'angular.filter',
    'ngtweet',
    'ngYoutube',
    'ui.sortable',
    'angularCSS',
    'slickCarousel',
    'ngclipboard'
  ])
  .config(['$qProvider','$locationProvider', '$routeProvider', 'localStorageServiceProvider', '$indexedDBProvider', '$httpProvider', 'ezfbProvider', function ($qProvider,$locationProvider, $routeProvider, localStorageServiceProvider, $indexedDBProvider, $httpProvider, ezfbProvider) {
    $locationProvider.hashPrefix('');
    $qProvider.errorOnUnhandledRejections(false);

    $routeProvider
      .when('/home', {
        templateUrl: 'views/home.html',
        resolve: {
          country: countryResolver
        }
      })
      .when('/trending', {
        templateUrl: 'views/feed.html',
        controller: 'HighlightsCtrl',
        controllerAs: 'highlights',
        resolve: {
          country: countryResolver
        }
      })
      .when('/live', {
        templateUrl: 'views/feed.html',
        controller: 'LiveCtrl',
        controllerAs: 'live',
        resolve: {
          country: countryResolver
        }
      })
      .when('/:country/:category/:subcategory', {
        templateUrl: 'views/feed.html',
        controller: 'FeedCtrl',
        controllerAs: 'feed',
        resolve: {
          country: countryResolver
        }
      })
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl',
        controllerAs: 'settings',
        resolve: {
          country: countryResolver
        }
      })
      .when('/custom-settings', {
        templateUrl: 'views/custom-settings.html',
        controller: 'SettingsCtrl',
        controllerAs: 'settings',
        resolve: {
          country: countryResolver
        }
      })
      .when('/sharepad', {
        templateUrl: 'views/sharepad.html',
        controller: 'SharepadCtrl',
        controllerAs: 'sharepad',
        resolve: {
          country: countryResolver
        }
      })
      .when('/search/:searchtext', {
        templateUrl: 'views/feed.html',
        controller: 'SearchCtrl',
        controllerAs: 'search',
        resolve: {
          country: countryResolver
        }
      })
      .when('/personalise/:personalise_item', {
        templateUrl: 'views/feed.html',
        controller: 'PersonalisecategoryCtrl',
        controllerAs: 'personalise',
        resolve: {
          country: countryResolver
        }
      })
      .when('/personalise/:custom_category/:custom_subcategory', {
        templateUrl: 'views/feed.html',
        controller: 'PersonalisecategoryCtrl',
        controllerAs: 'personalise',
        resolve: {
          country: countryResolver
        }
      })
      .when('/feedback', {
        templateUrl: 'views/feedback.html',
        controller: 'FeedbackCtrl',
        controllerAs: 'feedback'
      })
      .when('/about', {
        templateUrl: 'views/aboutTrendis.html',
        controller: 'AboutCtrl',
        controllerAs: 'About',
        resolve: {
          country: countryResolver
        }
      })
      .when('/feedbackDialog', {
        templateUrl: 'views/feedbackdialog.html',
        controller: 'FeedbackdialogCtrl',
        controllerAs: 'feedbackDialog'
      })
      .when('/copyright', {
        templateUrl: 'views/copyright.html'
      })
      .when('/my-settings', {
        templateUrl: 'views/my-settings.html',
        controller: 'MySettingsCtrl',
        controllerAs: 'mySettings'
      })
      .when('/duplicates/:category', {
          templateUrl: 'views/duplicates.html',
          controller: 'DuplicatesCtrl',
          controllerAs: 'duplicates',
          resolve: {
              country: countryResolver
          }
      })
      .when('/trends', {
        templateUrl: 'views/trends.html',
        controller: 'TrendsCtrl',
        controllerAs: 'trends',
        css: ['styles/trends/bootstrap.min.css', 'styles/trends/style.css','styles/trends/responsive.css','styles/trends/trends.css']
      })
      .otherwise({
        redirectTo: '/trends'
      });

    localStorageServiceProvider.setPrefix = 'trendis';

    $indexedDBProvider
      .connection('trendis')
      .upgradeDatabase(1, function (event, db, tx) {
        console.log('Inside upgrade database');
        _.forEach(storeConfigs, function (storeConfig, categoryName) {
          if (storeConfig.storeType == 'indexedDB') {
            var schema = storeConfig.schema;
            if (schema == undefined) {
              console.error('schema not available for ' + categoryName + ' in storeConfig');
              return;
            }

            var definition = schema.definition;
            if (definition == undefined) {
              console.error('definition not available for ' + categoryName + ' in storeConfig');
              return;
            }

            //TODO: Schema changes/upgrade needs to be added
            var objStore = db.createObjectStore(categoryName, definition.properties);
            if (schema.indexes == undefined || schema.indexes.length > 0) {
              _.forEach(schema.indexes, function (indexConfig) {
                objStore.createIndex(indexConfig.name, indexConfig.column, indexConfig.properties);
              })
            }
          }
        })
      })
      .upgradeDatabase(2, function (event, db, tx) {
        console.log('Inside upgrade database');
        _.forEach(storeConfigs, function (storeConfig, categoryName) {
          if (storeConfig.storeType == 'indexedDB' && storeConfig.dbVersion==2) {
            var schema = storeConfig.schema;
            if (schema == undefined) {
              console.error('schema not available for ' + categoryName + ' in storeConfig');
              return;
            }

            var definition = schema.definition;
            if (definition == undefined) {
              console.error('definition not available for ' + categoryName + ' in storeConfig');
              return;
            }
            if(!db.objectStoreNames.contains(categoryName)) {
              var objStore = db.createObjectStore(categoryName, definition.properties);
              if (schema.indexes == undefined || schema.indexes.length > 0) {
                _.forEach(schema.indexes, function (indexConfig) {
                  objStore.createIndex(indexConfig.name, indexConfig.column, indexConfig.properties);
                })
              }
            }
          }
        })
      });

    ezfbProvider.setInitParams({
      appId: channelConfigs.facebook.appID,
      version: 'v2.8'
    });
  }]);

  function countryResolver($q, $route, countryService, constantService) {
    console.log('params :'+JSON.stringify($route.current.params))

    var deferred = $q.defer();
    var routeCountry = $route.current.params.country;

    countryService.resolveAndGet(routeCountry)
        .then(function (country) {
          console.log('resolve country : '+country)
            if (country != routeCountry) {
                $route.current.params.country = country;
            }
            deferred.resolve($route.current.params.country);
        }, function (error) {
            $route.current.params.country = constantService.defaultCountry;
            deferred.resolve($route.current.params.country);

        })

    return deferred.promise;
}
