'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('SearchCtrl', ['$rootScope', '$scope', '$window', '$timeout', '$route', '$routeParams', 'constantService', 'cardViewFactory', 'waitUtilService', function ($rootScope, $scope, $window, $timeout, $route, $routeParams, constantService, cardViewFactory, waitUtilService) {
    $scope.feeds = [];
    $scope.feeds_grp_0 = [];
    $scope.feeds_grp_1 = [];
    $scope.feeds_grp_2 = [];
    $scope.feeds_grp_3 = [];
    var feedCardViewService = undefined;
    var timeOutArr = [];
    var _prevReq;
    var columns;
    var _prevCols;
    var columnIndx;
    var timeout = undefined;
    var country = undefined;
    var category = undefined;
    var timelimit = undefined;
    var pageType = constantService.searchCategory;
    var searchText = undefined;
    var sortBy = 'r';
    //function
    this.loadFeeds = function (searchCriteria) {
      angular.element('md-nav-ink-bar').css('display','none');//hide bottom color line of selected subcategory
      console.log('loadFeeds called!');
      waitUtilService.showWait();
      country = undefined;
      category = undefined;
      timelimit = undefined;
      sortBy = 't';
      if (searchCriteria) {
        $rootScope.searchCriteria = searchCriteria;
      }
      else if ($rootScope.searchCriteria) {
        searchCriteria = $rootScope.searchCriteria;
      }
      else {
        searchCriteria = { selectedCountry: undefined, selectedCategory: undefined, selectedTimeLimit: undefined, selectedSortBy: 't' }
      }//console.log(searchCriteria);
      if (searchCriteria.selectedCountry && searchCriteria.selectedCountry.id != "all") {
        country = searchCriteria.selectedCountry.id;
      }
      if (searchCriteria.selectedCategory && searchCriteria.selectedCategory.id != "all") {
        category = searchCriteria.selectedCategory.id;
      }
      if (searchCriteria.selectedTimeLimit && searchCriteria.selectedTimeLimit.id != "all") {
        timelimit = searchCriteria.selectedTimeLimit.id;
      }
      if (searchCriteria.selectedSortBy && searchCriteria.selectedSortBy.id && searchCriteria.selectedSortBy.id != "all") {
        sortBy = searchCriteria.selectedSortBy.id;
      }
      //var mediatype=$scope.selectedMedia;
      searchText = $routeParams.searchtext;
      feedCardViewService = cardViewFactory.create({ "country": country, "category": category, "timelimit": timelimit, "pageType": pageType, "sortBy": sortBy, "searchText": searchText }, {
        onData: function (feeds) {
          columns = _returnColumns();
          _prevCols = columns;
          $scope.errors = "";
          if (feeds.data.length > 0) {
            angular.forEach(feeds.data, function (itemData, index) {
              columnIndx = (itemData.index % columns);
              itemData.searchQuery = searchText;
              if (columnIndx != 0) {
                $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
              } else {
                $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
              }
              $scope.feeds = $scope.feeds.concat(itemData);
            });
            _prevReq = undefined;
          }
          else if (feeds.pageNo === 0 && feeds.data.length == 0) {
            angular.forEach(timeOutArr, function (tmItem) {
              $timeout.cancel(tmItem);
            });
            $scope.errors = "Whoops! Search not found for specified content filter and network filter, please check the spelling and filters and try again.";
            $scope.feeds = [];
          }
          else {
          }
          waitUtilService.hideWait();
        }.bind(this), onError: function (error) {
          waitUtilService.hideWait();
          $scope.errors = "Whoops! Search not found for specified content filter and network filter, please check the spelling and filters and try again.";
          if (feeds.pageNo === 0) {
            $scope.feeds = [];
          }
          //load from local-storage content
        }
      });
    }

    angular.element($window).bind('resize', function () {
      var timeoutItem = $timeout(function () {
        $scope._arrangeColsFeeds();
      }.bind(this));
      timeOutArr.push(timeoutItem);
    });

    $scope._arrangeColsFeeds = function () {
      columns = _returnColumns();
      if (_prevCols != columns) {
        console.log('arranging...')
        _prevCols = columns;
        $scope.feeds_grp_0.length = 0;
        $scope.feeds_grp_1.length = 0;
        $scope.feeds_grp_2.length = 0;
        $scope.feeds_grp_3.length = 0;
        angular.forEach($scope.feeds, function (itemData, index) {
          columnIndx = (itemData.index % columns);
          if (columnIndx != 0) {
            $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
          } else {
            $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
          }
        });
        //$scope.$apply();
      } else {

      }
    }

    this.init = function () {
      this.loadFeeds();
    };

    $scope.next = function () {
      if (!_prevReq && !$rootScope.isDialogOpen) {
        _prevReq = true;
        feedCardViewService.getNextPage();
      }
    }

    var myListener = $rootScope.$on('filterSearch', function (event, searchCriteria) {
      event.stopPropagation();
      $scope.feeds.length = 0;
      $scope.feeds_grp_0.length = 0;
      $scope.feeds_grp_1.length = 0;
      $scope.feeds_grp_2.length = 0;
      $scope.feeds_grp_3.length = 0;
      this.loadFeeds(searchCriteria);
      //$route.reload();
    }.bind(this));
    $scope.$on('$destroy', myListener);

    this.init();
  }]);
