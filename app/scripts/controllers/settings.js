'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('SettingsCtrl', ['$scope', 'categoryService', 'storageService', '$routeParams', '_', '$timeout', '$location', '$rootScope', 'constantService', 'userService', 'cardViewFactory', 'countryService', function ($scope, categoryService, storageService, $routeParams, _, $timeout, $location, $rootScope, constantService, userService, cardViewFactory, countryService) {
    $scope.personalisedArr = [];
    var waitForLoginComplete = false;
    $scope.settingAvailable = false;
    var ALL_INTEREST_ID = "1";
    $scope.saved = false;
    var userPreferences = [];
    $scope.selectedItems = {};
    $scope.errorMsg = undefined;
    var selectedCount = 0;
    $scope.categorySuggession = {};
    $scope.suggestedSubCategory = {};
    $scope.personalise = {
      value: undefined
    };
    var preferencesObj = {};
    $scope.imgSubCategories = 
    $scope.imgSubCategories = [
      // {
      //   category: 'humor',
      //   subCategory: 'all',
      //   imgpath: 'my_setting_all_humor.png',
      //   id: 'category1'
      // }, 
      {
        category: 'humor',
        subCategory: 'comics',
        imgpath: 'my_setting_comics.png',
        id: 'category2'
      }, {
        category: 'humor',
        subCategory: 'dailyhumor',
        imgpath: 'my_setting_daily_humor.png',
        id: 'category3'
      }, {
        category: 'humor',
        subCategory: 'gags',
        imgpath: 'my_setting_pranks.png',
        id: 'category4'
      }, {
        category: 'sports',
        subCategory: 'american_football',
        imgpath: 'my_setting_am_football.png',
        id: 'category5'
      }, {
        category: 'sports',
        subCategory: 'baseball',
        imgpath: 'my_setting_baseball.png',
        id: 'category6'
      }, {
        category: 'sports',
        subCategory: 'basketball',
        imgpath: 'my_setting_basketball.png',
        id: 'category7'
      }, {
        category: 'sports',
        subCategory: 'cricket',
        imgpath: 'my_setting_cricket.png',
        id: 'category8'
      }, {
        category: 'sports',
        subCategory: 'fights',
        imgpath: 'my_setting_fight.png',
        id: 'category9'
      }, {
        category: 'sports',
        subCategory: 'golf',
        imgpath: 'my_setting_golf.png',
        id: 'category10'
      }, {
        category: 'sports',
        subCategory: 'soccer',
        imgpath: 'my_setting_soccer.png',
        id: 'category11'
      }, 
      {
        category: 'sports',
        subCategory: 'all',
        imgpath: 'my_setting_sports.png',
        id: 'category12'
      }, 
      {
        category: 'sports',
        subCategory: 'tennis',
        imgpath: 'my_setting_tennis.png',
        id: 'category13'
      }, 
      // {
      //   category: 'entertainment',
      //   subCategory: 'all',
      //   imgpath: 'my_setting_entertainment.jpg',
      //   id: 'category14'
      // }, 
      {
        category: 'entertainment',
        subCategory: 'movies',
        imgpath: 'my_setting_movies.png',
        id: 'category15'
      }, {
        category: 'entertainment',
        subCategory: 'music',
        imgpath: 'my_setting_music.png',
        id: 'category16'
      }, {
        category: 'entertainment',
        subCategory: 'celebrity',
        imgpath: 'my_setting_celebrity.png',
        id: 'category17'
      }, 
      {
        category: 'entertainment',
        subCategory: 'television',
        imgpath: 'my_setting_television.jpg',
        id: 'category30'
      }, 
      // {
      //   category: 'infotainment',
      //   subCategory: 'all',
      //   imgpath: 'my_setting_all_news.png',
      //   id: 'category18'
      // }, 
      {
        category: 'infotainment',
        subCategory: 'auto',
        imgpath: 'my_setting_auto.png',
        id: 'category19'
      }, {
        category: 'infotainment',
        subCategory: 'interesting',
        imgpath: 'my_setting_interesting.png',
        id: 'category20'
      }, {
        category: 'infotainment',
        subCategory: 'news',
        imgpath: 'my_setting_news.png',
        id: 'category21'
      }, {
        category: 'infotainment',
        subCategory: 'tech',
        imgpath: 'my_setting_technology.png',
        id: 'category22'
      }, 
       {
        category: 'infotainment',
        subCategory: 'gaming',
        imgpath: 'my_setting_gaming.jpg',
        id: 'category31'
      }, 
       {
        category: 'infotainment',
        subCategory: 'business',
        imgpath: 'my_setting_business.png',
        id: 'category32'
      }, 
       {
        category: 'infotainment',
        subCategory: 'science',
        imgpath: 'my_setting_science.jpg',
        id: 'category33'
      }, 

      // {
      //   category: 'lifestyle',
      //   subCategory: 'all',
      //   imgpath: 'my_setting_lifestyle.png',
      //   id: 'category23'
      // }, 
      {
        category: 'lifestyle',
        subCategory: 'beauty',
        imgpath: 'my_setting_beauty.png',
        id: 'category24'
      }, {
        category: 'lifestyle',
        subCategory: 'food',
        imgpath: 'my_setting_food.png',
        id: 'category25'
      }, {
        category: 'lifestyle',
        subCategory: 'health',
        imgpath: 'my_setting_health.png',
        id: 'category26'
      }, {
        category: 'lifestyle',
        subCategory: 'travel',
       imgpath: 'my_setting_travel.png',
        id: 'category27'
      },
      {
        category: 'lifestyle',
        subCategory: 'quotes',
        imgpath: 'my_setting_quotes.png',
        id: 'category28'
      },
      {
        category: 'lifestyle',
        subCategory: 'workout',
        imgpath: 'my_setting_workout.jpg',
        id: 'category29'
      },
      {
        category: 'hobbies',
        subCategory: 'books',
        imgpath: 'my_setting_books.jpg',
        id: 'category29'
      },
      {
        category: 'hobbies',
        subCategory: 'diy',
        imgpath: 'my_setting_diy.jpg',
        id: 'category29'
      },
      {
        category: 'hobbies',
        subCategory: 'photography',
        imgpath: 'my_setting_photography.jpg',
        id: 'category29'
      },

    ]

    var init = function () {
      $scope.menuConfigs = menuConfigs;

      userService.getPreferences().then(function (preferencesData) {
        var selectedItems = {};
        var categoriesData = preferencesData;
        if (preferencesData && preferencesData.preferences) {
          categoriesData = preferencesData.preferences;
        }
        if (preferencesData && preferencesData.personalized) {
          $scope.personalisedArr = preferencesData.personalized;
        }

        _.each(categoriesData, function (categoryData) {
          var categoryId = angular.copy(categoryData.category);
          selectedItems[categoryId] = {}
          _.each(categoryData.subCategories, function (subCategoryId) {
            if ((categoryData.category === "infotainment" && subCategoryId === "health") || (categoryData.category === "infotainment" && subCategoryId === "travel")) {
              categoryId = "lifestyle";
              if (!selectedItems[categoryId]) {
                selectedItems[categoryId] = {};
              }
            }
            selectedItems[categoryId][subCategoryId] = true;
          });

          $scope.selectedItems = selectedItems;
          if (Object.keys($scope.selectedItems).length > 0) {
            $scope.settingAvailable = true;
          }
          $scope.parentobj.hideAlias = true;
        });
      }, function (error) {

      });

      categoryService._getsuggestedTopics().then(function (Topics) {
        $timeout(function () {
          $scope.suggestedCategories = Topics;
        });
      }, function (error) {

      });
    }

    var getSelections = function () {
      userPreferences = [];
      $scope.errorMsg = undefined;
      selectedCount = 0;
      _.each($scope.selectedItems, function (subCategoriesData, key) {
        var categoryPreferences = { category: key, subCategories: [] }
        _.each(subCategoriesData, function (selected, key) {
          if (selected == true) {
            categoryPreferences.subCategories.push(key)
            selectedCount++;
          }
        })
        if (categoryPreferences.subCategories.length > 0) {
          userPreferences.push(categoryPreferences)
        }
      });
    }

    $scope.next = function () {
      getSelections();
      if(userPreferences.length>0){
        preferencesObj.preferences = userPreferences;
      }   
      if($scope.personalisedArr.length>0){
        preferencesObj.personalized =  $scope.personalisedArr;
      }    
      
      if (selectedCount < constantService.settings.minSubCategory) {
        alert("Select minimum " + constantService.settings.minSubCategory + " topics!");
        return;
      }else{
        userService.savePreferences(preferencesObj).then(function () {
          $location.url('/custom-settings');
        }, function () {
          $scope.errorMsg = "Failed to save the preferences.  Try after some time..."
        })
      }
    }

    $scope.done = function (multideviceaccess, $event) {
      getSelections();
      if(userPreferences.length>0){
        preferencesObj.preferences = userPreferences;
      } 
      if($scope.personalisedArr.length>0){
        preferencesObj.personalized =  $scope.personalisedArr;
      }      
      if ($event) {
        $event.stopPropagation();
      }
      waitForLoginComplete = false;

      userService.savePreferences(preferencesObj).then(function () {
        $location.url('/home/mytrender');
      }, function () {
        $scope.errorMsg = "Failed to save the preferences.  Try after some time..."
      })
    }

    $scope.showCategorySuggession = function ($event, categoryId) {
      if ($event) {
        $event.stopPropagation();
      }

      $scope.categorySuggession[categoryId] = true;
    }

    $scope.addPersonalize = function (element) {
      if ($scope.personalise.value == undefined) {
        alert("Please enter your interest!");
        return false;
      } else if ($scope.personalise.value.length < 3) {
        alert("Please enter minimum 3 characters!");
        return false;
      } else if ($scope.personalisedArr.length >= constantService.settings.minPersonalize) {
        alert("You can follow maximum " + constantService.settings.minPersonalize + " specific content!");
        if (element) {
          $(element.offsetParent).removeClass("imgChked");
        }
        $scope.personalise.value = '';
        return false;
      } else {
        if ($scope.personalisedArr.indexOf($scope.personalise.value) > -1 || $scope.personalisedArr.indexOf($scope.personalise.value.charAt(0).toUpperCase() + $scope.personalise.value.substr(1).toLowerCase()) > -1 || $scope.personalisedArr.indexOf($scope.personalise.value.toLowerCase()) > -1 || $scope.personalisedArr.indexOf($scope.personalise.value.toUpperCase()) > -1) {
          alert("This value already exists.Please add unique interest!!");
          if (element && $(element.offsetParent)) {
            $(element.offsetParent).removeClass("imgChked");
          }
          $scope.personalise.value = '';
          return false;
        }
        if (angular.element('img[name="' + $scope.personalise.value + '"]').length > 0) {
          angular.element('img[name="' + $scope.personalise.value + '"]').parent().addClass("imgChked");
        }
        else if (angular.element('img[name="' + $scope.personalise.value.toLowerCase() + '"]').length > 0) {
          angular.element('img[name="' + $scope.personalise.value.toLowerCase() + '"]').parent().addClass("imgChked");
        }
        else if (angular.element('img[name="' + $scope.personalise.value.toUpperCase() + '"]').length > 0) {
          angular.element('img[name="' + $scope.personalise.value.toUpperCase() + '"]').parent().addClass("imgChked");
        }
        else {
          var capitalized = $scope.personalise.value;
          capitalized = capitalized.charAt(0).toUpperCase() + capitalized.substr(1).toLowerCase();
          angular.element('img[name="' + capitalized + '"]').parent().addClass("imgChked");
        }

        $scope.personalisedArr.push($scope.personalise.value);
        $scope.personalise.value = undefined;
      }
    }

    $scope.dropPersonalize = function (idx, value, element) {
      if (element) {
        _.each($scope.personalisedArr, function (selected, key) {
          if (selected == element || selected == element.toUpperCase() || selected == element.toLowerCase() || selected == (element.charAt(0).toUpperCase() + element.substr(1).toLowerCase())) {
            $scope.personalisedArr.splice(key, 1);
          }
        })
      }
      else {
        $scope.personalisedArr.splice(idx, 1);
        if (angular.element('img[name="' + value + '"]').length > 0) {
          angular.element('img[name="' + value + '"]').parent().removeClass("imgChked");
        }
        else if (angular.element('img[name="' + value.toLowerCase() + '"]').length > 0) {
          angular.element('img[name="' + value.toLowerCase() + '"]').parent().removeClass("imgChked");
        }
        else if (angular.element('img[name="' + value.toUpperCase() + '"]').length > 0) {
          angular.element('img[name="' + value.toUpperCase() + '"]').parent().removeClass("imgChked");
        }
        else {
          value = value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
          angular.element('img[name="' + value + '"]').parent().removeClass("imgChked");
        }
      }
    };

    init();
  }]);
