'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:TrendingCtrl
 * @description
 * # TrendingCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('TrendingCtrl', ['$window', '$scope', '$filter', '$rootScope', '$route', '$routeParams', '$timeout', '$location', 'cardViewFactory',
    'countryService', 'constantService', 'storageService', 'waitUtilService', function ($window, $scope, $filter, $rootScope, $route, $routeParams, $timeout, $location, cardViewFactory,
      countryService, constantService, storageService, waitUtilService) {
      var autocompleteCardViewService = undefined;
      var timeOutArr = [];
      var _prevReq;
      var columns;
      var _prevCols;
      var columnIndx;
      var timeout = undefined;
      $scope.all_feeds = [];
      $scope.feeds = [];
      $scope.feeds_grp_0 = [];
      $scope.feeds_grp_1 = [];
      $scope.feeds_grp_2 = [];
      $scope.feeds_grp_3 = [];
      $scope.pageNo = 0;

      $rootScope.$on('logged_into_server', function (event, status) {
        $location.url("/");
      });

      this.init = function () {
        waitUtilService.showWait();
        var country = $routeParams.country || countryService.getCountry();

        $scope.mediaType = 'video';
        var allFeedInfos = [];
        $scope.allFeedInfos = [];
        var hashcodeVal = undefined;
        var mediatypes = $scope.mediaType;
        var category = undefined;
        var subCategory = undefined;
        var pageType = 'topHome';
        cardViewFactory.create({ "country": country, "category": category, "subcategory": subCategory, "pageType": pageType }, {
          onData: function (categoryFeeds) {
            waitUtilService.hideWait();
            var feedInfo = [];
            _.each(categoryFeeds.data, function (categoryFeed, categoryIndex) {
              feedInfo = feedInfo.concat(categoryFeed.feeds);
            });
            
            feedInfo = feedInfo.sort(sortByTime);
            
            _.each(feedInfo, function (itemData, index) {
              itemData.index = index + 1;
              //itemData.hrsAgo=$filter('dateToHours')(itemData.postedTimestamp);      
              $scope.all_feeds = $scope.all_feeds.concat(itemData);
            });
            $scope.getNextPage();
            // timeout = $timeout(function () {
            //   this.loadAutoCompleteList();
            // }.bind(this), 5000);
            // timeOutArr.push(timeout);
          }.bind(this), onError: function (error) {
            //error in API call
            waitUtilService.hideWait();
          }
        });
      }

      $scope.getNextPage = function () {
        columns = _returnColumns();
        _prevCols = columns;
        var feeds = $scope.all_feeds.slice($scope.pageNo * 15, ($scope.pageNo * 15 + 15));
        if (feeds.length > 0) {
          _.each(feeds, function (itemData, index) {
            columnIndx = (itemData.index % columns);
            if (columnIndx != 0) {
              $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
            } else {
              $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
            }
            $scope.feeds = $scope.feeds.concat(itemData);
            console.log($scope["feeds_grp_" + (columns - 1)].length)
          });
          $scope.pageNo += 1;
        }
        _prevReq = undefined;
      }

      angular.element($window).bind('resize', function () {
        var timeoutItem = $timeout(function () {
          $scope._arrangeColsFeeds();
        }.bind(this));
        timeOutArr.push(timeoutItem);
      });

      $scope._arrangeColsFeeds = function () {
        columns = _returnColumns();
        if (_prevCols != columns) {
          console.log('arranging...')
          _prevCols = columns;
          $scope.feeds_grp_0.length = 0;
          $scope.feeds_grp_1.length = 0;
          $scope.feeds_grp_2.length = 0;
          $scope.feeds_grp_3.length = 0;
          angular.forEach($scope.feeds, function (itemData, index) {
            columnIndx = (itemData.index % columns);
            if (columnIndx != 0) {
              $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
            } else {
              $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
            }
          });
        } else {

        }
      }

      this.loadAutoCompleteList = function () {
        var country = $routeParams.country || countryService.getCountry();
        var category = $routeParams.category;
        var subcategory = $routeParams.subcategory;
        var pageType = 'autoComplete';
        var d = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
        storageService.get(constantService.storageDefaultCategory, "searchcontent_" + category + "_" + country, "searchcontent_" + category + "_" + country).then(function (item) {
          if (item) {
            if (item.lastDate != d) {
              $timeout(function () {
                storageService.remove(constantService.storageDefaultCategory, "searchcontent_" + category + "_" + country);
                fetchAndStoreAutocomplete(country, category, pageType);
              });
            }
          }
          else {
            timeout = $timeout(function () {
              fetchAndStoreAutocomplete(country, category, pageType);
            });
            timeOutArr.push(timeout);
          }
        });
      }

      function sortByTime(a, b) {
        var aTime = $filter('dateToMinutes')(a.postedTimestamp);
        var bTime = $filter('dateToMinutes')(b.postedTimestamp);
        /*if (aTime == 0) aTime = 1;
        if (bTime == 0) bTime = 1;*/
        if (aTime > bTime) return 1;
        if (aTime < bTime) return -1;
        return 0;
      }

      function fetchAndStoreAutocomplete(country, category, pageType) {
        var d = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();

        autocompleteCardViewService = cardViewFactory.create({ "country": country, "category": category, "pageType": pageType }, {
          onData: function (feeds) {
            if (feeds.data.length > 0) {
              storageService.set(constantService.storageDefaultCategory, "searchcontent_" + category + "_" + country, { lastDate: d, data: feeds.data }).then(function (item) {
                $rootScope.$emit('loadAutoComplete', { category: category, country: country });
              })
                .catch(function (response) {

                });
            }
          }, onError: function (error) {
          }
        });
      }

      $scope.next = function () {
        if (!_prevReq) {
          _prevReq = true;
          $scope.getNextPage();
        }
      }

      $scope.$on('$destroy', function () {
        angular.forEach(timeOutArr, function (timeoutItem) {
          $timeout.cancel(timeoutItem);
        });
      });

      $scope.$on('$locationChangeStart', function () {
        angular.forEach(timeOutArr, function (timeoutItem) {
          $timeout.cancel(timeoutItem);
        });
      });

      $rootScope.$on('country_change', function (event, country) {
        console.log('receive country_change...')
        console.log(country)
        countryService.resolveAndGet(country).then(function () {
          $route.reload();
        });
      });
      this.init();
    }]);
