'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:FeedbackdialogCtrl
 * @description
 * # FeedbackdialogCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('FeedbackdialogCtrl', ['$rootScope', '$scope', '$mdDialog', 'userService', 'feedbackService', function ($rootScope,$scope, $mdDialog, userService, feedbackService) {
    if (userService.isLoggedIn) {
      //TODO
      $scope.email = '';
    }

    $scope.submitFeedback = function () {
      if ($scope.subject && $scope.message && $scope.email) {
        feedbackService.submit($scope.subject, $scope.message, $scope.email).then(function () {
          alert('Feedback submitted successfully');
          $scope.subject = '';
          $scope.message = '';
          $scope.email = '';
          $mdDialog.hide();
        }, function () {
          alert('Failed to submit the feedback...Please try after some time...')
        })
      }
    }

    $scope.cancel = function () {
      $mdDialog.hide();
    }
  }]);

