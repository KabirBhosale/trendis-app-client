'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
.controller('MainCtrl',['$scope', '$timeout', '$mdSidenav', '$mdDialog',function ($scope, $timeout, $mdSidenav, $mdDialog) {
  $scope.toggleLeft = buildToggler('left');
  $scope.toggleRight = buildToggler('right');

  function buildToggler(componentId) {
    return function() {
      $mdSidenav(componentId).toggle();
    };
  }

  $scope.showFilter = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'views/filter.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: false // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
      console.log('You said the information was "' + answer + '".');
    }, function() {
      console.log('You cancelled the dialog.');
    });
  };

  function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };
  }

  $scope.tabs = [
    { title: 'All', content: 'Woah...that is a really long title!' },
    { title: 'Daily Humor', content: "Tabs will become paginated if there isn't enough room for them."},
    { title: 'Comics', content: "You can swipe left and right on a mobile device to change tabs."},
    { title: 'Gags', content: "You can bind the selected tab via the selected attribute on the md-tabs element."},
    { title: 'Sports', content: "If you set the selected tab binding to -1, it will leave no tab selected."},
    { title: 'Cricket', content: "If you remove a tab, it will try to select a new one."},
    { title: 'Football', content: "There's an ink bar that follows the selected tab, you can turn it off if you want."},
    { title: 'Tennis', content: "If you set ng-disabled on a tab, it becomes unselectable. If the currently selected tab becomes disabled, it will try to select the next tab."},
    { title: 'Fights', content: "If you look at the source, you're using tabs to look at a demo for tabs. Recursion!"},
    { title: 'Am. Football', content: "If you set md-theme=\"green\" on the md-tabs element, you'll get green tabs."},
    { title: 'Basketball', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Baseball', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Golf', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Movies', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Music', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Celebrity Gossip.', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Interesting', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Auto', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Tech', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Daily News', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Beauty', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Food', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Health', content: "If you're still reading this, you should just go check out the API docs for tabs!"},
    { title: 'Travel', content: "If you're still reading this, you should just go check out the API docs for tabs!"}
  ]
}]);
