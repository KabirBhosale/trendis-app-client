'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:ContentFilterCtrl
 * @description
 * # ContentFilterCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('ContentFilterCtrl', ['$mdDialog','$rootScope', '$scope', '$timeout', '$interval', '$route', '$routeParams', 'countryService', 'constantService', 'contentFilterService', function ($mdDialog,$rootScope, $scope, $timeout, $interval, $route, $routeParams, countryService, constantService, contentFilterService) {
    $scope.mediaTypes = [];
    $scope.constantService = constantService;
    $scope.networks = [];

    var category = undefined;
    var subcategory = undefined;
    var storageMediaKey = undefined;
    var storageNetworkKey = undefined;
    
    $scope.showFilter = function (ev) {
      $rootScope.isDialogOpen=true;
      $mdDialog.show({
        //controller: DialogController,
        scope: $scope.$new(),   // Uses prototypal inheritance to gain access to parent scope
        templateUrl: 'views/filter.html',
        //contentElement: '#myFilterDialog',
        parent: angular.element(document.body),
        targetEvent: ev,
        multiple: true,
        clickOutsideToClose: true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
        .then(function (answer) {
          $rootScope.isDialogOpen=false;
          console.log('You said the information was "' + answer + '".');
        }, function () {
          $rootScope.isDialogOpen=false;
          console.log('You cancelled the dialog.');
        });
    };

  $scope.hide = function () {
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    $mdDialog.cancel();
  };

  $scope.answer = function (answer) {
    $mdDialog.hide(answer);
  };

  $scope.init = function (inputCategory, inputSubcategory) {
    $scope.country = [countryService.getCountry()];
    storageMediaKey = constantService.storageMediaTypeKey;
    storageNetworkKey = constantService.storageNetworkKey;
    category = inputCategory || $routeParams.category;
    subcategory = inputSubcategory || $routeParams.subcategory;
    $scope.inputCategory = inputCategory;
    $scope.inputSubcategory = inputSubcategory;
    $scope._deviceType = _deviceType();
    if ($routeParams.searchtext) {
      category = constantService.searchCategory;
      subcategory = constantService.searchSubcategory;
      $scope.inputCategory = constantService.searchCategory;
      $scope.inputSubcategory = constantService.searchSubcategory;
      storageMediaKey = constantService.storageSearchMediaTypeKey;
      storageNetworkKey = constantService.storageSearchNetworkKey;
      contentFilterService.get(category, subcategory, constantService.storageSearchCountries).then(function (_country) {
        if (_country) {
          $scope.country = _country;
        } else {
          $scope.country = [constantService.country.IN, constantService.country.US];
        }
      });
    } else {//reset search filter

    }
    contentFilterService.get(category, subcategory, storageNetworkKey).then(function (networks) {
      // if ((networks.length == 0 || ($scope._deviceType == 'md' || $scope._deviceType == 'lg')) && (networks.length == Object.keys(constantService.feedTypes).length)) {
      //   $scope.networks = [];
      // }
      // else {
      //   $scope.networks = networks;
      // }
      $scope.networks = networks;
    });
    contentFilterService.get(category, subcategory, storageMediaKey).then(function (mediaTypes) {
      $timeout(function () {
        $scope.mediaTypes = mediaTypes;
        if (((category == 'humor' && subcategory == 'dailyhumor') || (inputCategory == 'humor' && inputSubcategory == 'dailyhumor')) && $scope.mediaTypes.indexOf('link') > -1) {
          if ($scope.mediaTypes.length == 1) {
            $scope.mediaTypes[0] = 'video';
          }
          else {
            $scope.mediaTypes.splice($scope.mediaTypes.indexOf('link'), 1);
          }
        }
      })
    })
  }

    $scope.contains = function (mediaType) {
      return ($scope.mediaTypes.indexOf(mediaType) != -1)
    }

    $scope.containscountry = function (country) {
      return ($scope.country.indexOf(country) != -1)
    }

    $scope.stopFilterBlink = function () {
      $('.filter-options').removeClass('blink_me');
    }

    $scope.containsNetwork = function (network) {
      if($scope.networks.length==0){
        return true;
      }else{
        network = getShortNetworkName(network);
        return ($scope.networks.indexOf(network) != -1)
      }
    }

    $scope.toggleNetwork = function (network) {
      network = getShortNetworkName(network);
      var networkIndex = $scope.networks.indexOf(network);
      if (networkIndex == -1) {
        $scope.networks.push(network);
      } else {
        if ($scope.networks.length == 1) {
          alert('Need to select one option')
          return;
        }
        $scope.networks.splice(networkIndex, 1);
      }
      // if (network != constantService.defaultFeedType && network != '') {
      //   network = getShortNetworkName(network);
      //   var networkIndex = $scope.networks.indexOf(network);
      //   if (networkIndex == -1) {
      //     $scope.networks.push(network);
      //     // if (($scope._deviceType == 'md' || $scope._deviceType == 'lg') && $scope.networks.length == Object.keys(constantService.feedTypes).length) {
      //     //   $scope.networks = [];
      //     // }
      //   } else {
      //     $scope.networks.splice(networkIndex, 1);
      //   }
      // } 
      // else {
      //   $scope.networks = [];
      // }

      // if ($scope._deviceType == 'md' || $scope._deviceType == 'lg') {
      //   console.log('Networks :' + $scope.networks)
      //   contentFilterService.set(category, subcategory, $scope.networks, storageNetworkKey).then(function () {
      //     $route.reload();
      //   });
      // }
    }

    $scope.toggleMediaType = function (mediaType) {
      var mediaIndex = $scope.mediaTypes.indexOf(mediaType)
      if (mediaIndex == -1) {
        $scope.mediaTypes.push(mediaType);
      } else {
        if ($scope.mediaTypes.length == 1) {
          alert('Need to select one option')
          return;
        }
        $scope.mediaTypes.splice(mediaIndex, 1);
      }

      category = $scope.inputCategory;
      subcategory = $scope.inputSubcategory;

      // contentFilterService.set(category, subcategory, $scope.mediaTypes).then(function () {
      // 	$route.reload();
      // });

      // if ($scope._deviceType == 'md' || $scope._deviceType == 'lg') {
      //   contentFilterService.set(category, subcategory, $scope.mediaTypes, storageMediaKey).then(function () {
      //     $route.reload();
      //   });
      // }
    }

    $scope.toggleCountry = function (country) {
      var countryIndex = $scope.country.indexOf(country);
      if (countryIndex == -1) {
        if (category != constantService.searchCategory) {
          $scope.country = [];
        }
        $scope.country.push(country);
      } else {
        if ($scope.country.length == 1) {
          alert('Need to select one option')
          return;
        }
        $scope.country.splice(countryIndex, 1);
      }
    }

    $scope.applyFilters = function () {
      contentFilterService.set(category, subcategory, $scope.mediaTypes, storageMediaKey).then(function (res) {
        contentFilterService.set(category, subcategory, $scope.networks, storageNetworkKey).then(function (res) {
          if (category != constantService.searchCategory) {
            $scope.$emit('country_change', $scope.country[0]);
            $mdDialog.hide();
            $route.reload();
            //angular.element('.ml-filter-modal').modal('hide');
          } else {
            contentFilterService.set(category, subcategory, $scope.country, constantService.storageSearchCountries).then(function (res) {
              var searchCountryId = 'all';
              if ($scope.country.length == 1) {
                searchCountryId = $scope.country[0];
              }
              $scope.filterSearch('country', searchCountryId);
              $mdDialog.hide();
              //angular.element('.ml-filter-modal').modal('hide');
            });
          }
        });
      });
    }

    $scope.$on('$routeChangeSuccess', function (event, next, current) {
      window._loq.push(["tag", "{" + $scope.mediaTypes + "}"]); // this will tag, won't star, and will append the tag

      if (current && next && current.params && next.params && current.params.searchtext && current.params.searchtext != next.params.searchtext) {
        contentFilterService.set(constantService.searchCategory, constantService.searchSubcategory, [constantService.mediaType.photo, constantService.mediaType.video, constantService.mediaType.link], constantService.storageSearchMediaTypeKey);
        contentFilterService.set(constantService.searchCategory, constantService.searchSubcategory, [constantService.country.IN, constantService.country.US], constantService.storageSearchCountries);
        contentFilterService.set(constantService.searchCategory, constantService.searchSubcategory, [getShortNetworkName(constantService.feedTypes.Facebook), getShortNetworkName(constantService.feedTypes.Youtube), getShortNetworkName(constantService.feedTypes.Twitter), getShortNetworkName(constantService.feedTypes.Instagram), getShortNetworkName(constantService.feedTypes.Reddit)], constantService.storageSearchNetworkKey);
      }
      $scope.init();
    });
  }]);
