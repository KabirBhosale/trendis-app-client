'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
