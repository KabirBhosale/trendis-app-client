'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:HomeCtrl
 * @description
 * # LoginCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('HomeCtrl', ['$rootScope', '$timeout', '$mdDialog', '$scope', '$location', '$interval', 'userService', 'countryService', 'constantService', 'storageService', 'albumService', 'favoriteService', function ($rootScope, $timeout, $mdDialog, $scope, $location, $interval, userService, countryService, constantService, storageService, albumService, favoriteService) {
    // storageService.get(constantService.storageDefaultCategory, "lastLoggedInID", "lastLoggedInID").then(function (item) {
    //   if (item) {
    //     $location.url("/home/mytrender");
    //   }
    // });

    // if (userService.isLoggedIn()) {
    //   $location.url("/home/mytrender");
    // }

    $scope.init=function(){
      $mdDialog.show({
        scope: $scope,
        parent: angular.element('#landing'),
        preserveScope: true,
        templateUrl: 'views/home-dialog.html',
        clickOutsideToClose: false
        //controller: 'FeedbackdialogCtrl',
        //bindToController: true
      });
      $timeout(function(){
        angular.element('.md-dialog-container').css('top','0');
      },500);
    }
    

    $scope.countryService = countryService;
    var menuConfigs = constantService.menuConfigs;

    var stop;
    var curMenuImgCnt = 0;

    var item = menuConfigs[Math.floor(Math.random() * menuConfigs.length)];
    $scope.cardImgStyle = {
      "background-image": "url(/images/landing-page/" + item.id + ".jpg)",
      "background-size": "cover",
      "background-position": "50% 0%"
    }

    $scope.innerStyle = {
      "background-image": "url('../images/landing-page/" + item.id + "_back.jpg')"
    }

    $scope.login = function () {
      userService.login();
    }

    $scope.skip=function(){
      $location.url($scope.homeUrl.replace('#',''));
    }

    $rootScope.$on('logged_into_server', function (event, status) {
      $location.url($scope.homeUrl.replace('#',''));
    });

    $scope.redirectToAboutTrendis = function () {
      storageService.remove(constantService.storageDefaultCategory, "uservisit").then(function (result) {
        //$location.url('/about');
      });
    };

    stop = $interval(function () {
      if (curMenuImgCnt == menuConfigs.length) { curMenuImgCnt = 0; }
      var item = menuConfigs[curMenuImgCnt];
      $scope.cardImgStyle = {
        "background-image": "url(/images/landing-page/" + item.id + ".jpg)",
        "background-size": "cover",
        "background-position": "50% 0%"
      }
      curMenuImgCnt++;
    }, 5000);

    $scope.$on('$destroy', function () {
      // angular.element(".modal-backdrop").remove()
      // angular.element("body").removeClass('modal-open')
      // angular.element("body").css('padding-right', 'initial')
      // angular.element("#dvHeaderPanel").show()

      if (angular.isDefined(stop)) {
        $interval.cancel(stop);
        stop = undefined;
      }
    });
  }]);
