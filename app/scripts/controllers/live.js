'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:LiveCtrl
 * @description
 * # LiveCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('LiveCtrl', ['$window', '$location', '$rootScope', '$scope', '$route', '$routeParams', '$timeout', '$q', '$analytics', 'favoriteService', 'cardViewFactory',
  'countryService', 'constantService', 'storageService', 'contentFilterService','waitUtilService', 'userService', function ($window, $location, $rootScope, $scope, $route, $routeParams, $timeout, $q, $analytics, favoriteService, cardViewFactory,
    countryService, constantService, storageService, contentFilterService,waitUtilService, userService) {
    $scope.feeds = []
    $scope.feeds_grp_0 = [];
    $scope.feeds_grp_1 = [];
    $scope.feeds_grp_2 = [];
    $scope.feeds_grp_3 = [];
    $scope.trendalyzerFeeds = []
    $scope.activeNotificationFeed = { title: 'Default' }

    var feedCardViewService = undefined
    var timeOutArr = [];
    var timeout = undefined;
    var myInterval = undefined;
    var _prevReq;
    var columns;
    var _prevCols;
    var columnIndx;
    var pageGenTime = undefined;

    this.loadFeed = function (sourceType) {
      waitUtilService.showWait();
      var country = $routeParams.country || countryService.getCountry();
      var userProperties=userService.getUserFbProperties();
      var gender = sourceType;//getShortGender(userProperties.gender) || getShortGender(constantService.defaultFbUserProp.gender);
      var age=userProperties.age || constantService.defaultFbUserProp.age;
      var pageType = 'live';
      var mediatype = constantService.mediaType.video;

      feedCardViewService = cardViewFactory.create({ "country": country, "gender": gender, "age": age, "pageType": pageType }, {
        onData: function (feeds) {
          columns = _returnColumns();
          waitUtilService.hideWait();
          _prevCols = columns;
          if (feeds.data.length > 0) {
            if (pageType === "live" && feeds.pageNo === 0) {//set page 0 data in local-storage
              $scope.feeds_grp_0.length = 0;
              $scope.feeds_grp_1.length = 0;
              $scope.feeds_grp_2.length = 0;
              $scope.feeds_grp_3.length = 0;
              angular.forEach(feeds.data, function (itemData, index) {
                if (index == 0 && itemData.genTime) {
                  pageGenTime = itemData.genTime;
                }
                itemData.is_newWindow=true;
                columnIndx = (itemData.index % columns);
                if (columnIndx != 0) {
                  $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
                } else {
                  $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
                }
                $scope.feeds[index] = itemData;
              });
              _prevReq = undefined;
            } else {
              angular.forEach(feeds.data, function (itemData, index) {
                if (index == 0 && itemData.genTime && itemData.genTime != pageGenTime) {
                  $route.reload();
                }
                itemData.is_newWindow=true;
                columnIndx = (itemData.index % columns);
                if (columnIndx != 0) {
                  $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
                } else {
                  $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
                }
                $scope.feeds[itemData.index - 1] = itemData;
              });
              _prevReq = undefined;
            }
          }
          else if (feeds.pageNo === 0 && feeds.data.length == 0) {
            angular.forEach(timeOutArr, function (tmItem) {
              $timeout.cancel(tmItem);
            });
            $scope.errors = "Whoops! Feeds not found for specified content filter and network filter, please check filters and try later.";
            $scope.feeds = [];
          }
        }.bind(this), onError: function (error) {
          waitUtilService.hideWait();
          $scope.errors = "Whoops! Feeds not found for specified content filter and network filter, please check filters and try later.";
        }
      });
    }

    angular.element($window).bind('resize', function () {
      var timeoutItem = $timeout(function () {
        $scope._arrangeColsFeeds();
      }.bind(this));
      timeOutArr.push(timeoutItem);
    });

    $scope._arrangeColsFeeds = function () {
      columns = _returnColumns();
      if (_prevCols != columns) {
        console.log('arranging....')
        _prevCols = columns;
        $scope.feeds_grp_0.length = 0;
        $scope.feeds_grp_1.length = 0;
        $scope.feeds_grp_2.length = 0;
        $scope.feeds_grp_3.length = 0;
        angular.forEach($scope.feeds, function (itemData, index) {
          columnIndx = (itemData.index % columns);
          if (columnIndx != 0) {
            $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
          } else {
            $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
          }
        });
      } else {

      }
    }

    this.init = function () {
      this.loadFeed();
    };

    $scope.next = function () {
      console.log('content filter popup opened : '+$rootScope.isDialogOpen)
      if (!_prevReq && !$rootScope.isDialogOpen) {
        _prevReq = true;
        feedCardViewService.getNextPage();
      }
    }

    $scope.$on('$destroy', function () {
      $rootScope.$broadcast('stopYbVideos');
      angular.forEach(timeOutArr, function (timeoutItem) {
        $timeout.cancel(timeoutItem);
      });
    });

    $scope.$on('$locationChangeStart', function () {
      angular.forEach(timeOutArr, function (timeoutItem) {
        $timeout.cancel(timeoutItem);
      });
    });

    $rootScope.$on("_remove_loading", function (e, feedType, element_id) {
      if (myInterval) {
        clearInterval(myInterval);
      }
      if (feedType == constantService.feedTypes.Twitter || feedType == constantService.feedTypes.Instagram || feedType == constantService.feedTypes.Reddit) {
        angular.element('.iframe-body').css('background', 'none');
        angular.element('.iframe-body').css('background', 'none');
        if (angular.element('.iframe-body').parent() && angular.element('.iframe-body').prev()) {
          angular.element('.iframe-body').css('height', (angular.element('.iframe-body').parent().height() - angular.element('.iframe-body').prev().height() - 30) + 'px');
        }
      } if (feedType == constantService.feedTypes.Facebook) {
        myInterval = setInterval(function () {
          if (angular.element('#' + element_id).attr("fb-xfbml-state") == "rendered") {
            clearInterval(myInterval);
            myInterval = undefined;
            angular.element('.iframe-body').css('background', 'none');
            if (angular.element('.iframe-body').parent() && angular.element('.iframe-body').prev()) {
              angular.element('.iframe-body').css('height', (angular.element('.iframe-body').parent().height() - angular.element('.iframe-body').prev().height() - 20) + 'px');
            }
          }
        }, 500);
      }
    });

    $rootScope.$on("changeFavouriteFlag", function (event, feedObj) {
      var oldFeedObj = _.filter($scope.feeds, { feedKey: feedObj.feedKey })[0];
      if (oldFeedObj) {
        if (feedObj.favorites == true) {
          oldFeedObj.favorites = true;
          $rootScope.$emit('add_feedfav', oldFeedObj);
        }
        else {
          oldFeedObj.favorites = false;
          $rootScope.$emit('delete_feedfav', oldFeedObj);
        }
      }
    });

    $rootScope.$on('filterLive', function (event, source) {
      this.loadFeed(source);
    }.bind(this));

    $rootScope.$on('login_status_change', function (event, status) {
      $route.reload();
    });

    this.init();
  }]);
