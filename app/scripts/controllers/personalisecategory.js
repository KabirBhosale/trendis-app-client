'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:PersonalisecategoryCtrl
 * @description
 * # PersonalisecategoryCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('PersonalisecategoryCtrl', ['$rootScope', '$window', '$scope', '$timeout', '$routeParams', 'constantService', 'cardViewFactory','waitUtilService', function ($rootScope, $window, $scope, $timeout, $routeParams, constantService, cardViewFactory, waitUtilService) {
    $scope.feeds = [];
    $scope.feeds_grp_0 = [];
    $scope.feeds_grp_1 = [];
    $scope.feeds_grp_2 = [];
    $scope.feeds_grp_3 = [];
    var feedCardViewService = undefined;
    var timeOutArr = [];
    var _prevReq;
    var columnIndx;
    var columns;
    var _prevCols;
    var timeout = undefined;
    var country = undefined;
    var category = undefined;
    var subcategory=undefined;
    var timelimit = undefined;
    var pageType = constantService.searchCategory;
    var searchText = undefined;
    var sortBy = 't';
    //function
    this.loadFeeds = function (searchCriteria) {
      waitUtilService.showWait();
      country = $routeParams.country || countryService.getCountry();
      searchText = $routeParams.personalise_item;
      var request_obj={ "country": country, "category": category, "timelimit": timelimit, "pageType": pageType, "sortBy": sortBy, "searchText": searchText, "is_personalise": true };
      if(!$routeParams.personalise_item){
        category = $routeParams.custom_category || constantService.defaultCategory;
        subcategory = $routeParams.custom_subcategory || constantService.defaultSubCategory;
        pageType = 'top';
        request_obj={ "country": country, "category": category, "subcategory": subcategory, "pageType": pageType };
      }
      
      feedCardViewService = cardViewFactory.create(request_obj, {
        onData: function (feeds) {
          waitUtilService.hideWait();
          columns = _returnColumns();
          _prevCols = columns;
          $scope.errors = "";
          if (feeds.data.length > 0) {
            angular.forEach(feeds.data, function (itemData, index) {
              columnIndx = (itemData.index % columns);
              itemData.searchQuery = searchText;
              if (columnIndx != 0) {
                $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
              } else {
                $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
              }
              $scope.feeds = $scope.feeds.concat(itemData);
            });
            _prevReq = undefined;
          }
          else if (feeds.pageNo === 0 && feeds.data.length == 0) {
            angular.forEach(timeOutArr, function (tmItem) {
              $timeout.cancel(tmItem);
            });
            $scope.errors = "Whoops! Feeds not found for specified content filter and network filter, please check filters and try later.";
            $scope.feeds = [];
          }
          else {
            //angular.element("#feeds_loader").hide();
          }
        }.bind(this), onError: function (error) {
          if (feeds.pageNo === 0) {
            $scope.feeds = [];
          }
          //load from local-storage content
        }
      });
    }

    angular.element($window).bind('resize', function () {
      var timeoutItem = $timeout(function () {
        $scope._arrangeColsFeeds();
      }.bind(this));
      timeOutArr.push(timeoutItem);
    });

    $scope._arrangeColsFeeds = function () {
      columns = _returnColumns();
      if (_prevCols != columns) {
        console.log('arranging...')
        _prevCols = columns;
        $scope.feeds_grp_0.length = 0;
        $scope.feeds_grp_1.length = 0;
        $scope.feeds_grp_2.length = 0;
        $scope.feeds_grp_3.length = 0;
        angular.forEach($scope.feeds, function (itemData, index) {
          columnIndx = (itemData.index % columns);
          if (columnIndx != 0) {
            $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
          } else {
            $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
          }
        });
      } else {

      }
    }

    this.init = function () {
      this.loadFeeds();
    };

    $scope.next = function () {
      if (!_prevReq && !$rootScope.isDialogOpen) {
        _prevReq = true;
        feedCardViewService.getNextPage();
      }
    }

    this.init();
  }]);
