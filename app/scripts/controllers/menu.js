'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:MenuCtrl
 * @description
 * # MenuCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp').controller('MenuCtrl', ['$rootScope', '$routeParams', '$anchorScroll', '$scope', '$route', '$window', '$document', '$location', '$timeout', 'constantService', '_', 'countryService', 'userService', 'storageService', '$mdSidenav', '$mdDialog', '$mdComponentRegistry', '$mdPanel', '$mdMedia', function ($rootScope, $routeParams, $anchorScroll, $scope, $route, $window, $document, $location, $timeout, constantService, _, countryService, userService, storageService, $mdSidenav, $mdDialog, $mdComponentRegistry, $mdPanel, $mdMedia) {
  $scope.toggleLeft = buildToggler('left');
  $scope.toggleRight = buildToggler('right');
  $rootScope.currentVideos=[];
  $scope.selectedIndex = 0;

  function buildToggler(componentId) {
    $scope.selectedIndex = 0;
    return function () {
      $mdSidenav(componentId).toggle();
    };
  }

  if ($mdMedia('xs')) {
    $scope._mediaScreen='xs';
    angular.element('#appbanner').css('display', 'inherit');     
  }else if ($mdMedia('sm')) {
    $scope._mediaScreen='sm';
  }

  $timeout(function(){
    $scope.hideAppBanner=true;
  },(60*1000));

  $scope.deviceos="android";
  var ua = navigator.userAgent || navigator.vendor || window.opera;
  if ((/iPad|iPhone|iPod/.test(ua) && !window.MSStream)) {//IOS
    $scope.deviceos="ios";
    $scope.hideAppBanner=true;
  }

  //..Route parameters
  var country = undefined;
  var category = undefined;
  var subcategory = undefined;
  var tabs = undefined;

  $scope.score = userService.score;
  $scope.isLoggedIn = false;
  $scope.loggedInUserName = undefined;
  $scope.allowedCountries = constantService.country;
  $scope.searchCriteria = {};
  


  $scope.searchInfo = {
    selectedCountry: { id: "all", value: "Any Country" },
    selectedCategory: { id: "all", value: "Any Category" },
    selectedTimeLimit: { id: "all", value: "Any Time" },
    selectedSortBy: { id: "t", value: "Sort By Time" }
  }

  $scope.filterInfo = {
    selectedCountry: { id: "all", value: "Any Country" },
    selectedCategory: { id: "all", value: "Any Category" },
    selectedMedia: { id: "yb", value: "Youtube" }
  }

  $scope.resetUserDefinedCategories = function () {
    $scope.userDefinedItems = angular.copy(constantService.homeDefaultCategories[countryService.getCountry()]);
  }
  
  $scope.changeCategory = function (_subcategory, personaliseTab) {
    var path = undefined;
    if (_subcategory == undefined) {
      path = '/trending';
    }else if(_subcategory=='my-settings'){
      if($scope._mediaScreen=='xs' || $scope._mediaScreen=='sm'){
        path = '/settings';
      }else{
        path = '/my-settings';
      }
    }else if (personaliseTab) {
      path = $scope.getPersonaliseUrl(_subcategory, true);
    } else {
      path = $scope.getMenuUrl(_subcategory, true);
    }

    $location.path(path);
  }

  $scope.isSubCategoryActive = function (menuConfig) {
    return (subcategory == menuConfig.id);
  }

  this.setParentId = function (menuConfigs) {
    _.forEach(menuConfigs, function (parentMenuConfig, index) {
      if (index < (menuConfigs.length - 1)) {
        menuConfigs[index].nextMenu = menuConfigs[index + 1];
      }
      if (index != 0) {
        menuConfigs[index].prevMenu = menuConfigs[index - 1];
      }

      if (parentMenuConfig.subMenuConfigs && parentMenuConfig.subMenuConfigs.length > 0) {
        _.forEach(parentMenuConfig.subMenuConfigs, function (subMenuConfig) {
          subMenuConfig.parentId = parentMenuConfig.id;
        })
      }
    })
  }

  var resetRouteParams = function () {
    country = undefined;
    category = undefined;
    subcategory = undefined;
  }

  var extractCategoryInfoFromUrl = function (url) {
    $scope.isDropdownItem = false;
    var urlSegments = url.split('/');
    if (urlSegments.length > 1) {
      category = urlSegments[1].toLowerCase();
    }
    if (urlSegments.length > 2) {
      subcategory = urlSegments[2].toLowerCase();
    }
    console.log('Category : ' + category + " - Sub-category : " + subcategory);
  }

  $scope.countWatchersOnPage = function () {
    var watchers = ($scope.$$watchers) ? $scope.$$watchers.length : 0;
    var child = $scope.$$childHead;
    while (child) {
      watchers += (child.$$watchers) ? child.$$watchers.length : 0;
      child = child.$$nextSibling;
    }
    return watchers;
  };

  $scope.getMenuUrl = function (menuConfig, excludeHash) {
    var url = "#/";
    if (excludeHash !== undefined && excludeHash === true) {
      url = "/";
    }

    var countrySpecific = false;
    if (menuConfig.countrySpecific == undefined || menuConfig.countrySpecific == true) {
      countrySpecific = true;
    }

    if (countrySpecific == true) {
      url += countryService.getCountry() + "/"
    }

    if (menuConfig.parentId == undefined) {
      url += menuConfig.id + '/';
      if (menuConfig.id === "home") {
        url += "custom/"
      }
      else if (countrySpecific == true) {
        url += "all/"
      }
    } else if ($scope.activeMenuConfig) {
      if ($scope.activeMenuConfig.id === "mytrender") {//01Apr16
        if (!$scope.isLoggedIn) {
        }
        url += menuConfig.id + '/';
        url += "all/"
      }
      else {
        url += $scope.activeMenuConfig.id + '/';
        url += menuConfig.id + '/';
      }
    }

    return url;
  }

  //Get personalise url
  $scope.getPersonaliseUrl = function (personaliseConfig, excludeHash) {
    var url = "#/personalise/";
    if (excludeHash !== undefined && excludeHash === true) {
      url = "/personalise/";
    }
    if (personaliseConfig.categoryId) {
      url += personaliseConfig.categoryId + '/';
      url += personaliseConfig.subcategoryId || personaliseConfig.id;
    } else {
      url += personaliseConfig.id;
    }
    return url + '/';
  }

  var slickInit = 0;

  this.registerLocationChange = function () {
    console.log('Trendis : Registering $routeChangeSuccess');
    $rootScope.currentVideos=[];
    $scope.$on('$routeChangeSuccess', function (event, next, current) {
      if(!current || !current.$$route || current.$$route.originalPath=="/my-settings"){
        $scope.resetUserDefinedCategories();
        loadPersonalisedMenus();
        $scope.selectedIndex = 0;
      }
      $rootScope.activeRoute=undefined;
      $scope.deviceType = _deviceType();
      if (!$routeParams.searchtext) {
        $scope.searchText = undefined;
        $scope.searchform=false;   
      }
      else {
        $scope.searchText = $routeParams.searchtext;
      }
      console.log('$routeChangeSuccess');
      $anchorScroll();
      if ($('html').hasClass('fp-enabled')) {
        $.fn.fullpage.destroy('all');//fullpage destroy call
        $('#dvHeader').css({ 'z-index': 'auto' });
        $('#dvmobilemenus').css({ 'z-index': 'auto' });
      }

      storageService.get(constantService.storageDefaultCategory, "searchcontent_" + $routeParams.category + "_" + $routeParams.country, "searchcontent_" + $routeParams.category + "_" + $routeParams.country).then(function (item) {
        if (item) {
          $scope.searchWords = item.data;
        }
      });

      if (next && next.params) {
        console.log('Trendis : ' + JSON.stringify(next.params));
        country = next.params.country;
        category = next.params.category;
        subcategory = next.params.subcategory;

        if (country == undefined || category == undefined) {
          extractCategoryInfoFromUrl($location.url());
        }

        //console.log(next.params);
        console.log('Route changed w param : ' + $location.url());
      } else {
        console.log('Route changed : ' + $location.url());
        extractCategoryInfoFromUrl($location.url());
      }

      category = category || constantService.defaultCategory;
      subcategory = subcategory || constantService.defaultSubCategory;

      $scope.activeMenuConfig = category != undefined ? _.find($scope.menuConfigs, 'id', category) : undefined;
      //for personalise Date :040117
      if (!$scope.activeMenuConfig && $routeParams.personalise_item) {
        $scope.activeMenuConfig = _.find($scope.menuConfigs, 'id', 'home');
        subcategory = $routeParams.personalise_item;
      } else if ($location.url().contains("personalise") || $location.url().contains("trending")) {
        $scope.activeMenuConfig = _.find($scope.menuConfigs, 'id', 'home');
      }
      tabs = $scope.activeMenuConfig;

      if ($scope.activeMenuConfig) {
        $scope.activeSubMenuConfig = subcategory != undefined ? _.find($scope.activeMenuConfig.subMenuConfigs, 'id', subcategory) : undefined;
        $rootScope.$emit("tooltipBoxEvent", $scope.activeMenuConfig.id);
      }

      if ($scope.activeSubMenuConfig && ($scope.activeSubMenuConfig.id == "sharepad" || $scope.activeSubMenuConfig.id == "trendalyzer")) {
        $rootScope.$emit("close_help_popover");
      }

      $scope.parentobj = {};
      angular.element("#dvHeaderPanel").show();
      if ($location.url().contains("settings")) {//010416
        $scope.activeMenuName = "settings";
        $scope.menuname = "settings";
        $scope.parentobj.hideAlias = false;
      }
      else if ($location.url().contains("sharepad")) {//010416
        $scope.menuname = "sharepad";
        $scope.activeMenuName = "sharepad";
        $scope.parentobj.hideAlias = false;
      }
      else if ($location.url().contains("copyright")) {//010416
        $scope.menuname = "copyright";
        $scope.activeMenuName = undefined;
        $scope.parentobj.hideAlias = false;
      }
      else if ($location.url().contains("about")) {//010416
        $scope.menuname = "about";
        $scope.activeMenuName = undefined;
        $scope.parentobj.hideAlias = false;
      }
      else if ($location.url().contains("live")) {//010416
        $scope.menuname = "live";
        $scope.activeMenuName = undefined;
        $scope.parentobj.hideAlias = false;
      }
      else if ($location.url().contains(constantService.searchCategory)) {
        $scope.menuname = constantService.searchCategory;
        $rootScope.activeRoute=constantService.searchCategory;
      }
      else if ($location.url().contains("personalise")) {
        $scope.menuname = "personalise";
      }
      else if ($location.url().contains("trending")) {
        $scope.menuname = "home";
      }
      else if ($location.url().contains("trends")) {
        $scope.menuname = "trends";
      }
      else {//010416
        //$scope.menuname = undefined;
        $scope.menuname=category;
        $scope.activeMenuName = undefined;
        $scope.parentobj.hideAlias = false;
      }
      
      if($scope.prevMenu && $scope.prevMenu!=$scope.menuname && $scope.menuname!='personalise'){
        $scope.selectedIndex=0;
        $scope.prevMenu=$scope.menuname;
      }else{
        $scope.prevMenu=$scope.menuname;
      }

      if (next.$$route && next.$$route.controller) {
        if (!userService.isLoggedIn() && $scope.activeMenuConfig && $scope.activeMenuConfig.personaliseItems) {
          $scope.activeMenuConfig.personaliseItems = [];
        }
      }

      if($scope.menuname!='trends'){
        angular.element('#header').show();
      }else{
        angular.element('#header').hide();
      }
    }.bind(this));
  }

  var swipedirection = function (element, direction) {
    var current = element;
    $rootScope.swiped = 0;
    while (current && current != document.body) {
      if ($(current).hasClass('feedcard-watermark') || current.getAttribute('swipe') == 'cancel') {
        $rootScope.swiped = 1;
        break;
      }
      current = current.parentElement;
    }
    if ($rootScope.swiped == 0) {
      if (direction < 0) {
        if ($scope.activeMenuConfig.prevMenu) {
          var url = $scope.getMenuUrl($scope.activeMenuConfig.prevMenu, true);
          $location.url(url);
        }
      }
      else {
        if ($scope.activeMenuConfig.nextMenu) {
          var url = $scope.getMenuUrl($scope.activeMenuConfig.nextMenu, true);
          $location.url(url);
        }
      }
    }
  }

  $scope.searchFeed = function (searchtxt) {
    $scope.searchInfo = {
      selectedCountry: { id: "all", value: "Any Country" },
      selectedCategory: { id: "all", value: "Any Category" },
      selectedTimeLimit: { id: "all", value: "Any Time" },
      selectedSortBy: { id: "t", value: "Sort By Time" }
    }
    if (!searchtxt || searchtxt.length < 3) {
      alert("Enter atleast 3 characters for search !");
      return;
    }
    $rootScope.searchCriteria = undefined;
    window._loq.push(["submit_form", "frm-search", true]);
    $location.url('/search/' + encodeURIComponent(searchtxt.slice(0, 100)));
    //$('#search-modal').modal('hide');
  }

  $scope.clearSearch = function () {
    if ($scope.searchText && $scope.searchText.trim() == "" && $routeParams.searchtext) {
      $scope.searchText = $routeParams.searchtext;
    }
  }

  $scope.moveCategory = function ($event, direction) {
    if (direction < 0) {
      //alert('back');
      if ($scope.activeMenuConfig.prevMenu) {
        var url = $scope.getMenuUrl($scope.activeMenuConfig.prevMenu, true);
        $location.url(url);
      }
    } else {
      //alert('forward');
      if ($scope.activeMenuConfig.nextMenu) {
        var url = $scope.getMenuUrl($scope.activeMenuConfig.nextMenu, true);
        $location.url(url);
      }
    }
  }

  this.init = function () {
    resetRouteParams();
    $scope.menuConfigs = constantService.menuConfigs;

    this.registerLocationChange();
    this.setParentId($scope.menuConfigs);

    $scope.countryService = countryService;

    $scope.$on('country_change', function (event, country) {
      console.log('receive country_change...');
      console.log('Trendis :' + country);
      countryService.resolveAndGet(country).then(function () {
        if ($routeParams.personalise_item) {//for personalise Date :040117
          $route.reload();
        }
        if ($scope.activeMenuConfig != undefined) {
          if ($scope.activeMenuConfig.countrySpecific == undefined || $scope.activeMenuConfig.countrySpecific == true) {
            var url = $location.url();
            var urlSegments = url.split('/')
            console.log(urlSegments)
            if (urlSegments.length > 2) {
              urlSegments[1] = country;
            }
            else {//KB
              urlSegments[1] = country;
            }
            $location.url(urlSegments.join("/"));
          }
        }
      });
    });

    $rootScope.$on("loadAutoComplete", function (event, searchConfig) {
      storageService.get(constantService.storageDefaultCategory, "searchcontent_" + searchConfig.category + "_" + searchConfig.country, "searchcontent_" + searchConfig.category + "_" + searchConfig.country).then(function (item) {
        if (item) {
          $scope.searchWords = item.data;
        }
      });
    });
    $rootScope.$on('login_status_change', function (event, status) {
      $scope.isLoggedIn = userService.isLoggedIn();
      $scope.loggedInUserName = userService.userName;
      $scope.loggedInUserPicture = userService.picture || constantService.defaultProfilePicture;
      $scope.score = userService.score;
      loadPersonalisedMenus();
      $scope.selectedIndex = 0;
    });

    $rootScope.$on('logged_into_server', function (event, status) {
      $scope.score = userService.score;

      loadPersonalisedMenus();
    });
  }

  this.init();

  $scope.login = function () {
    userService.login();
  }

  $scope.logout = function () {
    userService.logout();
  }

  var loadPersonalisedMenus = function () {
    //if ($scope.activeMenuConfig && $scope.activeMenuConfig.id == 'home') {      
    userService.getPreferences().then(function (preferencesData) {
      if (preferencesData) {
        $scope.userDefinedItems.length = 0;
      }else{
        $scope.userDefinedItems.length = 0;
        $scope.resetUserDefinedCategories();
      }
      if (preferencesData && preferencesData.personalized) {
        var tempPersonalized = [];
        _.each(preferencesData.personalized, function (per_item, per_index) {
          if (tempPersonalized.indexOf(per_item) == -1) {
            tempPersonalized.push(per_item);
            $scope.userDefinedItems.push({
              id: per_item.toLowerCase(),
              name: per_item,
              tags: [],
              countrySpecific: true
            });
          }
        });
      }

      if (preferencesData && preferencesData.preferences) {
        _.each(preferencesData.preferences, function (pref, pref_index) {
          _.each(pref.subCategories, function (pref_category) {
            $scope.userDefinedItems.push({
              id: pref_category.toLowerCase(),
              name: pref_category == 'all' ? pref.category : pref_category,
              countrySpecific: true,
              categoryId: pref.category
            });
          });
        });
      }
    }, function (err) {
      console.log('Getting personalise value error : ' + err);
    });
    //}
  }

  $scope.filterSearch = function (key, val) {
    if (key == 'category') {
      $scope.searchInfo.selectedTimeLimit = { id: "all", value: "Any Time" };
    } else if (key == 'country') {
      if (val != undefined) {
        $scope.searchInfo.selectedCountry = { id: val };
      }
    }
    $rootScope.$emit('filterSearch', $scope.searchInfo);
  }

  $scope.filterLive = function (val) {
    $scope.selectedLiveSource=val;
    $rootScope.$emit('filterLive', val);
  }

  $rootScope.$on("$routeChangeStart", function (event, current, previous) {
    console.log("routes change start : " + pendingAjaxReq.length);
    angular.forEach(pendingAjaxReq, function (p) {
      if (p.xhr.statusText != "OK" && (p.url.indexOf('/autocompletelist?') > -1 || p.url.indexOf('/search?') > -1 || p.url.indexOf('/feeds/topHome?') > -1 || p.url.indexOf('/feeds/top?') > -1)) {
        console.log('jqXHR pending : ' + p.url);
        p.xhr.abort();
      }
    });
    pendingAjaxReq.length = 0;
  });

  $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
    alert("failed to change routes");
  });

  //This sets up a trigger event when the sidenav closes
  $scope.sideNavIsOpen = function () {
    return false;
  };

  $mdComponentRegistry.when('left').then(function (sideNav) {
    $scope.sideNavIsOpen = angular.bind(sideNav, sideNav.isOpen);
  });

  $scope.$watch('sideNavIsOpen()', function () {
    if (!$scope.sideNavIsOpen()) {
      $('body').removeClass('not-scrollable');
      console.log('closed');
    }
    else {
      $('body').addClass('not-scrollable');
      console.log('open');
    }
  });

  $scope.openSearchBar=function(){
    $scope.searchform=true;
  }
  $scope.closeSearchBar=function(ev){
    $scope.searchform=false;   
  }
  $scope.searchBlurEvent=function(ev){
    if(!$scope.searchText){
      $scope.searchform=false;   
    }
  }

  $scope.feedback = function () {
    $rootScope.isDialogOpen=true;
    $mdDialog.show({
      clickOutsideToClose: true,
      //scope: $scope,
      preserveScope: true,
      multiple: true,
      templateUrl: 'views/feedback-dialog.html',
      controller: 'FeedbackdialogCtrl',
      bindToController: true
    })
    .then(function (answer) {
      $rootScope.isDialogOpen=false;
      console.log('You said the information was "' + answer + '".');
    }, function () {
      $rootScope.isDialogOpen=false;
      console.log('You cancelled the dialog.');
    });
  };

  $scope.showMenu = function (ev) {
    this._mdPanel = $mdPanel;
    var position = this._mdPanel.newPanelPosition()
      .relativeTo('.user-menu')
      .addPanelPosition(this._mdPanel.xPosition.ALIGN_START, this._mdPanel.yPosition.BELOW);

    var config = {
      attachTo: angular.element(document.body),
      controller: 'UserPanelMenuCtrl',
      controllerAs: 'ctrl',
      template: '' +
      '<div class="menu-panel" md-whiteframe="4">' +
      '  <div class="menu-content">' +
      '    <div class="menu-item" ng-show="!ctrl.isLoggedIn">' +
      '      <md-button class="md-button" ng-click="ctrl.login()">' +
      '        <span>Login</span>' +
      '      </md-button>' +
      '    </div>' +
      '    <div class="menu-item">' +
      '      <md-button class="md-button" ng-click="ctrl.feedback()">' +
      '        <span>Feedback</span>' +
      '      </md-button>' +
      '    </div>' +
      '    <div class="menu-item">' +
      '      <md-button href="#/copyright" class="md-button">' +
      '        <span>Copyright</span>' +
      '      </md-button>' +
      '    </div>' +
      '    <div class="menu-item" ng-show="ctrl.isLoggedIn">' +
      '      <md-button class="md-button" ng-click="ctrl.logout()">' +
      '        <span>Logout</span>' +
      '      </md-button>' +
      '    </div>' +
      '    <md-divider></md-divider>' +
      '    <div class="menu-item">' +
      '      <md-button class="md-button" ng-click="ctrl.closeMenu()">' +
      '        <span>Close Menu</span>' +
      '      </md-button>' +
      '    </div>' +
      '  </div>' +
      '</div>',
      panelClass: 'demo-menu-example',
      position: position,
      locals: {
        'selected': this.selected,
        'items': ['Login', 'Feedback', 'Copyright']
      },
      openFrom: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      focusOnOpen: false,
      zIndex: 9999
    };

    this._mdPanel.open(config)
      .then(function (result) {
        //position._left=(parseInt(position._left.replace('px',''))-210)+'px';
        angular.element('.demo-menu-example').css('left', 'auto')
        angular.element('.demo-menu-example').css('right', '10px')
      });
  };

}]);