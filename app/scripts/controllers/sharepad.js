'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:SharepadCtrl
 * @description
 * # SharepadCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('SharepadCtrl', ['$rootScope', '$mdDialog', '$log', '$scope', '$filter', '$timeout', '$window', '$route', 'cardViewFactory', 'constantService', 'favoriteService', '_', 'albumService', function ($rootScope, $mdDialog, $log, $scope, $filter, $timeout, $window, $route, cardViewFactory, constantService, favoriteService, _, albumService) {
    var ALL_ALBUM_ID = "1";
    var _prevReq;
    var albumsData = [{
      id: ALL_ALBUM_ID,
      title: "All"
    }];
    var count = 0;
    $scope.all_feeds = [];

    var columnIndx;
    var selected = null,
      previous = null;
    $scope.albumsData = albumsData;
    $timeout(function () {
      $scope.selectedIndex = 0;
      $scope.selectedAlbum = ALL_ALBUM_ID;
      $scope.defaultAlbum = ALL_ALBUM_ID;
      $scope.selectedAlbumId = ALL_ALBUM_ID;
      $scope.$watch('selectedIndex', function (current, old) {
        //$scope.selectedIndex = 0;
        previous = selected;
        selected = albumsData[current];
        if (old + 1 && (old != current)) $log.debug('Goodbye ' + previous.title + '!');
        if (current + 1) $log.debug('Hello ' + selected.title + '!');
      });
    }, 1000);


    $scope.openaddAlbumdialog = function () {
      $mdDialog.show({
        scope: $scope,   // Uses prototypal inheritance to gain access to parent scope
        templateUrl: 'views/add-album.html',
        parent: angular.element(document.body),
        preserveScope: true,
        //targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })

    }

    $scope.addAlbum = function (albumName) {
      var uniquealbumName = true;
      if (albumName == undefined || albumName == "") {
        alert("Please Enter AlbumName.");
        return;
      }
      else {
        albumName = albumName.toLowerCase();

        angular.forEach(albumsData, function (value, key) {
          if (value.id == albumName) {
            alert("This albumName already exists.Please add unique albumName.");
            uniquealbumName = false;
            $scope.newAlbumName = "";
          }

        })

        if (uniquealbumName == true) {
          var guid = albumName.replace(/ /g, '').toLowerCase();
          albumsData.push({
            id: guid,
            title: $scope.newAlbumName,
            albumStore_id: guid + '_1',
            albumId: guid,
            albumName: $scope.newAlbumName,
            userName: $scope.fullName
          });

          var newalbum = _.last(albumsData);

          albumService.add(newalbum);
          albumName = "";
          $scope.newAlbumName = "";
          $mdDialog.hide();
        }
      }
    };

    $scope.cancel = function () {
      $mdDialog.hide();
    }

    $scope.deleteAlbumdialog = function () {
      $mdDialog.show({
        scope: $scope,   // Uses prototypal inheritance to gain access to parent scope
        templateUrl: 'views/deleteAlbum.html',
        parent: angular.element(document.body),
        preserveScope: true,
        clickOutsideToClose: true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
    };

    $scope.feeds = [];
    $scope.feeds_grp_0 = [];
    $scope.feeds_grp_1 = [];
    $scope.feeds_grp_2 = [];
    $scope.feeds_grp_3 = [];
    var timeOutArr = [];
    var timeoutItem = undefined;
    $scope.selectedAlbum = undefined;
    $scope.defaultAlbum = undefined;
    $scope.storageMediaType = undefined;
    $scope.selectedfeedKey = undefined;
    $scope.title = "Select Album";
    $scope.toDrag = undefined;
    var columns;
    var _prevCols;

    var albums = {};
    var myInterval = undefined;

    $scope.defaultAlbum = ALL_ALBUM_ID;
    $scope.selectedAlbumId = ALL_ALBUM_ID;

    $scope.albums = albums;

    $scope.init = function () {
      if ($(window).width() < 768) {		//Set default value for screen sizes.
        $scope.toDrag = false;
      }
      else {
        $scope.toDrag = true;
      }

      $scope.storageMediaType = constantService.mediaType.photo + "," + constantService.mediaType.video + "," + constantService.mediaType.link;
      $scope.loadVideoSaves();
      $scope.getuserAlbums();

    }

    $scope.getSelectedAlbum = function () {
      $scope.selectedAlbum = undefined;
      if ($scope.selectedAlbumindropdown != undefined && $scope.selectedAlbumindropdown != "Please select an album") {
        $scope.selectedAlbum = $scope.selectedAlbumindropdown.albumId;
        $scope.title = $scope.selectedAlbumindropdown.title;
        return $scope.selectedAlbumindropdown.title;
      } else {
        return "Please select an album ";
      }
    };

    $scope.loadVideoSaves = function () {
      $scope.pageNo = 0;
      $scope.all_feeds.length = 0;
      $scope.feeds_grp_0.length = 0;
      $scope.feeds_grp_1.length = 0;
      $scope.feeds_grp_2.length = 0;
      $scope.feeds_grp_3.length = 0;
      $scope.feeds.length = 0;
      var feedCardViewService = cardViewFactory.create({ "country": undefined, "category": undefined, "subcategory": undefined, "pageType": undefined }, {
        getInitialConfig: function () {
          return {
            channels: [
              {
                type: 'facebook',
                albumID: $scope.selectedAlbumId,
                mediaType: $scope.storageMediaType
              },
              {
                type: 'favorite',
                albumID: $scope.selectedAlbumId,
                mediaType: $scope.storageMediaType
              }
            ],
            pipes: [
              {
                name: 'indexAppender',
                config: {}
              }
            ]
          }
        },
        onData: function (feeds) {
          console.log('Total favorites : ' + feeds.data.length);          

          var dataArray = feeds.data;

          dataArray = dataArray.sort(sortByTime);
          console.log('after dataArray sort');
          dataArray = $filter('unique')(dataArray, "feedKey");
          //update indexes after sorting and filtering-260516
          columns = _returnColumns();
          //columns=columns-1;
          console.log('after filter');
          _prevCols = columns;
          console.log('columns : ' + columns);
          _.each(dataArray, function (value, index) {
            value.is_saved = true;

            if (!value.favorites) {
              value.favorites = true;
            }
            value.index = index + 1;

            if (value.favorites) {
              $scope.all_feeds = $scope.all_feeds.concat(value);
            }
          });
          $scope.getNextPage();
        }, onError: function (error) {
          
        }
      })
    }


    angular.element($window).bind('resize', function () {
      var timeoutItem = $timeout(function () {
        $scope._arrangeColsFeeds();
      }.bind(this));
      timeOutArr.push(timeoutItem);
    });

    $scope._arrangeColsFeeds = function () {
      columns = _returnColumns();
      //columns=columns-1;
      //if (_prevCols != columns) {
      console.log('arranging...')
      _prevCols = columns;
      $scope.feeds_grp_0.length = 0;
      $scope.feeds_grp_1.length = 0;
      $scope.feeds_grp_2.length = 0;
      $scope.feeds_grp_3.length = 0;
      angular.forEach($scope.feeds, function (itemData, index) {
        itemData.index = index + 1;
        if ((itemData.index % columns) == 0) {
          $scope.feeds_grp_0 = $scope.feeds_grp_0.concat(itemData);
        } else if ((itemData.index % columns) == 1) {
          $scope.feeds_grp_1 = $scope.feeds_grp_1.concat(itemData);
        } else if ((itemData.index % columns) == 2) {
          $scope.feeds_grp_2 = $scope.feeds_grp_2.concat(itemData);
        }
        else {
          $scope.feeds_grp_3 = $scope.feeds_grp_3.concat(itemData);
        }
      });

    }

    $scope.next = function () {
      if (!_prevReq) {
       $timeout(function(){
        _prevReq = true;
        $scope.getNextPage();
       });
      }
    }

    $scope.getNextPage = function () {
      columns = _returnColumns();
      _prevCols = columns;
      var feeds = $scope.all_feeds.slice($scope.pageNo * 15, ($scope.pageNo * 15 + 15));
      if (feeds.length > 0) {
        _.each(feeds, function (itemData, index) {
          columnIndx = (itemData.index % columns);
          if (columnIndx != 0) {
            $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
          } else {
            $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
          }
          $scope.feeds = $scope.feeds.concat(itemData);
        });
        $scope.pageNo += 1;
      }
      _prevReq = undefined;
    }

    function sortByTime(a, b) {
      var aTime = $filter('dateToMinutes')(a.postedTimestamp);
      var bTime = $filter('dateToMinutes')(b.postedTimestamp);
      if (aTime > bTime) return 1;
      if (aTime < bTime) return -1;
      return 0;
    }
    /*call from index app for reloading feeds*/

    $rootScope.$on("reloadFavouritesFeeds", function (event) {
      $scope.loadVideoSaves();
    });

    $rootScope.$on("_remove_loading", function (e, feedType, element_id) {
      if (myInterval) {
        clearInterval(myInterval);
      }
      if (feedType == constantService.feedTypes.Twitter) {
        //angular.element('#'+element_id).removeClass('iframe-body');
        angular.element('.iframe-body').css('background', 'none');
        angular.element('.iframe-body').css('background', 'none');
        if (angular.element('.iframe-body').parent() && angular.element('.iframe-body').prev()) {
          angular.element('.iframe-body').css('height', (angular.element('.iframe-body').parent().height() - angular.element('.iframe-body').prev().height() - 20) + 'px');
        }
      } if (feedType == constantService.feedTypes.Facebook) {
        myInterval = setInterval(function () {
          if (angular.element('#' + element_id).attr("fb-xfbml-state") == "rendered") {
            clearInterval(myInterval);
            myInterval = undefined;
            angular.element('.iframe-body').css('background', 'none');
            if (angular.element('.iframe-body').parent() && angular.element('.iframe-body').prev()) {
              angular.element('.iframe-body').css('height', (angular.element('.iframe-body').parent().height() - angular.element('.iframe-body').prev().height() - 20) + 'px');
            }
          }
        }, 500);
      }
    });

    $scope.$on("delete_feed", function (event, feedKeyEvent) {
      //alert(feedKey);
      if (feedKeyEvent.selector != undefined) {
        angular.forEach($scope.feeds, function (dataItem, index) {
          var favorite = dataItem;
          if (favorite.feedKey === feedKeyEvent.selector) {
            $scope.feeds.splice(index, 1);
          }
        });
      }
    });

    $rootScope.$on("delete_feedfav", function (event, feed) {
      if (feed != undefined) {
        angular.forEach($scope.feeds, function (dataItem, index) {
          var favorite = dataItem;
          if (favorite.feedKey === feed.feedKey) {
            $scope.feeds.splice(index, 1);

            $scope._arrangeColsFeeds();
            //$route.reload();
          }
        });
      }
    });
    $rootScope.$on("add_feedfav", function (event, feed) {
      var oldFeedObj = _.filter($scope.feeds, { feedKey: feed.feedKey })[0];
      if (!oldFeedObj) {
        if (feed != undefined) {
          $scope.feeds.splice(feed.index - 1, 0, feed);
        }
      }
    });

    $scope.getuserAlbums = function () {
      albumService.getAll().then(function (allAlbums) {
        angular.forEach(allAlbums, function (value, key) {
          albumsData.push(value);
        });
      })
    }


    $scope.setSelectedFeed = function (feed) {
      $scope.selectedFeed = feed;
      $mdDialog.show({
        //controller: DialogController,
        scope: $scope,   // Uses prototypal inheritance to gain access to parent scope
        templateUrl: 'views/move-album.html',
        parent: angular.element(document.body),
        preserveScope: true,
        //targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
    }

    $scope.albumClicked = function (albumId) {
      $scope.selectedAlbumId = albumId;
      $scope.selectedAlbum = albumId;
      $scope.defaultAlbum = albumId;
      angular.forEach(timeOutArr, function (timeoutItem) {
        $timeout.cancel(timeoutItem);
      });
      $scope.loadVideoSaves();
    }

    $scope.confirmDialog = function (ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm({ scope: $scope.$new() })
        .title('Would you like to delete this album?')
        .ariaLabel('Lucky day')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        if ($scope.selectedAlbum == undefined || $scope.selectedAlbum == "") {
          alert("Please Select Any Album!");
          //$mdDialog.hide();
          return;
        }

        var albumIdToDelete = $scope.selectedAlbum;
        albumService.removeById(albumIdToDelete);

        angular.forEach($scope.albumsData, function (album, index) {
          if (album && album.id === albumIdToDelete) {
            $scope.albumsData.splice(index, 1);
          }
        });

        // reset the albumId in favorites
        favoriteService.getAll().then(function (data) {
          if (data) {
            angular.forEach(data, function (dataItem) {
              var favorite = dataItem;
              if (favorite.albumId === albumIdToDelete) {
                delete favorite.albumId;
                favoriteService.update(favorite);
              }
            });
          }
        });

        if ($scope.selectedAlbumId == albumIdToDelete) {
          $scope.selectedAlbumId = ALL_ALBUM_ID;
          $scope.selectedAlbum = undefined;
          $scope.loadVideoSaves();
        } else if ($scope.selectedAlbumId == ALL_ALBUM_ID) {
          $scope.loadVideoSaves();
        }
        else {
          $scope.selectedAlbumId = ALL_ALBUM_ID;
          $scope.selectedAlbum = ALL_ALBUM_ID;
          $scope.loadVideoSaves();
        }

        $scope.defaultAlbum = ALL_ALBUM_ID;
        $scope.selectedIndex = 0;

        $mdDialog.hide();

      }, function () {
        alert('You decided to keep this album.');
      });
    };
    // $scope.$on('albumClicked', function (event, albumId) {
    //   $scope.selectedAlbumId = albumId;
    //   $scope.selectedAlbum = albumId;
    //   $scope.defaultAlbum = albumId;
    //   $scope.loadVideoSaves();
    // });

    // Add/MoveFavoriteToAlbum
    $scope.addFavoriteToAlbum = function () {
      if ($scope.selectedFeed != undefined) {
        //push feed in album
        var selectedFeed = $scope.selectedFeed;
        selectedFeed['albumId'] = $scope.selectedAlbum;
        favoriteService.update(selectedFeed);
        if ($scope.defaultAlbum != undefined && $scope.defaultAlbum != ALL_ALBUM_ID) {
          angular.forEach($scope.feeds, function (dataItem, index) {
            var favorite = dataItem;
            if (favorite.feedKey == selectedFeed.feedKey) {

              $scope.feeds.splice(index, 1);
              $scope._arrangeColsFeeds();

            }
          });
        }
      }
      //$scope.selectedAlbumindropdown="Please select an album";
      $mdDialog.hide();
      $scope.selectedAlbumindropdown = "";
    }

    $scope.cancelmovetoalbumdialog = function () {
      $mdDialog.hide();
    }

    $scope.deleteAlbum = function () {

      /*if ($scope.selectedAlbum == undefined || $scope.selectedAlbum == "") {
         alert("Please Select Any Album!");
         //$mdDialog.hide();
         return;
       }*/

      var albumIdToDelete = $scope.selectedAlbum;
      albumService.removeById(albumIdToDelete);

      angular.forEach($scope.albumsData, function (album, index) {
        if (album && album.id === albumIdToDelete) {
          $scope.albumsData.splice(index, 1);
        }
      });

      // reset the albumId in favorites
      favoriteService.getAll().then(function (data) {
        if (data) {
          angular.forEach(data, function (dataItem) {
            var favorite = dataItem;
            if (favorite.albumId === albumIdToDelete) {
              delete favorite.albumId;
              favoriteService.update(favorite);
            }
          });
        }
      });

      if ($scope.selectedAlbumId == albumIdToDelete) {
        $scope.selectedAlbumId = ALL_ALBUM_ID;
        //$scope.defaultAlbum = ALL_ALBUM_ID;
        $scope.selectedAlbum = undefined;
        $scope.loadVideoSaves();
      } else if ($scope.selectedAlbumId == ALL_ALBUM_ID) {
        $scope.loadVideoSaves();
      }
      else {
        $scope.selectedAlbumId = ALL_ALBUM_ID;
        $scope.loadVideoSaves();
      }
      //$rootScope.$broadcast("deleteAlbum");
      $scope.selectedAlbumindropdown = "Please select an album";

      $scope.selectedIndex = 0;

      $mdDialog.hide();
    }

    $scope.onDropComplete = function (data, evt, album) {
      $scope.selectedAlbum = album.albumId;
      $scope.selectedFeed = data;
      $scope.addFavoriteToAlbum();
    }
    $scope.onDragMove = function (e) {
      angular.element("[ng-drop]").css({ border: '1px solid red' });

    }
    $scope.onDragStop = function (e) {
      angular.element("[ng-drop]").css({ border: '' });
    }

    $scope.$on('$destroy', function () {
      angular.forEach(timeOutArr, function (timeoutItem) {
        $timeout.cancel(timeoutItem);
      });
    });

    $scope.init();
  }]);
