'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('FeedCtrl', ['$window', '$location', '$rootScope', '$scope', '$route', '$routeParams', '$timeout', '$q', '$analytics', 'favoriteService', 'cardViewFactory',
    'countryService', 'constantService', 'storageService', 'contentFilterService','waitUtilService', function ($window, $location, $rootScope, $scope, $route, $routeParams, $timeout, $q, $analytics, favoriteService, cardViewFactory,
      countryService, constantService, storageService, contentFilterService,waitUtilService) {
      $scope.feeds = []
      $scope.feeds_grp_0 = [];
      $scope.feeds_grp_1 = [];
      $scope.feeds_grp_2 = [];
      $scope.feeds_grp_3 = [];
      $scope.trendalyzerFeeds = []
      $scope.activeNotificationFeed = { title: 'Default' }

      var feedCardViewService = undefined
      var trendalyzerCardViewService = undefined;
      var autocompleteCardViewService = undefined;
      var timeOutArr = [];
      var timeout = undefined;
      var oldFeedsHashKey = undefined;
      var lsKey = undefined;
      var myInterval = undefined;
      var _prevReq;
      var columns;
      var _prevCols;
      var columnIndx;
      var pageGenTime = undefined;

      this.loadFeed = function () {
        waitUtilService.showWait();
        var country = $routeParams.country || countryService.getCountry();
        var category = $routeParams.category || constantService.defaultCategory;
        var subcategory = $routeParams.subcategory || constantService.defaultSubCategory;
        var pageType = 'top';
        var mediatype = constantService.mediaType.video;
        lsKey = country + "_" + pageType + "_" + category + "_" + subcategory + "_" + mediatype;

        //$analytics.eventTrack('Click', { country : country, category : category, subcategory : subcategory});

        feedCardViewService = cardViewFactory.create({ "country": country, "category": category, "subcategory": subcategory, "pageType": pageType }, {
          onData: function (feeds) {
            columns = _returnColumns();
            waitUtilService.hideWait();
            _prevCols = columns;
            if (feeds.data.length > 0) {
              if (pageType === "top" && feeds.pageNo === 0) {//set page 0 data in local-storage
                //$scope.feeds = feeds.data;
                angular.forEach(feeds.data, function (itemData, index) {
                  if (index == 0 && itemData.genTime) {
                    pageGenTime = itemData.genTime;
                  }
                  itemData.category = category;
                  itemData.subcategory = subcategory;
                  itemData.include = country;

                  columnIndx = (itemData.index % columns);
                  if (columnIndx != 0) {
                    $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
                  } else {
                    $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
                  }
                  $scope.feeds[index] = itemData;
                });
                _prevReq = undefined;
              } else {
                //$scope.feeds = $scope.feeds.concat(feeds.data);
                angular.forEach(feeds.data, function (itemData, index) {
                  if (index == 0 && itemData.genTime && itemData.genTime != pageGenTime) {
                    $route.reload();
                  }
                  itemData.category = category;
                  itemData.subcategory = subcategory;
                  itemData.include = country;
                  
                  columnIndx = (itemData.index % columns);
                  if (columnIndx != 0) {
                    $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
                  } else {
                    $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
                  }
                  $scope.feeds[itemData.index - 1] = itemData;
                });
                _prevReq = undefined;
              }
            }
            else if (feeds.pageNo === 0 && feeds.data.length == 0) {
              angular.forEach(timeOutArr, function (tmItem) {
                $timeout.cancel(tmItem);
              });
              $scope.errors = "Whoops! Feeds not found for specified content filter and network filter, please check filters and try later.";
              $scope.feeds = [];
            }
          }.bind(this), onError: function (error) {
            waitUtilService.hideWait();
            $scope.errors = "Whoops! Feeds not found for specified content filter and network filter, please check filters and try later.";
            //load from local-storage content
            this.loadFeedsFromLocalstorage(country, pageType, category, subcategory, mediatype);
          }
        });
      }

      angular.element($window).bind('resize', function () {
        var timeoutItem = $timeout(function () {
          $scope._arrangeColsFeeds();
        }.bind(this));
        timeOutArr.push(timeoutItem);
      });

      $scope._arrangeColsFeeds = function () {
        columns = _returnColumns();
        if (_prevCols != columns) {
          console.log('arranging....')
          _prevCols = columns;
          $scope.feeds_grp_0.length = 0;
          $scope.feeds_grp_1.length = 0;
          $scope.feeds_grp_2.length = 0;
          $scope.feeds_grp_3.length = 0;
          angular.forEach($scope.feeds, function (itemData, index) {
            columnIndx = (itemData.index % columns);
            if (columnIndx != 0) {
              $scope["feeds_grp_" + (columnIndx - 1)] = $scope["feeds_grp_" + (columnIndx - 1)].concat(itemData);
            } else {
              $scope["feeds_grp_" + (columns - 1)] = $scope["feeds_grp_" + (columns - 1)].concat(itemData);
            }
          });
          //$scope.$apply();
        } else {

        }
      }

      this.loadTrendalyzerFeed = function () {
        var country = $routeParams.country || countryService.getCountry();
        var category = $routeParams.category || constantService.defaultCategory;
        var subcategory = $routeParams.subcategory || constantService.defaultSubCategory;
        var pageType = 'trendalyzer';

        trendalyzerCardViewService = cardViewFactory.create({ "country": country, "category": category, "subcategory": subcategory, "pageType": pageType }, {
          onData: function (feeds) {
            timeout = $timeout(function () {
              $scope.trendalyzerFeeds = $scope.trendalyzerFeeds.concat(feeds.data)
            });
            timeOutArr.push(timeout);
          }, onError: function (error) {
          }
        })
      }

      this.loadAutoCompleteList = function () {
        var country = $routeParams.country || countryService.getCountry();
        var category = $routeParams.category || constantService.defaultCategory;
        var subcategory = $routeParams.subcategory || constantService.defaultSubCategory;
        var pageType = 'autoComplete';
        var d = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
        storageService.get(constantService.storageDefaultCategory, "searchcontent_" + category + "_" + country, "searchcontent_" + category + "_" + country).then(function (item) {
          if (item) {
            if (item.lastDate != d) {
              timeout = $timeout(function () {
                //storageService.remove("localstorage", "searchcontent_"+category+"_"+country);
                storageService.remove(constantService.storageDefaultCategory, "searchcontent_" + category + "_" + country);
                fetchAndStoreAutocomplete(country, category, pageType);
              });
              timeOutArr.push(timeout);
            }
          }
          else {
            timeout = $timeout(function () {
              fetchAndStoreAutocomplete(country, category, pageType);
            });
            timeOutArr.push(timeout);
          }
        });
      }

      function fetchAndStoreAutocomplete(country, category, pageType) {
        var d = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();

        autocompleteCardViewService = cardViewFactory.create({ "country": country, "category": category, "pageType": pageType }, {
          onData: function (feeds) {
            if (feeds.data.length > 0) {
              storageService.set(constantService.storageDefaultCategory, "searchcontent_" + category + "_" + country, { lastDate: d, data: feeds.data }).then(function (item) {
                timeout = $timeout(function () {
                  $rootScope.$emit('loadAutoComplete', { category: category, country: country });
                });
                timeOutArr.push(timeout);
              })
                .catch(function (response) {

                });
            }
          }, onError: function (error) {
            
          }
        });
      }

      this.loadFeedsFromLocalstorage = function (country, pageType, category, subcategory, mediatype) {
        contentFilterService.get(undefined, undefined, constantService.storageMediaTypeKey).then(function (mediaTypes) {
          mediatype = mediaTypes;
          if (mediatype.length == 0) {
            mediatype = defaultMediaTypes;
          }
          var lsKey = country + "_" + pageType + "_" + category + "_" + subcategory + "_" + mediatype;
          storageService.get(constantService.storageDefaultCategory, lsKey, lsKey).then(function (item) {
            if (item) {
              //oldFeedsHashKey=Hashcode.value(item);
              angular.forEach(item, function (itemData, index) {
                timeout = $timeout(function () {
                  $scope.feeds = $scope.feeds.concat(itemData);
                }, index * 50);
                timeOutArr.push(timeout);
              });
            }
          })
            .catch(function (response) {
              
            });
        })
          .catch(function (response) {
            
          });
      };

      this.init = function () {
        this.loadFeed();
        //this.loadTrendalyzerFeed();
        // timeout = $timeout(function () {
        //   this.loadAutoCompleteList();
        // }.bind(this), 5000);
        //timeOutArr.push(timeout);
      };

      $scope.next = function () {
        console.log('content filter popup opened : '+$rootScope.isDialogOpen)
        if (!_prevReq && !$rootScope.isDialogOpen) {
          _prevReq = true;
          feedCardViewService.getNextPage();
        }
      }

      $scope.$on('$destroy', function () {
        $rootScope.$broadcast('stopYbVideos');
        angular.forEach(timeOutArr, function (timeoutItem) {
          $timeout.cancel(timeoutItem);
        });
      });

      $scope.$on('$locationChangeStart', function () {
        angular.forEach(timeOutArr, function (timeoutItem) {
          $timeout.cancel(timeoutItem);
        });
      });

      $rootScope.$on("_remove_loading", function (e, feedType, element_id) {
        if (myInterval) {
          clearInterval(myInterval);
        }
        if (feedType == constantService.feedTypes.Twitter || feedType == constantService.feedTypes.Instagram || feedType == constantService.feedTypes.Reddit) {
          //angular.element('#'+element_id).removeClass('iframe-body');
          angular.element('.iframe-body').css('background', 'none');
          angular.element('.iframe-body').css('background', 'none');
          if (angular.element('.iframe-body').parent() && angular.element('.iframe-body').prev()) {
            //console.log(angular.element('.iframe-body').parent().height());
            //console.log(angular.element('.iframe-body').prev().height());
            angular.element('.iframe-body').css('height', (angular.element('.iframe-body').parent().height() - angular.element('.iframe-body').prev().height() - 30) + 'px');
          }
        } if (feedType == constantService.feedTypes.Facebook) {
          myInterval = setInterval(function () {
            if (angular.element('#' + element_id).attr("fb-xfbml-state") == "rendered") {
              clearInterval(myInterval);
              myInterval = undefined;
              angular.element('.iframe-body').css('background', 'none');
              if (angular.element('.iframe-body').parent() && angular.element('.iframe-body').prev()) {
                angular.element('.iframe-body').css('height', (angular.element('.iframe-body').parent().height() - angular.element('.iframe-body').prev().height() - 20) + 'px');
              }
            }
          }, 500);
        }
      });

      $rootScope.$on("changeFavouriteFlag", function (event, feedObj) {
        var oldFeedObj = _.filter($scope.feeds, { feedKey: feedObj.feedKey })[0];
        if (oldFeedObj) {
          if (feedObj.favorites == true) {
            oldFeedObj.favorites = true;
            $rootScope.$emit('add_feedfav', oldFeedObj);
          }
          else {
            oldFeedObj.favorites = false;
            $rootScope.$emit('delete_feedfav', oldFeedObj);
          }
        }
      });

      this.init();
    }]);
