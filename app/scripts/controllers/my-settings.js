'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:MySettingsCtrl
 * @description
 * # MySettingsCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('MySettingsCtrl', ['$scope', '$mdToast', 'userService', '_', 'constantService', 'countryService', function ($scope, $mdToast, userService, _, constantService, countryService) {
    var defaultCategories= defaultCategories = constantService.homeDefaultCategories[countryService.getCountry()];
    $scope.hiddenCategories={
      // humor:{ id: 'humor', categoryId: 'humor', subcategoryId: 'all', name: 'Humor'},
      sports:{ id: 'sports', categoryId: 'sports', subcategoryId: 'all', name: 'Sports'},
      // entertainment:{ id: 'entertainment', categoryId: 'entertainment', subcategoryId: 'all', name: 'Entertainment'},
      dailynews:{ id: 'dailynews', categoryId: 'infotainment', subcategoryId: 'dailynews', name: 'Daily News'},
      tech:{ id: 'tech', categoryId: 'infotainment', subcategoryId: 'tech', name: 'Technology'},
      travel:{ id: 'travel', categoryId: 'lifestyle', subcategoryId: 'travel', name: 'Travel'},
      health:{ id: 'health', categoryId: 'lifestyle', subcategoryId: 'health', name: 'Health'},
      beauty:{ id: 'beauty', categoryId: 'lifestyle', subcategoryId: 'beauty', name: 'Beauty'},
      food:{ id: 'food', categoryId: 'lifestyle', subcategoryId: 'food', name: 'Food'},
      comics:{ id: 'comics', categoryId: 'humor', subcategoryId: 'comics', name: 'Comics'},
      dailyhumor:{ id: 'dailyhumor', categoryId: 'humor', subcategoryId: 'dailyhumor', name: 'Daily Humor'},
      pranks:{ id: 'pranks', categoryId: 'humor', subcategoryId: 'pranks', name: 'Pranks'},
      movies:{ id: 'movies', categoryId: 'entertainment', subcategoryId: 'movies', name: 'Movies'},
      music:{ id: 'music', categoryId: 'entertainment', subcategoryId: 'music', name: 'Music'},
      celebrity:{ id: 'celebrity', categoryId: 'entertainment', subcategoryId: 'celebrity', name: 'Celebrity'},
      television:{ id: 'television', categoryId: 'entertainment', subcategoryId: 'television', name: 'Television'},
      american_football:{ id: 'american_football', categoryId: 'sports', subcategoryId: 'american_football', name: 'American Football'},
      basketball:{ id: 'basketball', categoryId: 'sports', subcategoryId: 'basketball', name: 'Basketball'},
      cricket:{ id: 'cricket', categoryId: 'sports', subcategoryId: 'cricket', name: 'Cricket'},
      fights:{ id: 'fights', categoryId: 'sports', subcategoryId: 'fights', name: 'Fights'},
      soccer:{ id: 'soccer', categoryId: 'sports', subcategoryId: 'soccer', name: 'Soccer'},
      baseball:{ id: 'baseball', categoryId: 'sports', subcategoryId: 'baseball', name: 'Baseball'},
      golf:{ id: 'golf', categoryId: 'sports', subcategoryId: 'golf', name: 'Golf'},
      tennis:{ id: 'tennis', categoryId: 'sports', subcategoryId: 'tennis', name: 'Tennis'},
      //infotainment: { id: 'infotainment', categoryId: 'infotainment', subcategoryId: 'all', name: 'Infotainment'},
      interesting:{ id: 'interesting', categoryId: 'infotainment', subcategoryId: 'interesting', name: 'Interesting'},
      auto:{ id: 'auto', categoryId: 'infotainment', subcategoryId: 'auto', name: 'Auto'},
      gaming:{ id: 'gaming', categoryId: 'infotainment', subcategoryId: 'gaming', name: 'Gaming'},
      business:{ id: 'business', categoryId: 'infotainment', subcategoryId: 'business', name: 'Business'},
      science:{ id: 'science', categoryId: 'infotainment', subcategoryId: 'science', name: 'Science'},
      // lifestyle:{ id: 'lifestyle', categoryId: 'lifestyle', subcategoryId: 'all', name: 'LifeStyle'},
      quotes:{ id: 'quotes', categoryId: 'lifestyle', subcategoryId: 'quotes', name: 'Quotes'},
      workout:{ id: 'workout', categoryId: 'lifestyle', subcategoryId: 'workout', name: 'Workout'},
      // hobbies:{ id: 'hobbies', categoryId: 'hobbies', subcategoryId: 'all', name: 'Hobbies'},
      books:{ id: 'books', categoryId: 'hobbies', subcategoryId: 'books', name: 'Books'},
      diy:{ id: 'diy', categoryId: 'hobbies', subcategoryId: 'diy', name: 'Diy'},
      photography:{ id: 'photography', categoryId: 'hobbies', subcategoryId: 'photography', name: 'Photography'}
    }
    $scope.userDefinedCategories=[];

    var init = function () {
      $scope.menuConfigs = menuConfigs;

      userService.getPreferences().then(function (preferencesData) {
        var categoriesData = preferencesData;
        if(preferencesData){          
          if (preferencesData.preferences) {
            categoriesData = preferencesData.preferences;
          }
          if (preferencesData.personalized) {
            _.each(preferencesData.personalized, function (item) {
              $scope.userDefinedCategories.push({
                id:item.toLowerCase(),
                name:item,
                personaliseItem:true
              });
              delete $scope.hiddenCategories[item.toLowerCase()];
            });
          }
          _.each(categoriesData, function (categoryData) {
            var categoryId = angular.copy(categoryData.category);
            _.each(categoryData.subCategories, function (subCategoryId) {
              if($scope.hiddenCategories[subCategoryId]){
                $scope.userDefinedCategories.push($scope.hiddenCategories[subCategoryId]);
                delete $scope.hiddenCategories[subCategoryId];
              }
            });
          });
        }else{
          $scope.userDefinedCategories=defaultCategories;
          _.each(defaultCategories, function (subCategoryInfo) {
            if(subCategoryInfo.id){
              delete $scope.hiddenCategories[subCategoryInfo.id];
            }
          });
        }
      }, function (error) {

      });

      // categoryService._getsuggestedTopics().then(function (Topics) {
      //   $timeout(function () {
      //     $scope.suggestedCategories = Topics;
      //   });
      // }, function (error) {

      // });
    }

    $scope.addCategory = function (selectedCategory) {
      if(selectedCategory.id && $scope.hiddenCategories[selectedCategory.id.toLowerCase()]){
        $scope.userDefinedCategories.push($scope.hiddenCategories[selectedCategory.id.toLowerCase()]);
        delete $scope.hiddenCategories[selectedCategory.id.toLowerCase()];
        //$scope.showSavedToast();
        $scope.saveSettings();
      }
    }

    $scope.removeCategory = function (selectedCategory) {
      if(selectedCategory.id){
        var keyIndex=undefined;
        _.each($scope.userDefinedCategories,function(arrItem,arrIndex){
          if(arrItem && arrItem.id==selectedCategory.id){
            keyIndex=arrIndex;
          }
        });

        if(keyIndex!=undefined){
          $scope.userDefinedCategories.splice(keyIndex,1);
          $scope.hiddenCategories[selectedCategory.id.toLowerCase()]=selectedCategory;
          //$scope.showSavedToast();
          $scope.saveSettings();
        }
      }
    }

    $scope.createCategory=function(){
      if(!$scope.newCategory){
        alert('Enter category name!');
        return;
      }else if($scope.newCategory.length<3){
        alert('Category name should be atleast 3 characters!');
        return;
      }else{
        var duplCatEntered = false; 
        if ($scope.userDefinedCategories && $scope.userDefinedCategories.length > 0) {
          _.each($scope.userDefinedCategories, function (value, index) {
            if (value.id == $scope.newCategory.toLowerCase()) {
              alert("You have entered repeated category.please enter another content!!");
              duplCatEntered = true;
            }

          });
        }

        if (duplCatEntered == true) {
          $scope.newCategory = "";
          return;
        }
        $scope.userDefinedCategories.unshift({
          id:$scope.newCategory.toLowerCase(),
          name:$scope.newCategory,
          personaliseItem:true});
        $scope.newCategory=undefined;
        //$scope.showSavedToast();
        $scope.saveSettings();
      }
    }
    
    $scope.saveSettings=function(){
      var preferencesObj={
        preferences:[],
        personalized:[]
      };

      var selItems={};
      _.each($scope.userDefinedCategories,function(arrItem,arrIndex){
        if(arrItem && arrItem.personaliseItem && preferencesObj.personalized.indexOf(arrItem.id.toLowerCase())==-1){
          preferencesObj.personalized.push(arrItem.name.toLowerCase());
        }else{
          if(!selItems[arrItem.categoryId]){
            selItems[arrItem.categoryId]=[];
          }          
          selItems[arrItem.categoryId].push(arrItem.subcategoryId);
        }
      });

      _.each(selItems,function(selsubcategories,selCategory){
        preferencesObj.preferences.push({
          category:selCategory,
          subCategories:selsubcategories
        })
      });
      
      if(preferencesObj.preferences.length==0){
        delete preferencesObj.preferences;
      }
      if(preferencesObj.personalized.length==0){
        delete preferencesObj.personalized;
      }
      console.log(preferencesObj);
      
      userService.savePreferences(preferencesObj).then(function () {
        $scope.showSavedToast();
      }, function () {
        alert('Something went wrong!');
      });
    }

    $scope.sortableOptions = {
      // activate: function() {
      //     console.log("activate");
      // },
      // beforeStop: function() {
      //     console.log("beforeStop");
      // },
      // change: function() {
      //     console.log("change");
      // },
      // create: function() {
      //     console.log("create");
      // },
      // deactivate: function() {
      //     console.log("deactivate");
      // },
      // out: function() {
      //     console.log("out");
      // },
      // over: function() {
      //     console.log("over");
      // },
      // receive: function() {
      //     console.log("receive");
      // },
      // remove: function() {
      //     console.log("remove");
      // },
      // sort: function() {
      //     console.log("sort");
      // },
      // start: function() {
      //     console.log("start");
      // },
      // update: function(e, ui) {
      //   console.log("update");
      // },
      stop: function(e, ui) {
        console.log("stop");
        //$scope.showSavedToast();
        $scope.saveSettings();
      }
    };

    $scope.showSavedToast = function() {
      var toast = $mdToast.simple()
      .textContent('Setting saved!')
      //.action('OK')
      .position('bottom center')
      //.hideDelay(3000)
      .highlightAction(false);                     
   
      $mdToast.show(toast).then(function(response) {
          if ( response == 'ok' ) {
            console.log('You clicked \'OK\'.');
          }
      });
    };

    init();
  }]);
