'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:UserPanelMenuCtrl
 * @description
 * # UserPanelMenuCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('UserPanelMenuCtrl',['$rootScope','$mdDialog', 'mdPanelRef','$document','userService', function ($rootScope,$mdDialog, mdPanelRef,$document,userService) {
    this._mdPanelRef = mdPanelRef;
    this.isLoggedIn = userService.isLoggedIn();
    
    this.closeMenu = function () {
      mdPanelRef && mdPanelRef.close();
    }
  
    this.login = function () {
      userService.login();
      this.closeMenu();
    }
  
    this.logout = function () {
      userService.logout();
      this.closeMenu();
    }
  
    this.feedback = function () {
      mdPanelRef && mdPanelRef.close();
      $rootScope.isDialogOpen=true;
      $mdDialog.show({
        clickOutsideToClose: true,
        //scope: $scope,
        preserveScope: true,
        multiple: true,
        templateUrl: 'views/feedback-dialog.html',
        controller: 'FeedbackdialogCtrl',
        bindToController: true
      })
      .then(function (answer) {
        $rootScope.isDialogOpen=false;
        console.log('You said the information was "' + answer + '".');
      }, function () {
        $rootScope.isDialogOpen=false;
        console.log('You cancelled the dialog.');
      });
    };
  
    angular.element($document).bind("scroll", function (e) {
      mdPanelRef && mdPanelRef.close();
    });
  }]);
