'use strict';

/**
 * @ngdoc function
 * @name trendisAppClientApp.controller:TrendsCtrl
 * @description
 * # TrendsCtrl
 * Controller of the trendisAppClientApp
 */
angular.module('trendisAppClientApp')
  .controller('TrendsCtrl', ['$window', '$scope', '$filter', '$rootScope', '$route', '$routeParams', '$timeout', '$location', 'cardViewFactory',
    'countryService', 'constantService', 'storageService', 'waitUtilService', 'slideShowService', function ($window, $scope, $filter, $rootScope, $route, $routeParams, $timeout, $location, cardViewFactory,
      countryService, constantService, storageService, waitUtilService, slideShowService) {
      var autocompleteCardViewService = undefined;
      var timeOutArr = [];
      var timeout = undefined;
      $scope.feeds = [];
      $scope.humor_feeds = [];
      $scope.sports_feeds = [];
      $scope.hour_feeds = [];
      $scope.day_feeds = [];

      $scope.slickConfig = {
        enabled: true,
        autoplay: false,
        draggable: false,
        autoplaySpeed: 3000,
        method: {},
        slidesToShow: 4,
        slidesToScroll: 2,
        infinite: false,
        lazyLoad: "ondemand",
        arrows: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ],
        event: {
          beforeChange: function (event, slick, currentSlide, nextSlide) {
          },
          afterChange: function (event, slick, currentSlide, nextSlide) {
            event.stopPropagation();
            //event.preventDefault();
            //return false;
          }
        }
      };

      $rootScope.$on('logged_into_server', function (event, status) {
        $location.url("/trends");
      });

      this.init = function () {
        waitUtilService.showWait();
        var country = $routeParams.country || countryService.getCountry();

        if(country){
          _loadNextPage(country);
        }else{
          console.log('country not found!');
          countryService.resolveAndGet(undefined).then(function(_country){
            console.log(_country)
            _loadNextPage(_country);
          },function(err){
            console.log(err);
            _loadNextPage('US');
          });
        }
      }

      var _loadNextPage=function(c){
        if(!c){
          c='US';
        }
        $scope.mediaType = 'video';
        $scope.allFeedInfos = [];
        var hashcodeVal = undefined;
        var mediatypes = $scope.mediaType;
        var category = undefined;
        var subCategory = undefined;
        var pageType = 'trends';
        cardViewFactory.create({ "country": c, "category": category, "subcategory": subCategory, "pageType": pageType }, {
          onData: function (categoryFeeds) {
            waitUtilService.hideWait();
            var feedInfo = [];
            _.each(categoryFeeds.data, function (categoryFeed, categoryIndex) {
              feedInfo = categoryFeed.feeds.splice(0, 8);
              _.each(feedInfo, function (feed) {
                feed.index = $scope.feeds.length + 1;
                $scope.feeds = $scope.feeds.concat(feed);
              });
              $scope[categoryFeed.section + '_feeds'] = feedInfo;
            });
          }.bind(this), onError: function (error) {
            //error in API call
            waitUtilService.hideWait();
          }
        });
      }

      $scope.$on('$destroy', function () {
        angular.forEach(timeOutArr, function (timeoutItem) {
          $timeout.cancel(timeoutItem);
        });
      });

      $scope.$on('$locationChangeStart', function () {
        angular.forEach(timeOutArr, function (timeoutItem) {
          $timeout.cancel(timeoutItem);
        });
      });

      $scope.showSlideShow = function (feed) {
        $scope.feed = feed;
        slideShowService.doSlideShow($scope, { feeds: $scope.feeds, index: feed.index })
      }

      // $scope.$on('$viewContentLoaded', function () {
      //   //Here your view content is fully loaded !!
      // });

      $rootScope.$on('country_change', function (event, country) {
        console.log('receive country_change...')
        console.log(country)
        countryService.resolveAndGet(country).then(function () {
          $route.reload();
        });
      });

      var length = $('#all-output').height() - $('#sidebar-stick').height() + $('#all-output').offset().top;

      $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        var height = $('#sidebar-stick').height() + 'px';
        if ($('#all-output').offset() && scroll < $('#all-output').offset().top) {
          // $('#sidebar-stick').css({
          //     'position': 'absolute',
          //     'top': '0'
          // });
          $('#sidebar-stick').removeAttr("style");
          // } else if (scroll > length) {
          //     $('#sidebar-stick').css({
          //         'position': 'absolute',
          //         'bottom': '0',
          //         'top': 'auto'
          //     });

        } else {
          $('#sidebar-stick').css({
            'position': 'fixed',
            'top': '0',
          });
        }
      });

      var $category_show = $('#main-category-toggler'),
        $main_category = $('#main-category'),
        $main_category_toggler_close = $('#main-category-toggler-close'),
        $main_category_toggler = $('#main-category-toggler');

      $category_show.click(function () {
        $main_category.slideDown();
        $main_category_toggler_close.show();
        $main_category_toggler.hide();
      });


      $main_category_toggler_close.click(function () {
        $main_category.slideUp();
        $main_category_toggler_close.hide();
        $main_category_toggler.slideDown();
      });

      $('[data-toggle="tooltip"]').tooltip();
      $('body').removeClass('not-scrollable');


      this.init();
    }]);
